# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from unittest import mock
from uuid import uuid4
from zsnl_domains.document.infrastructures import ZohoInfrastructure


class Test_zohoInfrastructure(TestBase):
    def setup(self):
        config = {
            "zoho_write_base_url": "http://zoho-writer",
            "zoho_api_key": "samplekey",
        }
        self.zoho = ZohoInfrastructure()(config=config)

    @mock.patch(
        "zsnl_domains.document.infrastructures.document_editor.requests.request"
    )
    def test_edit_document(self, requests_mock):
        response = (
            "https://writer.zoho.eu/writer/officeapi/v1/document/c043d4c/open"
        )

        fake_json = {
            "session_delete_url": "https://writer.zoho.eu/writer/officeapi/v1/session/c043d4c",
            "save_url": "https://writer.zoho.eu/writer/officeapi/v1/document/c043d4c/save",
            "session_id": "c043d4c",
            "document_delete_url": "https://writer.zoho.eu/writer/officeapi/v1/document/1532836",
            "document_id": "1532836",
            "document_url": "https://writer.zoho.eu/writer/officeapi/v1/document/c043d4c/open",
        }
        requests_mock.return_value.json.return_value = fake_json
        requests_mock.return_value.status_code = 200

        document_info = {
            "document_name": "https://example.com/example.txt",
            "document_id": str(uuid4()),
        }
        callback_settings = {
            "save_format": "txt",
            "save_url": "https://example.com/example.txt",
            "context_info": "Edit document",
        }
        document_url = "https://example.com/example.txt"
        user_info = {
            "user_id": "sample_user_id",
            "display_name": "sample_user_name",
        }
        options = {
            "editor_settings": '{ "unit": "in", "language": "ln", "view": "pageview" }',
            "permissions": '{ "document.export": true, "document.print": true, "document.edit": true, "review.changes.resolve": false, "review.comment": true, "collab.chat": true }',
            "callback_settings": str(callback_settings),
            "document_info": str(document_info),
            "user_info": str(user_info),
            "document_defaults": '{ "orientation": "portrait", "paper_size": "Letter", "font_name": "Lato", "font_size": 12, "track_changes": "disabled" }',
            "url": document_url,
        }

        result = self.zoho.edit_document(options)

        assert result == response
