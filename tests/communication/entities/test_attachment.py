# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from uuid import uuid4
from zsnl_domains.communication.entities import AttachedFile


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestFileEntity:
    filename = "test.pdf"
    uuid = uuid4()
    mimetype = "file/pdf"
    size = 3455
    md5 = "84f6119b40b50523533e37d68e251e6e"
    date_created = "2019-09-19T15:45:41.423244"
    storage_location = "S3Local"

    def setup(self):
        self.file = AttachedFile(
            uuid=self.uuid,
            filename=self.filename,
            md5=self.md5,
            mimetype=self.mimetype,
            date_created=self.date_created,
            size=self.size,
            storage_location=self.storage_location,
        )

    def test_entity_id(self):
        assert self.file.entity_id == self.uuid
