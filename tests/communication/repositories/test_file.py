# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import io
import pytest
from collections import namedtuple
from email.message import EmailMessage
from minty.exceptions import NotFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication import entities
from zsnl_domains.communication.repositories import FileRepository


class InfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra
        self.config = {}

    def get_config(self, context):
        return self.config

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestFileRepository:
    def setup(self):
        self.mock_infra = mock.MagicMock()
        self.infra = InfraFactoryMock(infra={"database": mock.MagicMock()})
        self.event_service = mock.MagicMock()
        self.file_repo = FileRepository(
            infrastructure_factory=self.infra,
            context=None,
            event_service=self.event_service,
        )
        self.db = self.file_repo._get_infrastructure("database")

    @mock.patch("tempfile.NamedTemporaryFile")
    def test_get_file_handle(self, tempfile_mock):
        mock_self = mock.MagicMock()
        file_uuid = str(uuid4())

        fh = FileRepository.get_file_handle(mock_self, file_uuid)

        mock_self.session.execute.assert_called_once()
        mock_self.session.execute().fetchone.assert_called_once()

        mock_self._get_infrastructure.assert_called_once_with(name="s3")

        row = mock_self.session.execute().fetchone()
        infra = mock_self._get_infrastructure()
        infra.download_file.assert_called_once_with(
            destination=tempfile_mock(),
            file_uuid=row.uuid,
            storage_location=row.storage_location[0],
        )

        assert fh == tempfile_mock()

    def test_parse_as_message(self):
        fh = io.BytesIO(
            b"From: me@example.com\n"
            b"To: you@example.com\n"
            b"Subject: Message\n"
            b"\n"
            b"Body"
        )
        mock_self = mock.MagicMock()
        mock_self.get_file_handle.return_value = fh

        message_file_uuid = str(uuid4())

        rv = FileRepository.parse_as_message(mock_self, message_file_uuid)

        assert isinstance(rv, EmailMessage)
        assert rv["subject"] == "Message"
        mock_self.get_file_handle.assert_called_once_with(message_file_uuid)

    def test_upload(self):
        mock_self = mock.MagicMock()
        message_file_uuid = str(uuid4())
        file_content = io.BytesIO(b"some bytes")
        file_meta = {
            "uuid": message_file_uuid,
            "md5": "MD5HASH",
            "size": 12344,
            "mime_type": "application/pdf",
            "storage_location": "Minio",
        }
        mock_self._get_infrastructure(
            name="s3"
        ).upload.return_value = file_meta

        rv = FileRepository.upload(mock_self, file_content, message_file_uuid)
        assert rv == file_meta

    @mock.patch(
        "zsnl_domains.communication.repositories.file.FileRepository._transform_to_entity"
    )
    def test_get_file_by_uuid(self, _transform_to_entity):

        file_uuid = str(uuid4())
        self.db.execute().fetchone.return_value = "db_result"

        self.file_repo.get_file_by_uuid(file_uuid)

        self.db.execute().fetchone.assert_called_once()
        _transform_to_entity.assert_called_with(result="db_result")

        self.db.execute().fetchone.return_value = None
        with pytest.raises(NotFound):
            self.file_repo.get_file_by_uuid(file_uuid)

    def test__transform_to_entity(self):
        result = namedtuple(
            "Result",
            "uuid original_name md5 size mimetype date_created storage_location",
        )
        res = result(
            uuid=uuid4(),
            original_name="email.msg",
            md5="1234-md5",
            size=123,
            mimetype="application/vnd.ms-outlook",
            date_created="2019-11-21T12:40:33.166416+00:00",
            storage_location="S3",
        )
        file = self.file_repo._transform_to_entity(res)
        assert isinstance(file, entities.File)
        assert res.uuid == file.uuid
        assert res.original_name == file.filename
        assert res.md5 == file.md5
        assert res.size == file.size
        assert res.mimetype == file.mimetype
        assert res.date_created == file.date_created
        assert res.storage_location == file.storage_location

    @mock.patch(
        "zsnl_domains.communication.repositories.file.FileRepository.get_file_handle"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.file.load_outlook_msg"
    )
    def test_parse_as_message_outlook(self, mock_load_msg, get_file_handle):
        mock_load_msg.return_value = "email_message goes here"

        with open("tests/data/Dit is een testbericht.msg", "rb") as fh:
            file_uuid = str(uuid4())
            get_file_handle.return_value = fh
            rv = self.file_repo.parse_as_message_outlook(file_uuid)
            assert rv == "email_message goes here"
