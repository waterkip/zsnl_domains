# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin import integrations


class Test_ASUser_Get_Active_Appointment_Integrations(TestBase):
    def setup(self):
        self.load_query_instance(integrations)

    def test_get_active_appointment_integrations(self):
        integration_uuids = [uuid4(), uuid4()]
        appointment_integrations = [mock.MagicMock(), mock.MagicMock()]
        appointment_integrations[0].configure_mock(
            id=1,
            uuid=integration_uuids[0],
            name="Appointment Integration-1",
            active=True,
            module="appointment",
        )
        appointment_integrations[1].configure_mock(
            id=2,
            uuid=integration_uuids[1],
            name="Appointment Integration-2",
            active=True,
            module="appointment",
        )
        self.session.execute().fetchall.return_value = appointment_integrations
        self.session.execute.reset_mock()
        result = self.qry.get_active_appointment_integrations()

        assert len(result) == 2
        assert result[0].entity_id == integration_uuids[0]
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT interface.id, interface.uuid, interface.name, interface.active, interface.module \n"
            "FROM interface \n"
            "WHERE interface.active IS true AND interface.module IN (%(module_1_1)s) AND interface.date_deleted IS NULL"
        )


class Test_ASUser_Get_Active_Appointment_v2_Integrations(TestBase):
    def setup(self):
        self.load_query_instance(integrations)

    def test_get_active_appointment_v2_integrations(self):
        integration_uuids = [uuid4(), uuid4()]
        appointment_integrations = [mock.MagicMock(), mock.MagicMock()]
        appointment_integrations[0].configure_mock(
            id=1,
            uuid=integration_uuids[0],
            name="Appointment Integration-1",
            active=True,
            module="appointment",
        )
        appointment_integrations[1].configure_mock(
            id=2,
            uuid=integration_uuids[1],
            name="Appointment Integration-2",
            active=True,
            module="appointment",
        )
        self.session.execute().fetchall.return_value = appointment_integrations
        self.session.execute.reset_mock()
        result = self.qry.get_active_appointment_v2_integrations()

        assert len(result) == 2
        assert result[0].entity_id == integration_uuids[0]
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT interface.id, interface.uuid, interface.name, interface.active, interface.module \n"
            "FROM interface \n"
            "WHERE interface.active IS true AND interface.module IN (%(module_1_1)s) AND interface.date_deleted IS NULL"
        )


class Test_ASUser_Get_Active_Document_Integrations(TestBase):
    def setup(self):
        self.load_query_instance(integrations)

    def test_get_active_document_integrations(self):
        integration_uuids = [uuid4(), uuid4()]
        document_integrations = [mock.MagicMock(), mock.MagicMock()]
        document_integrations[0].configure_mock(
            id=1,
            uuid=integration_uuids[0],
            name="Document Integration-1",
            active=True,
            module="xential",
        )
        document_integrations[1].configure_mock(
            id=2,
            uuid=integration_uuids[1],
            name="Document Integration-2",
            active=True,
            module="stuf_dcr",
        )
        self.session.execute().fetchall.return_value = document_integrations
        self.session.execute.reset_mock()
        result = self.qry.get_active_integrations_for_document()

        assert len(result) == 2
        assert result[0].entity_id == integration_uuids[0]

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT interface.id, interface.uuid, interface.name, interface.active, interface.module \n"
            "FROM interface \n"
            "WHERE interface.active IS true AND interface.module IN (%(module_1_1)s, %(module_1_2)s) AND interface.date_deleted IS NULL"
        )
