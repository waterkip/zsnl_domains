# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import json
import pydantic
import pytest
from collections import namedtuple
from minty import entity, exceptions
from minty.cqrs import UserInfo, test
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sqlalchemy_pg
from typing import List, Optional, TypedDict
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management import entities
from zsnl_domains.database import schema


class CustomObjectCaseRelation(TypedDict):
    uuid: UUID
    source_custom_field_uuid: Optional[UUID]


class CustomObjectTestBase(test.TestBase):
    _fetchone_cache = {}

    def _grep_query_from_calls(self, search_string):
        for call in self.session.execute.call_args_list:
            query = call[0][0].compile(dialect=sqlalchemy_pg.dialect())

            if search_string in str(query):
                return query

        return None

    def get_object_type_params(self, ot_uuid=None, has_title=True):
        if ot_uuid is None:
            ot_uuid = uuid4()
        return {
            "uuid": ot_uuid,
            "name": "Animal",
            "title": "[]",
            "subtitle": "[]",
            "external_reference": " ztc_onderwerp is '[[ j('$.attributes.custom_fields.ztc_onderwerp.value') ]]' ztc_document is '[[ j('$.attributes.custom_fields.ztc_document.value') ]]' ",
            "status": "active",
            "version": 1,
            "date_created": datetime.datetime.now(datetime.timezone.utc),
            "last_modified": datetime.datetime.now(datetime.timezone.utc),
            "version_independent_uuid": uuid4(),
            "is_active_version": True,
            "custom_field_definition": {
                "custom_fields": [
                    {
                        "label": "Onderwerp",
                        "custom_field_type": "text",
                        "magic_string": "ztc_onderwerp",
                        "name": "ZTC Onderwerp",
                        "is_required": True,
                    },
                    {
                        "label": "Document",
                        "custom_field_type": "relationship",
                        "magic_string": "ztc_document",
                        "name": "ZTC Document",
                    },
                    {
                        "label": "Address",
                        "custom_field_type": "address_v2",
                        "magic_string": "ztc_address_v2",
                        "name": "ZTC Address",
                    },
                    {
                        "label": "Custom Object",
                        "custom_field_type": "relationship",
                        "magic_string": "custom_object_relation_field",
                        "name": "ZTC custom_object relation",
                        "is_required": False,
                        "custom_field_specification": {
                            "uuid": uuid4(),
                            "name": "relation_field",
                            "type": "custom_object",
                        },
                        "attribute_uuid": uuid4(),
                        "multiple_values": True,
                    },
                ]
            },
            "audit_log": {
                "updated_components": ["authorizations"],
                "description": "Revoked access from Frits",
            },
        }

    def get_object_type(self, ot_uuid: Optional[UUID] = None):
        return entities.CustomObjectType(
            **self.get_object_type_params(ot_uuid or uuid4())
        )

    def load_row_into_fetchone(
        self,
        uuid=None,
        version_independent_uuid=None,
        related_cases: Optional[List[CustomObjectCaseRelation]] = None,
        related_documents: Optional[List[UUID]] = None,
        related_subjects: Optional[List[UUID]] = None,
        related_custom_objects: Optional[List[UUID]] = None,
        status="active",
    ):
        dbparams = {
            "version_independent_uuid": version_independent_uuid or uuid4(),
            "uuid": uuid or uuid4(),
            "object_type_name": "Animal",
            "title": "[[ object.name ]]",
            "subtitle": "[[object.subtitle]]",
            "external_reference": "ztc_onderwerp is Cat",
            "status": status,
            "version": 1,
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
            "archive_status": "archived",
            "archive_ground": "Custom text",
            "archive_retention": 5,
            "is_active_version": 0,
            "cases": related_cases or [],
            "documents": related_documents or [],
            "subjects": related_subjects or [],
            "custom_objects": related_custom_objects or [],
            "date_created": datetime.datetime.now(datetime.timezone.utc),
            "last_modified": datetime.datetime.now(datetime.timezone.utc),
            "authorizations": ["admin", "read"],
            "ot_version_independent_uuid": uuid4(),
            "ot_uuid": uuid4(),
            "ot_name": "Animal",
            "ot_title": "[]",
            "ot_subtitle": "[]",
            "ot_external_reference": "ztc_onderwerp is [[ j('$.attributes.custom_fields.ztc_onderwerp.value') ]]",
            "ot_audit_log": {
                "updated_components": ["authorizations"],
                "description": "Revoked access from Frits",
            },
            "ot_status": "active",
            "ot_version": 1,
            "ot_custom_field_definition": {
                "custom_fields": [
                    {
                        "label": "Onderwerp",
                        "custom_field_type": "text",
                        "magic_string": "ztc_onderwerp",
                        "name": "ZTC Onderwerp",
                        "is_required": True,
                    }
                ]
            },
            "ot_date_created": datetime.datetime.now(datetime.timezone.utc),
            "ot_last_modified": datetime.datetime.now(datetime.timezone.utc),
            "ot_date_deleted": None,
            "ot_is_active_version": 1,  # Todo
        }

        try:
            self.session.execute().fetchone.return_value = (
                self._fetchone_cache[uuid][version_independent_uuid][0]
            )
        except Exception:
            rv = mock.MagicMock(**dbparams)
            self.session.execute().fetchone.return_value = rv
            self._fetchone_cache[uuid] = {}
            self._fetchone_cache[uuid][version_independent_uuid] = [
                rv,
                dbparams,
            ]

        return self._fetchone_cache[uuid][version_independent_uuid][1]

    def load_rows_into_fetchall(self):
        self.load_row_into_fetchone()
        self.session.execute().fetchall.return_value = [
            self.session.execute().fetchone(),
            self.session.execute().fetchone(),
        ]


class TestAs_An_Assignee_I_Want_To_Get_An_Object(CustomObjectTestBase):
    def setup(self):
        self.load_query_instance(case_management)
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_check_entity_fields(self):
        fielddata = {
            "name": "Animal",
            "entity_id": uuid4(),
            "entity_meta_summary": "Cat",
            "title": "Cat",
            "subtitle": "Cow",
            "uuid": uuid4(),
            "status": "active",
            "version": 1,
            "date_created": datetime.datetime.now(datetime.timezone.utc),
            "last_modified": datetime.datetime.now(datetime.timezone.utc),
            "version_independent_uuid": uuid4(),
            "is_active_version": True,
            "custom_fields": {
                "ztc_onderwerp": ["This is a cat"],
                "ztc_startdate": ["2020-03-10T08:13:52.918Z"],
            },
            "archive_metadata": {
                "status": "archived",
                "ground": "Custom text",
                "retention": 5,
            },
            "custom_object_type": self.get_object_type_params(),
        }

        co = entities.custom_object.CustomObject.parse_obj(fielddata)

        for key in fielddata.keys():
            if isinstance(getattr(co, key), entity.Entity):
                continue

            assert getattr(co, key) == fielddata[key]

    def test_query_to_our_orm(self):
        co_uuid = uuid4()

        dbparams = self.load_row_into_fetchone(co_uuid)
        co = self.qry.get_custom_object_by_uuid(uuid=str(co_uuid))

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert query.params["uuid_1"] == self.qry.user_info.user_uuid
        assert query.params["uuid_2"] == co_uuid

        assert co.name == dbparams["object_type_name"]
        assert co.title == dbparams["title"]
        assert co.external_reference == dbparams["external_reference"]
        assert co.status == dbparams["status"]
        assert co.version == dbparams["version"]
        assert co.is_active_version == dbparams["is_active_version"]
        assert co.custom_fields == dbparams["custom_fields"]
        assert co.entity_id
        assert co.custom_object_type.entity_id
        assert co.entity_meta_summary
        assert co.entity_meta_authorizations == {"admin", "read"}

        assert co.custom_object_type.entity_id == co.custom_object_type.uuid

        assert co.archive_metadata.dict() == {
            "status": dbparams["archive_status"],
            "ground": dbparams["archive_ground"],
            "retention": dbparams["archive_retention"],
        }

    def test_get_custom_object_with_relationships(self):
        co_uuid = uuid4()
        related_cases = [
            CustomObjectCaseRelation(
                uuid=uuid4(), source_custom_field_uuid=None
            )
        ]
        related_documents = [uuid4()]
        related_subjects = [uuid4()]
        related_custom_objects = [uuid4()]

        dbparams = self.load_row_into_fetchone(
            co_uuid,
            related_cases=related_cases,
            related_documents=related_documents,
            related_subjects=related_subjects,
            related_custom_objects=related_custom_objects,
        )
        co = self.qry.get_custom_object_by_uuid(uuid=str(co_uuid))

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert query.params["uuid_1"] == self.qry.user_info.user_uuid
        assert query.params["uuid_2"] == co_uuid
        assert co.name == dbparams["object_type_name"]

        assert co.cases[0].entity_id == related_cases[0]["uuid"]
        assert co.documents[0].entity_id == related_documents[0]
        assert co.subjects[0].entity_id == related_subjects[0]
        assert co.custom_objects[0].entity_id == related_custom_objects[0]

    def test_unexisting_uuid_for_custom_object(self):
        custom_object_uuid = "c7d81da3-c28d-4910-a427-48dfa0e6e3e8"
        self.session.execute().fetchone.return_value = None

        custom_object = None
        with pytest.raises(exceptions.NotFound) as excinfo:
            custom_object = self.qry.get_custom_object_by_uuid(
                uuid=custom_object_uuid
            )

        assert (
            "Could not find or rights are insufficient for 'custom_object' with uuid"
            in str(excinfo.value)
        )

        assert custom_object is None


class TestAs_An_Assignee_I_Want_To_Get_A_List_Of_Objects(CustomObjectTestBase):
    def setup(self):
        self.load_query_instance(case_management)
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_get_list_of_objects_by_case_uuid(self):
        self.load_rows_into_fetchall()

        case_uuid = uuid4()

        self.session.query().filter().one_or_none.return_value = (
            mock.MagicMock(id=1234)
        )

        custom_objects = self.qry.get_custom_objects(
            {"relationships.cases.id": case_uuid}
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert query.params["relationship_type_1"] == "case"
        assert isinstance(custom_objects, entity.EntityCollection)


class TestAs_An_Assignee_I_Want_To_Filter_A_List_Of_Objects(
    CustomObjectTestBase
):
    def setup(self):
        self.load_query_instance(case_management)
        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_filter_list_of_objects_by_title(self):
        self.load_rows_into_fetchall()
        custom_objects = self.qry.get_custom_objects(
            filter_params={"title": "bla"}
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert query.params["title_1"] == "%bla%"
        assert query.params["date_deleted_1"]
        assert isinstance(custom_objects, entity.EntityCollection)

    def test_filter_list_of_objects_by_external_reference(self):
        self.load_rows_into_fetchall()
        custom_objects = self.qry.get_custom_objects(
            filter_params={"attributes.external_reference": "This is subject"}
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert query.params["external_reference_1"] == "%This is subject%"
        assert query.params["date_deleted_1"]
        assert isinstance(custom_objects, entity.EntityCollection)


class TestAs_An_Assignee_I_Want_To_Create_An_Object(CustomObjectTestBase):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    def test_create_a_custom_object_with_faults(self, mock):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {"ztc_onderwerp": ["This is a subject"]},
        }

        mock.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        with pytest.raises(exceptions.ValidationError) as excinfo:
            wrong_fields = params.copy()
            wrong_fields["custom_fields"] = []

            self.cmd.create_custom_object(**wrong_fields)

        with pytest.raises(pydantic.ValidationError) as excinfo:
            wrong_fields = params.copy()
            wrong_fields["custom_fields"] = {"ztc_onderwerp": "wrong value"}

            self.cmd.create_custom_object(**wrong_fields)

        assert "value is not a valid dict" in str(excinfo.value)

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    def test_create_a_basic_object(self, mock):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {"ztc_onderwerp": {"value": "This is a subject"}},
        }

        mock.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        # with pytest.raises(exceptions.NotFound) as excinfo:
        self.cmd.create_custom_object(**params)

        # Expect a select and a delete
        call_list = self.session.execute.call_args_list
        assert call_list[0][0][0].__class__.__name__ == "Select"
        assert call_list[1][0][0].__class__.__name__ == "Insert"
        assert call_list[2][0][0].__class__.__name__ == "Insert"
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Update"

        query = call_list[0][0][0].compile()
        assert query.params["uuid_1"] == str(params["custom_object_type_uuid"])

        query = call_list[1][0][0].compile()
        for key, value in {
            "archive_ground": "The Force",
            "archive_retention": 10,
            "archive_status": "archived",
        }.items():
            assert query.params[key] == value

        assert query.params["custom_fields"]["ztc_onderwerp"] == {
            "value": "This is a subject",
            "type": "text",
        }

        query = call_list[2][0][0].compile()
        assert set(query.params.items()).issuperset(
            set(
                dict(
                    {
                        "date_deleted": None,
                        "uuid": str(params["uuid"]),
                        "status": params["status"],
                        "title": "[]",
                        "external_reference": " ztc_onderwerp is 'This is a subject' ztc_document is '' ",
                        "version": 1,
                    }
                ).items()
            )
        )
        assert query.params["date_created"]
        assert query.params["last_modified"]

        query = call_list[3][0][0].compile()
        assert query.params["uuid"]

        query = call_list[4][0][0].compile()
        assert query.params["custom_object_id"]
        assert query.params["id_1"]

    def test_create_custom_object_failed_when_user_has_no_rights(self):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {"ztc_onderwerp": {"value": "This is a subject"}},
        }

        self.session.execute().fetchone.return_value = None
        self.cmd.user_info.permissions["admin"] = False

        with pytest.raises(exceptions.NotFound) as excinfo:
            self.cmd.create_custom_object(**params)

        assert excinfo.value.args == (
            "Could not find or rights are insufficient for object type by uuid {}".format(
                params["custom_object_type_uuid"]
            ),
            "case_management/custom_object/object_create/unauthorised",
        )


class TestAs_An_Assignee_I_Want_To_Relate_An_Object_To_A_Case(
    CustomObjectTestBase
):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository._process_relationships"
    )
    def test_create_a_basic_object_with_cases(
        self, mock_relationships, mock_by_uuid
    ):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {"ztc_onderwerp": {"value": "This is a subject"}},
            "related_to": {"case": [str(uuid4())]},
        }

        mock_by_uuid.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        def create_new_relation(*args, **kwargs):
            kwargs["new_relationships"]["insert"].append(
                {
                    schema.CustomObjectRelationship.custom_object_id: 21,
                    schema.CustomObjectRelationship.custom_object_version_id: 33,
                    schema.CustomObjectRelationship.relationship_type: "case",
                    schema.CustomObjectRelationship.related_case_id: 44,
                    schema.CustomObjectRelationship.related_uuid: "8f5cceea-11f9-4ef7-b799-09574720870d",
                    schema.CustomObjectRelationship.source_custom_field_type_id: None,
                }
            )

        mock_relationships.side_effect = create_new_relation

        # with pytest.raises(exceptions.NotFound) as excinfo:
        self.cmd.create_custom_object(**params)

        # Expect a select and a delete
        call_list = self.session.execute.call_args_list
        assert call_list[0][0][0].__class__.__name__ == "Select"
        assert call_list[1][0][0].__class__.__name__ == "Insert"
        assert call_list[2][0][0].__class__.__name__ == "Insert"
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Update"
        assert call_list[6][0][0].__class__.__name__ == "Insert"

        query = call_list[0][0][0].compile()
        assert query.params["uuid_1"] == str(params["custom_object_type_uuid"])

        query = call_list[2][0][0].compile()
        assert set(query.params.items()).issuperset(
            set(
                dict(
                    {
                        "date_deleted": None,
                        "uuid": str(params["uuid"]),
                        "status": params["status"],
                        "title": "[]",
                        "version": 1,
                    }
                ).items()
            )
        )
        assert query.params["date_created"]
        assert query.params["last_modified"]

        query = call_list[3][0][0].compile()
        assert query.params["uuid"]

        query = call_list[4][0][0].compile()
        assert query.params["custom_object_id"]
        assert query.params["id_1"]

        query = call_list[6][0][0].compile(dialect=sqlalchemy_pg.dialect())

        assert query.params["custom_object_id"]
        assert query.params["custom_object_version_id"]
        assert query.params["related_case_id"]


class TestAs_An_Assignee_I_Want_To_Relate_An_Object_To_An_Object(
    CustomObjectTestBase
):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository._process_relationships"
    )
    def test_create_a_basic_object_with_objects(
        self, mock_relationships, mock_by_uuid
    ):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {"ztc_onderwerp": {"value": "This is a subject"}},
            "related_to": {"custom_object": [str(uuid4())]},
        }

        mock_by_uuid.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        def create_new_relation(*args, **kwargs):
            kwargs["new_relationships"]["insert"].append(
                {
                    schema.CustomObjectRelationship.custom_object_id: 21,
                    schema.CustomObjectRelationship.custom_object_version_id: 33,
                    schema.CustomObjectRelationship.relationship_type: "custom_object",
                    schema.CustomObjectRelationship.related_custom_object_id: 44,
                    schema.CustomObjectRelationship.related_uuid: "5e6cf1a0-384c-40fe-bf7a-4ecb159a7a5a",
                    schema.CustomObjectRelationship.source_custom_field_type_id: None,
                }
            )

        mock_relationships.side_effect = create_new_relation

        # with pytest.raises(exceptions.NotFound) as excinfo:
        self.cmd.create_custom_object(**params)

        # Expect a select and a delete
        call_list = self.session.execute.call_args_list
        assert call_list[0][0][0].__class__.__name__ == "Select"
        assert call_list[1][0][0].__class__.__name__ == "Insert"
        assert call_list[2][0][0].__class__.__name__ == "Insert"
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Update"
        assert call_list[6][0][0].__class__.__name__ == "Insert"

        query = call_list[0][0][0].compile()
        assert query.params["uuid_1"] == str(params["custom_object_type_uuid"])

        query = call_list[2][0][0].compile()
        assert set(query.params.items()).issuperset(
            set(
                dict(
                    {
                        "date_deleted": None,
                        "uuid": str(params["uuid"]),
                        "status": params["status"],
                        "title": "[]",
                        "version": 1,
                    }
                ).items()
            )
        )
        assert query.params["date_created"]
        assert query.params["last_modified"]

        query = call_list[3][0][0].compile()
        assert query.params["uuid"]

        query = call_list[4][0][0].compile()
        assert query.params["custom_object_id"]
        assert query.params["id_1"]

        query = call_list[6][0][0].compile(dialect=sqlalchemy_pg.dialect())

        assert query.params["custom_object_id"]
        assert query.params["custom_object_version_id"]
        assert query.params["related_custom_object_id"]

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository._process_relationships"
    )
    def test_create_a_basic_object_with_objects_in_relation_field(
        self, mock_relationships, mock_by_uuid
    ):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subject"},
                "custom_object_relation_field": {
                    "value": [
                        {
                            "value": "1f2f76b8-f2ee-45ea-9281-650b8faab314",
                            "specifics": {
                                "relationship_type": "custom_object",
                                "metadata": {
                                    "summary": "summary of this object"
                                },
                            },
                        }
                    ]
                },
            },
            "related_to": {"custom_object": [str(uuid4())]},
        }

        mock_by_uuid.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        def create_new_relation(*args, **kwargs):
            kwargs["new_relationships"]["insert"].append(
                {
                    schema.CustomObjectRelationship.custom_object_id: 21,
                    schema.CustomObjectRelationship.custom_object_version_id: 33,
                    schema.CustomObjectRelationship.relationship_type: "custom_object",
                    schema.CustomObjectRelationship.related_custom_object_id: 44,
                    schema.CustomObjectRelationship.related_uuid: "5e6cf1a0-384c-40fe-bf7a-4ecb159a7a5a",
                    schema.CustomObjectRelationship.source_custom_field_type_id: None,
                }
            )

        mock_relationships.side_effect = create_new_relation

        # with pytest.raises(exceptions.NotFound) as excinfo:
        self.cmd.create_custom_object(**params)

        # Expect a select and a delete
        call_list = self.session.execute.call_args_list
        assert call_list[0][0][0].__class__.__name__ == "Select"
        assert call_list[1][0][0].__class__.__name__ == "Insert"
        assert call_list[2][0][0].__class__.__name__ == "Insert"
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Update"
        assert call_list[6][0][0].__class__.__name__ == "Insert"

        query = call_list[0][0][0].compile()
        assert query.params["uuid_1"] == str(params["custom_object_type_uuid"])

        query = call_list[2][0][0].compile()
        assert set(query.params.items()).issuperset(
            set(
                dict(
                    {
                        "date_deleted": None,
                        "uuid": str(params["uuid"]),
                        "status": params["status"],
                        "title": "[]",
                        "version": 1,
                    }
                ).items()
            )
        )
        assert query.params["date_created"]
        assert query.params["last_modified"]

        query = call_list[3][0][0].compile()
        assert query.params["uuid"]

        query = call_list[4][0][0].compile()
        assert query.params["custom_object_id"]
        assert query.params["id_1"]

        query = call_list[6][0][0].compile(dialect=sqlalchemy_pg.dialect())

        assert query.params["custom_object_id"]
        assert query.params["custom_object_version_id"]
        assert query.params["related_custom_object_id"]

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository._process_relationships"
    )
    def test_create_a_basic_object_with_None_in_relation_field(
        self, mock_relationships, mock_by_uuid
    ):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subject"},
                "custom_object_relation_field": None,
            },
            "related_to": {"custom_object": [str(uuid4())]},
        }

        mock_by_uuid.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        def create_new_relation(*args, **kwargs):
            kwargs["new_relationships"]["insert"].append(
                {
                    schema.CustomObjectRelationship.custom_object_id: 21,
                    schema.CustomObjectRelationship.custom_object_version_id: 33,
                    schema.CustomObjectRelationship.relationship_type: "custom_object",
                    schema.CustomObjectRelationship.related_custom_object_id: 44,
                    schema.CustomObjectRelationship.related_uuid: "5e6cf1a0-384c-40fe-bf7a-4ecb159a7a5a",
                    schema.CustomObjectRelationship.source_custom_field_type_id: None,
                }
            )

        mock_relationships.side_effect = create_new_relation

        # with pytest.raises(exceptions.NotFound) as excinfo:
        self.cmd.create_custom_object(**params)

        # Expect a select and a delete
        call_list = self.session.execute.call_args_list
        assert call_list[0][0][0].__class__.__name__ == "Select"
        assert call_list[1][0][0].__class__.__name__ == "Insert"
        assert call_list[2][0][0].__class__.__name__ == "Insert"
        assert call_list[3][0][0].__class__.__name__ == "Insert"
        assert call_list[4][0][0].__class__.__name__ == "Update"
        assert call_list[6][0][0].__class__.__name__ == "Insert"

        query = call_list[0][0][0].compile()
        assert query.params["uuid_1"] == str(params["custom_object_type_uuid"])

        query = call_list[2][0][0].compile()
        assert set(query.params.items()).issuperset(
            set(
                dict(
                    {
                        "date_deleted": None,
                        "uuid": str(params["uuid"]),
                        "status": params["status"],
                        "title": "[]",
                        "version": 1,
                    }
                ).items()
            )
        )
        assert query.params["date_created"]
        assert query.params["last_modified"]

        query = call_list[3][0][0].compile()
        assert query.params["uuid"]

        query = call_list[4][0][0].compile()
        assert query.params["custom_object_id"]
        assert query.params["id_1"]

        query = call_list[6][0][0].compile(dialect=sqlalchemy_pg.dialect())

        assert query.params["custom_object_id"]
        assert query.params["custom_object_version_id"]
        assert query.params["related_custom_object_id"]


class TestAs_An_Assignee_I_Want_To_Relate_A_Document_To_A_Case(
    CustomObjectTestBase
):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository._process_relationships"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    def test_create_a_basic_object_with_document(
        self, find_by_uuid_mock, relationship_mock
    ):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subject"},
                "ztc_document": {
                    "value": uuid4(),
                    "specifics": {"relationship_type": "document"},
                },
            },
        }

        find_by_uuid_mock.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        def create_new_relation(*args, **kwargs):
            kwargs["new_relationships"]["insert"].append(
                {
                    schema.CustomObjectRelationship.custom_object_id: 21,
                    schema.CustomObjectRelationship.custom_object_version_id: 33,
                    schema.CustomObjectRelationship.relationship_type: "document",
                    schema.CustomObjectRelationship.related_document_id: 44,
                    schema.CustomObjectRelationship.related_uuid: "26d0d1e0-cd7a-4f4b-935c-d5e66f4be62b",
                    schema.CustomObjectRelationship.source_custom_field_type_id: None,
                }
            )

        relationship_mock.side_effect = create_new_relation

        # with pytest.raises(exceptions.NotFound) as excinfo:
        self.cmd.create_custom_object(**params)

        query = self._grep_query_from_calls(
            "INSERT INTO custom_object_relationship"
        )

        assert query.params["relationship_type"] == "document"


class TestAs_An_Assignee_I_Want_To_Update_An_Object(CustomObjectTestBase):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_raise_on_checks(self):
        params = {
            "existing_uuid": str(uuid4()),
            "uuid": str(uuid4()),
            "status": "active",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
        }

        self.session.execute().fetchone.return_value = None

        with pytest.raises(exceptions.NotFound) as excinfo:
            self.cmd.update_custom_object(**params)

        assert (
            "Could not find or rights are insufficient for 'custom_object'"
            in str(excinfo.value)
        )

    def test_no_permission_to_update_inactive(self):
        params = {
            "existing_uuid": str(uuid4()),
            "uuid": str(uuid4()),
            "status": "active",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
        }

        self.cmd.user_info.permissions["admin"] = False
        self.load_row_into_fetchone(params["existing_uuid"], status="inactive")

        with pytest.raises(exceptions.Forbidden):
            self.cmd.update_custom_object(**params)

    def test_admin_permission_to_update_inactive(self):
        params = {
            "existing_uuid": str(uuid4()),
            "uuid": str(uuid4()),
            "status": "active",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
        }

        self.cmd.user_info.permissions["admin"] = True
        self.load_row_into_fetchone(params["existing_uuid"], status="inactive")

        self.cmd.update_custom_object(**params)

    def test_update_a_basic_object(self):
        params = {
            "existing_uuid": str(uuid4()),
            "uuid": str(uuid4()),
            "status": "active",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
        }

        dbparams = self.load_row_into_fetchone(params["existing_uuid"])

        self.cmd.update_custom_object(**params)

        # Expect a select and a delete
        call_list = self.session.execute.call_args_list

        assert call_list[3][0][0].__class__.__name__ == "Select"
        assert call_list[4][0][0].__class__.__name__ == "Select"
        assert call_list[5][0][0].__class__.__name__ == "Insert"
        assert call_list[6][0][0].__class__.__name__ == "Insert"
        assert call_list[7][0][0].__class__.__name__ == "Select"
        assert call_list[8][0][0].__class__.__name__ == "Update"
        assert call_list[9][0][0].__class__.__name__ == "Update"

        query = call_list[3][0][0].compile()
        assert query.params["uuid_1"] == str(params["existing_uuid"])

        query = call_list[4][0][0].compile()
        assert query.params["uuid_1"] == str(dbparams["ot_uuid"])

        query = call_list[5][0][0].compile()

        assert query.params == {
            "archive_ground": "The Force22",
            "archive_retention": 10,
            "archive_status": "archived",
            "custom_fields": {
                "ztc_onderwerp": {
                    "value": "This is a subjectttt",
                    "type": "text",
                }
            },
        }

        query = call_list[6][0][0].compile()
        assert query.params["date_created"]
        assert query.params["last_modified"]

        query = call_list[7][0][0].compile()
        assert query.params["uuid_1"]

        query = call_list[8][0][0].compile()
        assert query.params["custom_object_version_id"]
        assert query.params["id_1"]

    def test_update_object_with_missing_field(self):
        params = {
            "existing_uuid": str(uuid4()),
            "uuid": str(uuid4()),
            "status": "active",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {"ztc_onderwerp": {"value": ""}},
        }

        with pytest.raises(exceptions.ValidationError) as excinfo:
            self.load_row_into_fetchone(params["existing_uuid"])
            self.cmd.update_custom_object(**params)

        assert excinfo.value.args == (
            "Custom fields 'ztc_onderwerp' is/are required",
            "custom_object/required_custom_fields_missing",
        )


class TestAs_An_Assignee_I_Want_To_Create_An_Object_with_Address_v2(
    CustomObjectTestBase
):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    def test_create_a_custom_object_with_address_2_with_faults(self, mock):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_address_v2": {
                    "value": {
                        "bag": {"type": "nummeraanduiding", "id": "0001"},
                        "address": {
                            "full": "Straatnaam 12A, 1234 AB Amsterdam"
                        },
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "type": "Feature",
                                    "properties": {},
                                    "geometry": {
                                        "type": "Point",
                                        "coordinates": [4.040148, 52.224801],
                                    },
                                }
                            ],
                        },
                    }
                },
            },
        }

        mock.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        # When the value for bag is not a dict
        with pytest.raises(pydantic.ValidationError) as excinfo:
            wrong_fields = params.copy()
            wrong_fields["custom_fields"] = {
                "ztc_address_v2": {
                    "value": {
                        "bag": "hello1234",
                        "address": {
                            "full": "Straatnaam 12A, 1234 AB Amsterdam"
                        },
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "type": "Feature",
                                    "properties": {},
                                    "geometry": {
                                        "type": "Point",
                                        "coordinates": [4.040148, 52.224801],
                                    },
                                }
                            ],
                        },
                    }
                }
            }

            self.cmd.create_custom_object(**wrong_fields)
        assert "value is not a valid dict" in str(excinfo.value)

        # When the bag id has non-digit charactres in it
        with pytest.raises(ValueError) as excinfo:
            wrong_fields = params.copy()
            wrong_fields["custom_fields"] = {
                "ztc_address_v2": {
                    "value": {
                        "bag": {
                            "type": "nummeraanduiding",
                            "id": "nummeraanduiding-hello1234",
                        },
                        "address": {
                            "full": "Straatnaam 12A, 1234 AB Amsterdam"
                        },
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "type": "Feature",
                                    "properties": {},
                                    "geometry": {
                                        "type": "Point",
                                        "coordinates": [4.040148, 52.224801],
                                    },
                                }
                            ],
                        },
                    }
                }
            }

            self.cmd.create_custom_object(**wrong_fields)
        assert (
            "Invalid bag id 'nummeraanduiding-hello1234'. Only digits are allowed for bag id."
            in str(excinfo.value)
        )

        # When the value for address is not a dict
        with pytest.raises(pydantic.ValidationError) as excinfo:
            wrong_fields = params.copy()
            wrong_fields["custom_fields"] = {
                "ztc_address_v2": {
                    "value": {
                        "bag": {"type": "nummeraanduiding", "id": "1234"},
                        "address": "Straatnaam 12A, 1234 AB Amsterdam",
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "type": "Feature",
                                    "properties": {},
                                    "geometry": {
                                        "type": "Point",
                                        "coordinates": [4.040148, 52.224801],
                                    },
                                }
                            ],
                        },
                    }
                }
            }

            self.cmd.create_custom_object(**wrong_fields)
        assert "value is not a valid dict" in str(excinfo.value)

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    def test_create_a_object_with_address_v2(self, mock):
        params = {
            "uuid": str(uuid4()),
            "status": "draft",
            "custom_object_type_uuid": str(uuid4()),
            "archive_metadata": {
                "ground": "The Force",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_address_v2": {
                    "value": {
                        "bag": {"type": "nummeraanduiding", "id": "0123"},
                        "address": {
                            "full": "Straatnaam 12A, 1234 AB Amsterdam"
                        },
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "type": "Feature",
                                    "properties": {},
                                    "geometry": {
                                        "type": "Point",
                                        "coordinates": [4.040148, 52.224801],
                                    },
                                }
                            ],
                        },
                    }
                },
                "ztc_onderwerp": {"value": "This is a subject"},
            },
        }

        mock.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        self.cmd.create_custom_object(**params)

        call_list = self.session.execute.call_args_list
        query = call_list[1][0][0].compile()
        assert query.params["custom_fields"]["ztc_address_v2"] == {
            "value": {
                "bag": {"type": "nummeraanduiding", "id": "123"},
                "address": {"full": "Straatnaam 12A, 1234 AB Amsterdam"},
                "geojson": {
                    "type": "FeatureCollection",
                    "features": [
                        {
                            "type": "Feature",
                            "properties": {},
                            "geometry": {
                                "type": "Point",
                                "coordinates": [4.040148, 52.224801],
                            },
                        }
                    ],
                },
            },
            "type": "address_v2",
        }


class TestAs_An_Assignee_I_Want_To_Delete_An_Object(CustomObjectTestBase):
    def setup(self):
        self.load_command_instance(case_management)

    def test_delete_object(self):

        object_uuid = str(uuid4())
        params = {"custom_object_uuid": object_uuid}
        self.session.execute().fetchone.return_value = None

        with pytest.raises(exceptions.NotFound):
            self.cmd.delete_custom_object(**params)

        self.load_row_into_fetchone(object_uuid)

        self.session.reset_mock()
        self.cmd.delete_custom_object(**params)

        self.assert_has_event_name("CustomObjectDeleted")

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 5

        select_statement = call_list[0][0][0]
        assert isinstance(select_statement, sql.Select)

        select_statement = call_list[1][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert str(compiled_select) == (
            "SELECT custom_object_version.custom_object_id \n"
            "FROM custom_object_version \n"
            "WHERE custom_object_version.uuid = %(uuid_1)s"
        )

        select_statement = call_list[2][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert str(compiled_select) == (
            "SELECT custom_object_version.custom_object_version_content_id \n"
            "FROM custom_object_version \n"
            "WHERE custom_object_version.custom_object_id = %(custom_object_id_1)s"
        )

        select_statement = call_list[3][0][0]
        assert isinstance(select_statement, sql.Delete)

        compiled_select = select_statement.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert str(compiled_select) == (
            "DELETE FROM custom_object WHERE custom_object.id = %(id_1)s"
        )

        select_statement = call_list[4][0][0]
        assert isinstance(select_statement, sql.dml.Delete)

    def test_delete_object_without_version(self):
        object_uuid = str(uuid4())
        params = {"custom_object_uuid": object_uuid}
        version_independent_uuid = uuid4()
        uuid = uuid4()
        dbparams = {
            "version_independent_uuid": version_independent_uuid,
            "uuid": uuid,
            "object_type_name": "Animal",
            "title": "[[ object.name ]]",
            "subtitle": "[[ object.subtitle ]]",
            "external_reference": "ztc_onderwerp is Cat",
            "status": "active",
            "version": 1,
            "custom_fields": {"ztc_onderwerp": ["Cat"]},
            "archive_status": "archived",
            "archive_ground": "Custom text",
            "archive_retention": 5,
            "is_active_version": 0,
            "cases": [],
            "documents": [],
            "date_created": datetime.datetime.now(datetime.timezone.utc),
            "last_modified": datetime.datetime.now(datetime.timezone.utc),
            "authorizations": None,
            "ot_version_independent_uuid": uuid4(),
            "ot_uuid": uuid4(),
            "ot_name": "Animal",
            "ot_title": "[]",
            "ot_subtitle": "[]",
            "ot_external_reference": "ztc_onderwerp is [[ j('$.attributes.custom_fields.ztc_onderwerp.value') ]]",
            "ot_audit_log": {
                "updated_components": ["authorizations"],
                "description": "Revoked access from Frits",
            },
            "ot_status": "active",
            "ot_version": 1,
            "ot_custom_field_definition": {
                "custom_fields": [
                    {
                        "label": "Onderwerp",
                        "custom_field_type": "text",
                        "magic_string": "ztc_onderwerp",
                        "name": "ZTC Onderwerp",
                    }
                ]
            },
            "ot_date_created": datetime.datetime.now(datetime.timezone.utc),
            "ot_last_modified": datetime.datetime.now(datetime.timezone.utc),
            "ot_date_deleted": None,
            "ot_is_active_version": 1,
        }
        rv = mock.MagicMock(**dbparams)
        self.session.execute().fetchone.side_effect = [
            rv,
            namedtuple("RowProxy", "custom_object_id")(custom_object_id=None),
        ]

        with pytest.raises(exceptions.NotFound) as excinfo:
            self.cmd.delete_custom_object(**params)

        assert 'Could not find "custom_object_version" with uuid' in str(
            excinfo.value
        )


class TestAs_An_Assignee_I_Want_To_Relate_CustomObject_to_Case(
    CustomObjectTestBase
):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )
        self.custom_object_uuid = str(uuid4())
        self.related_cases = [str(uuid4())]
        self.related_documents = [str(uuid4())]

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository._process_relationships"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    def test_relate_custom_object_to_case(
        self, mock_by_uuid, mock_relationships
    ):
        db_params = self.load_row_into_fetchone(self.custom_object_uuid)

        params = {
            "custom_object_uuid": self.custom_object_uuid,
            "existing_uuid": self.custom_object_uuid,
            "custom_object_type_uuid": str(db_params["ot_uuid"]),
            "status": "draft",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
            "cases": self.related_cases,
        }

        mock_by_uuid.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        def create_new_relation(*args, **kwargs):
            kwargs["new_relationships"]["insert"].append(
                {
                    schema.CustomObjectRelationship.custom_object_id: 21,
                    schema.CustomObjectRelationship.custom_object_version_id: 33,
                    schema.CustomObjectRelationship.relationship_type: "case",
                    schema.CustomObjectRelationship.related_case_id: 44,
                    schema.CustomObjectRelationship.related_uuid: params[
                        "cases"
                    ][0],
                    schema.CustomObjectRelationship.source_custom_field_type_id: None,
                }
            )

        mock_relationships.side_effect = create_new_relation

        self.cmd.relate_custom_object_to(**params)

        call_list = self.session.execute.call_args_list

        assert call_list[1][0][0].__class__.__name__ == "Select"
        query = call_list[1][0][0].compile()
        assert query.params["uuid_2"] == params["custom_object_uuid"]

        assert call_list[2][0][0].__class__.__name__ == "Select"
        query = call_list[2][0][0].compile()
        assert query.params["relationship_type_1"] == "case"
        assert query.params["relationship_type_2"] == "custom_object"
        assert query.params["uuid_1"] == params["custom_object_uuid"]
        assert query.params["uuid_2"] == params["custom_object_uuid"]

        assert call_list[3][0][0].__class__.__name__ == "Select"
        query = call_list[3][0][0].compile()
        assert query.params["uuid_1"] == params["custom_object_type_uuid"]

        assert call_list[4][0][0].__class__.__name__ == "Select"
        query = call_list[4][0][0].compile()
        assert query.params["uuid_1"] == params["custom_object_uuid"]

        assert call_list[5][0][0].__class__.__name__ == "Select"
        query = call_list[5][0][0].compile()
        assert query.params["uuid_1"] == db_params["version_independent_uuid"]

        assert call_list[6][0][0].__class__.__name__ == "CompoundSelect"
        query = call_list[6][0][0].compile()
        assert query.params["subject_type_1"] == "employee"

        assert call_list[7][0][0].__class__.__name__ == "Insert"
        query = call_list[7][0][0].compile()
        assert set(query.params.items()).issuperset(
            set(
                dict(
                    {
                        "custom_object_id": 21,
                        "custom_object_version_id": 33,
                        "relationship_type": "case",
                        "related_case_id": 44,
                        "related_uuid": params["cases"][0],
                    }
                ).items()
            )
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    def test_relate_custom_object_to_case_when_object_not_found(
        self, mock_by_uuid
    ):
        params = {
            "custom_object_uuid": self.custom_object_uuid,
            "existing_uuid": self.custom_object_uuid,
            "custom_object_type_uuid": str(uuid4()),
            "status": "draft",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
            "cases": self.related_cases,
        }
        mock_by_uuid.return_value = None
        with pytest.raises(exceptions.NotFound) as excinfo:
            self.cmd.relate_custom_object_to(**params)

        assert excinfo.value.args == (
            "Could not find or rights are insufficient for 'custom_object' with uuid {}".format(
                self.custom_object_uuid
            ),
            "case_management/custom_object/object/not_found",
        )


class TestAs_An_Assignee_I_Want_To_UnRelate_CustomObject_from_Case(
    CustomObjectTestBase
):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )
        self.custom_object_uuid = str(uuid4())
        self.related_cases = [
            CustomObjectCaseRelation(
                uuid=uuid4(), source_custom_field_uuid=None
            )
        ]

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository._process_relationships"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_uuid"
    )
    def test_unrelate_custom_object_to_case(
        self, mock_by_uuid, mock_relationships
    ):
        db_params = self.load_row_into_fetchone(
            self.custom_object_uuid, related_cases=self.related_cases
        )
        params = {
            "custom_object_uuid": self.custom_object_uuid,
            "existing_uuid": self.custom_object_uuid,
            "custom_object_type_uuid": str(db_params["ot_uuid"]),
            "status": "draft",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
            "cases": [str(rc["uuid"]) for rc in self.related_cases],
        }

        mock_by_uuid.return_value = self.get_object_type(
            params["custom_object_type_uuid"]
        )

        self.cmd.unrelate_custom_object_from(**params)

        call_list = self.session.execute.call_args_list

        assert call_list[1][0][0].__class__.__name__ == "Select"
        query = call_list[1][0][0].compile()
        assert query.params["uuid_1"] == self.cmd.user_info.user_uuid
        assert query.params["uuid_2"] == params["custom_object_uuid"]

        assert call_list[2][0][0].__class__.__name__ == "Select"
        query = call_list[2][0][0].compile()
        assert query.params["relationship_type_1"] == "case"
        assert query.params["relationship_type_2"] == "custom_object"
        assert query.params["uuid_1"] == params["custom_object_uuid"]
        assert query.params["uuid_2"] == params["custom_object_uuid"]

        assert call_list[3][0][0].__class__.__name__ == "Select"
        query = call_list[3][0][0].compile()
        assert query.params["uuid_1"] == params["custom_object_type_uuid"]

        assert call_list[4][0][0].__class__.__name__ == "Select"
        query = call_list[4][0][0].compile()
        assert query.params["uuid_1"] == params["custom_object_uuid"]

        assert call_list[5][0][0].__class__.__name__ == "Select"
        query = call_list[5][0][0].compile()
        assert query.params["uuid_1"] == db_params["version_independent_uuid"]

        assert call_list[6][0][0].__class__.__name__ == "CompoundSelect"
        query = call_list[6][0][0].compile()
        assert query.params["subject_type_1"] == "employee"

    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    def test_un_relate_custom_object_from_when_object_not_found(
        self, mock_by_uuid
    ):
        params = {
            "custom_object_uuid": self.custom_object_uuid,
            "existing_uuid": self.custom_object_uuid,
            "custom_object_type_uuid": str(uuid4()),
            "status": "draft",
            "archive_metadata": {
                "ground": "The Force22",
                "status": "archived",
                "retention": 10,
            },
            "custom_fields": {
                "ztc_onderwerp": {"value": "This is a subjectttt"}
            },
            "cases": [str(rc["uuid"]) for rc in self.related_cases],
        }
        mock_by_uuid.return_value = None
        with pytest.raises(exceptions.NotFound) as excinfo:
            self.cmd.unrelate_custom_object_from(**params)

        assert excinfo.value.args == (
            "Could not find or rights are insufficient for 'custom_object' with uuid {}".format(
                self.custom_object_uuid
            ),
            "case_management/custom_object/object/not_found",
        )


class TestAs_An_Assignee_I_Want_To_Create_Or_Update_CustomObject_Based_On_A_Phase_Action_Event(
    CustomObjectTestBase
):
    def setup(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )
        self.custom_object_uuid = str(uuid4())
        self.related_cases = [uuid4()]

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_version_independent_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository.find_case_type_version_by_uuid"
    )
    def test_custom_object_action_trigger_create(
        self,
        case_type_version_mock,
        custom_object_by_uuid,
        custom_object_type_uuid,
        mock_case_by_uuid,
    ):
        relation_field = "my_relation_field"
        params = {
            "case_uuid": str(uuid4()),
            "action_type": "create",
            "relation_field_magic_string": relation_field,
            "custom_object_fields": {
                "field_1": "value_1",
                "field_2": "value_2",
            },
            "status": True,
        }
        case_mock = mock.MagicMock()
        mock_case_by_uuid.return_value = case_mock
        custom_object_type = mock.MagicMock(uuid=uuid4())
        custom_object_type_uuid.return_value = custom_object_type
        self.cmd.create_custom_object = mock.MagicMock()
        custom_object = mock.MagicMock(
            title="New title",
            subtitle="New subtitle",
            version_independent_uuid=uuid4(),
        )
        custom_object_by_uuid.return_value = custom_object

        self.cmd.custom_object_action_trigger(**params)

        # verify if object creation is called with the correct parameters
        self.cmd.create_custom_object.assert_called_with(
            uuid=mock.ANY,
            custom_object_type_uuid=str(custom_object_type.uuid),
            custom_fields={
                "field_1": {"value": "value_1"},
                "field_2": {"value": "value_2"},
            },
            status="active",
        )

        # verify updating the relation attribute value
        case_mock.update_custom_field.assert_called_with(
            magic_string=relation_field,
            new_value={
                "value": [
                    {
                        "type": "relationship",
                        "value": str(custom_object.version_independent_uuid),
                        "specifics": {
                            "relationship_type": "custom_object",
                            "metadata": {
                                "description": "New subtitle",
                                "summary": "New title",
                            },
                        },
                    }
                ],
                "specifics": None,
                "type": "relationship",
            },
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_version_independent_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository.find_case_type_version_by_uuid"
    )
    def test_custom_object_action_trigger_create_with_existing_relation(
        self,
        case_type_version_mock,
        custom_object_by_uuid,
        custom_object_type_uuid,
        mock_case_by_uuid,
    ):
        relation_field = "my_relation_field"
        params = {
            "case_uuid": str(uuid4()),
            "action_type": "create",
            "relation_field_magic_string": relation_field,
            "custom_object_fields": {
                "field_1": "value_1",
                "field_2": "value_2",
                "address_v2_field": json.dumps(
                    {
                        "bag": {
                            "type": "nummeraanduiding",
                            "id": "nummeraanduiding-0268200000036925",
                        },
                        "address": {"full": "Ubbergseweg 28, 6522KJ Nijmegen"},
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "geometry": {
                                        "coordinates": [
                                            5.87445739,
                                            51.84730048,
                                        ],
                                        "type": "Point",
                                    },
                                    "type": "Feature",
                                    "properties": {},
                                }
                            ],
                        },
                    }
                ),
                "geojson_field": json.dumps(
                    {
                        "value": [
                            {
                                "type": "FeatureCollection",
                                "features": [
                                    {
                                        "geometry": {
                                            "type": "Polygon",
                                            "coordinates": [
                                                [
                                                    [4.853165, 52.214663],
                                                    [4.853165, 52.306397],
                                                    [5.187795, 52.306397],
                                                    [5.187795, 52.214663],
                                                    [4.853165, 52.214663],
                                                ]
                                            ],
                                        },
                                        "properties": {},
                                        "type": "Feature",
                                    },
                                ],
                            }
                        ]
                    }
                ),
                "a_relation_field": json.dumps(
                    {
                        "value": [
                            {
                                "value": "375c8acc-e876-40e7-a743-4ef6b66b6fa3",
                                "specifics": {
                                    "relationship_type": "custom_object",
                                    "metadata": {
                                        "summary": "titel: straatje 222",
                                        "description": "subtitel : straatje",
                                    },
                                },
                            },
                            {
                                "value": "9a11a1bc-4864-4eed-a49b-69d35e6ca32f",
                                "specifics": {
                                    "relationship_type": "custom_object",
                                    "metadata": {
                                        "summary": "titel: straatje 1234",
                                        "description": "subtitel : straatje",
                                    },
                                },
                            },
                        ]
                    },
                ),
                "a_subject_relation_field": json.dumps(
                    {
                        "value": "655a532b-aa08-4f6f-a175-d39567b6a30b",
                        "type": "relationship",
                        "specifics": {
                            "relationship_type": "person",
                            "metadata": {
                                "description": "Disney street 145",
                                "summary": "Micky Mouse",
                            },
                        },
                    }
                ),
                "empty_relation_contact": " ",
            },
            "status": True,
        }
        case_mock = mock.MagicMock()

        def my_side_effect(*args, **kwargs):
            if args[0] == "relationship":
                return {
                    "a_relation_field": mock.MagicMock(),
                    "a_subject_relation_field": mock.MagicMock(),
                    "empty_relation_contact": mock.MagicMock(),
                    relation_field: mock.MagicMock(),
                }
            elif args[0] == "address_v2":
                return {"address_v2_field": None}
            elif args[0] == "geojson":
                return {"geojson_field": None}

        case_type_version_mock().get_custom_fields_of_type.side_effect = (
            my_side_effect
        )
        mock_case_by_uuid.return_value = case_mock
        custom_object_type = mock.MagicMock(uuid=uuid4())
        custom_object_type_uuid.return_value = custom_object_type
        self.cmd.create_custom_object = mock.MagicMock()
        custom_object = mock.MagicMock(
            title="New title",
            subtitle="New subtitle",
            version_independent_uuid=uuid4(),
        )
        custom_object_by_uuid.return_value = custom_object
        case_mock.custom_fields = {
            relation_field: {
                "type": "relationship",
                "value": {
                    "value": [
                        {
                            "type": "relationship",
                            "value": "8e118737-0d65-4713-ad58-52dff8cea5ef",
                            "specifics": {
                                "relationship_type": "custom_object",
                                "metadata": {
                                    "description": "exising_object_subtitle",
                                    "summary": "existing_object_title",
                                },
                            },
                        }
                    ],
                    "specifics": None,
                    "type": "relationship",
                },
            }
        }

        self.cmd.custom_object_action_trigger(**params)

        # verify if object creation is called with the correct parameters
        self.cmd.create_custom_object.assert_called_with(
            uuid=mock.ANY,
            custom_object_type_uuid=str(custom_object_type.uuid),
            custom_fields={
                "field_1": {"value": "value_1"},
                "field_2": {"value": "value_2"},
                "address_v2_field": {
                    "value": {
                        "bag": {
                            "type": "nummeraanduiding",
                            "id": "nummeraanduiding-0268200000036925",
                        },
                        "address": {"full": "Ubbergseweg 28, 6522KJ Nijmegen"},
                        "geojson": {
                            "type": "FeatureCollection",
                            "features": [
                                {
                                    "geometry": {
                                        "coordinates": [
                                            5.87445739,
                                            51.84730048,
                                        ],
                                        "type": "Point",
                                    },
                                    "type": "Feature",
                                    "properties": {},
                                }
                            ],
                        },
                    }
                },
                "geojson_field": {
                    "value": {
                        "value": [
                            {
                                "type": "FeatureCollection",
                                "features": [
                                    {
                                        "geometry": {
                                            "type": "Polygon",
                                            "coordinates": [
                                                [
                                                    [4.853165, 52.214663],
                                                    [4.853165, 52.306397],
                                                    [5.187795, 52.306397],
                                                    [5.187795, 52.214663],
                                                    [4.853165, 52.214663],
                                                ]
                                            ],
                                        },
                                        "properties": {},
                                        "type": "Feature",
                                    }
                                ],
                            }
                        ]
                    }
                },
                "a_relation_field": {
                    "value": [
                        {
                            "value": "375c8acc-e876-40e7-a743-4ef6b66b6fa3",
                            "specifics": {
                                "relationship_type": "custom_object",
                                "metadata": {
                                    "summary": "titel: straatje 222",
                                    "description": "subtitel : straatje",
                                },
                            },
                        },
                        {
                            "value": "9a11a1bc-4864-4eed-a49b-69d35e6ca32f",
                            "specifics": {
                                "relationship_type": "custom_object",
                                "metadata": {
                                    "summary": "titel: straatje 1234",
                                    "description": "subtitel : straatje",
                                },
                            },
                        },
                    ]
                },
                "a_subject_relation_field": {
                    "value": "655a532b-aa08-4f6f-a175-d39567b6a30b",
                    "type": "relationship",
                    "specifics": {
                        "relationship_type": "subject",
                        "metadata": {
                            "description": "Disney street 145",
                            "summary": "Micky Mouse",
                        },
                    },
                },
                "empty_relation_contact": None,
            },
            status="active",
        )

        # verify updating the relation attribute value
        case_mock.update_custom_field.assert_called_with(
            magic_string=relation_field,
            new_value={
                "value": [
                    {
                        "type": "relationship",
                        "value": "8e118737-0d65-4713-ad58-52dff8cea5ef",
                        "specifics": {
                            "relationship_type": "custom_object",
                            "metadata": {
                                "description": "exising_object_subtitle",
                                "summary": "existing_object_title",
                            },
                        },
                    },
                    {
                        "type": "relationship",
                        "value": str(custom_object.version_independent_uuid),
                        "specifics": {
                            "relationship_type": "custom_object",
                            "metadata": {
                                "description": custom_object.subtitle,
                                "summary": custom_object.title,
                            },
                        },
                    },
                ],
                "specifics": None,
                "type": "relationship",
            },
        )

    @mock.patch(
        "zsnl_domains.case_management.commands.custom_object.CustomObjectActionTrigger._is_relation_field_multivalue"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_version_independent_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository.find_case_type_version_by_uuid"
    )
    def test_custom_object_action_trigger_create_in_single_relation_attribute(
        self,
        case_type_version_mock,
        custom_object_by_uuid,
        custom_object_type_uuid,
        mock_case_by_uuid,
        is_multi_relationfield,
    ):
        relation_field = "my_relation_field"
        params = {
            "case_uuid": str(uuid4()),
            "action_type": "create",
            "relation_field_magic_string": relation_field,
            "custom_object_fields": {
                "field_1": "value_1",
                "field_2": "value_2",
            },
            "status": True,
        }
        is_multi_relationfield.return_value = False
        case_mock = mock.MagicMock()
        mock_case_by_uuid.return_value = case_mock
        custom_object_type = mock.MagicMock(uuid=uuid4())
        custom_object_type_uuid.return_value = custom_object_type
        self.cmd.create_custom_object = mock.MagicMock()
        custom_object = mock.MagicMock(
            title="New title",
            subtitle="New subtitle",
            version_independent_uuid=uuid4(),
        )
        custom_object_by_uuid.return_value = custom_object
        case_mock.custom_fields = {}

        self.cmd.custom_object_action_trigger(**params)

        # verify if object creation is called with the correct parameters
        self.cmd.create_custom_object.assert_called_with(
            uuid=mock.ANY,
            custom_object_type_uuid=str(custom_object_type.uuid),
            custom_fields={
                "field_1": {"value": "value_1"},
                "field_2": {"value": "value_2"},
            },
            status="active",
        )

        # verify updating the relation attribute value
        case_mock.update_custom_field.assert_called_with(
            magic_string=relation_field,
            new_value={
                "type": "relationship",
                "value": str(custom_object.version_independent_uuid),
                "specifics": {
                    "relationship_type": "custom_object",
                    "metadata": {
                        "description": custom_object.subtitle,
                        "summary": custom_object.title,
                    },
                },
            },
        )

    @mock.patch(
        "zsnl_domains.case_management.commands.custom_object.CustomObjectActionTrigger._is_relation_field_multivalue"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository.find_case_type_version_by_uuid"
    )
    def test_custom_object_action_trigger_edit_with_existing_relation(
        self,
        mock_case_type_version,
        custom_object_by_uuid,
        mock_case_by_uuid,
        is_multi_relationfield,
    ):
        # verify that 2 out of 3 objects are updated.
        # The object with uuid '40ece4e3-dc0b-471d-aaa4-409330fa8339' is
        # inactive and should not be affected
        relation_field = "my_relation_field"
        params = {
            "case_uuid": str(uuid4()),
            "action_type": "edit",
            "relation_field_magic_string": relation_field,
            "custom_object_fields": {
                "field_1": "value_1",
                "field_2": "value_2",
            },
            "status": True,
        }
        case = entities.Case.parse_obj(
            {
                "id": 123,
                "number": 123,
                "uuid": uuid4(),
                "department": entities.Department.parse_obj(
                    {
                        "uuid": uuid4(),
                        "name": "Department of Mysteries",
                        "description": "Who knows!",
                    }
                ),
                "role": entities.Role.parse_obj(
                    {"uuid": uuid4(), "name": "Administrator"}
                ),
                "destruction_date": datetime.date(2019, 1, 1),
                "archival_state": "2019-23-23",
                "status": "open",
                "created_date": datetime.date(2019, 10, 22),
                "registration_date": datetime.date(2019, 10, 23),
                "target_completion_date": datetime.date(2019, 10, 24),
                "completion_date": datetime.date(2019, 10, 25),
                "stalled_until_date": datetime.date(2019, 10, 22),
                "milestone": 1,
                "extension_reason": "no reason",
                "suspension_reason": "no reason",
                "related_to": {},
                "completion": "yes",
                "stalled_since_date": datetime.date(2019, 12, 22),
                "last_modified_date": datetime.date(2019, 4, 19),
                "coordinator": None,
                "assignee": None,
                "case_type_version": {
                    "id": 123,
                    "uuid": uuid4(),
                    "case_type_uuid": uuid4(),
                    "version": 100,
                    "name": "Case Type Name",
                    "active": True,
                    "phases": [],
                    "requestor": {
                        "type_of_requestors": ["employee"],
                        "use_for_correspondence": False,
                    },
                    "settings": {"payment": {}},
                    "terms": {
                        "lead_time_legal": {"type": "days", "value": 100},
                        "lead_time_service": {"type": "days", "value": 100},
                    },
                    "case_type_results": [],
                    "is_eligible_for_case_creation": True,
                    "created": datetime.datetime(
                        2022, 8, 9, 13, 19, 12, tzinfo=datetime.timezone.utc
                    ),
                    "last_modified": datetime.datetime(
                        2022, 8, 9, 13, 19, 12, tzinfo=datetime.timezone.utc
                    ),
                    "metadata": {
                        "legal_basis": "None",
                        "process_description": "None",
                    },
                },
                "_event_service": self.cmd.event_service,
            }
        )
        mock_case_by_uuid.return_value = case

        is_multi_relationfield.return_value = True
        self.session.execute().fetchone.return_value = mock.MagicMock(
            id=13, value_type="relationship"
        )
        self.cmd.update_custom_object = mock.MagicMock()
        custom_object = mock.MagicMock(
            title="New title", subtitle="New subtitle"
        )
        custom_object_inactive = mock.MagicMock(
            title="exising_object_title 3",
            subtitle="exising_object_subtitle 3",
            status=entities.custom_object.ValidObjectStatus.inactive,
        )

        def custom_object_find_uuid(*args, **kwargs):
            if kwargs["uuid"] == "40ece4e3-dc0b-471d-aaa4-409330fa8339":
                return custom_object_inactive
            else:
                custom_object.version_independent_uuid = kwargs["uuid"]
                return custom_object

        custom_object_by_uuid.side_effect = custom_object_find_uuid

        case.custom_fields = {
            relation_field: {
                "type": "relationship",
                "value": {
                    "value": [
                        {
                            "type": "relationship",
                            "value": "8e118737-0d65-4713-ad58-52dff8cea5ef",
                            "specifics": {
                                "relationship_type": "custom_object",
                                "metadata": {
                                    "description": "exising_object_subtitle 1",
                                    "summary": "existing_object_title 1",
                                },
                            },
                        },
                        {
                            "type": "relationship",
                            "value": "de50dd2d-c288-40fe-9b01-0d7ca55b79db",
                            "specifics": {
                                "relationship_type": "custom_object",
                                "metadata": {
                                    "description": "exising_object_subtitle 2",
                                    "summary": "existing_object_title 2",
                                },
                            },
                        },
                        {
                            "type": "relationship",
                            "value": "40ece4e3-dc0b-471d-aaa4-409330fa8339",
                            "specifics": {
                                "relationship_type": "custom_object",
                                "metadata": {
                                    "description": "exising_object_subtitle 3",
                                    "summary": "existing_object_title 3",
                                },
                            },
                        },
                    ],
                    "specifics": None,
                    "type": "relationship",
                },
            }
        }

        self.cmd.custom_object_action_trigger(**params)

        self.cmd.update_custom_object.assert_has_calls(
            [
                mock.call(
                    existing_uuid="8e118737-0d65-4713-ad58-52dff8cea5ef",
                    custom_fields={
                        "field_1": {"value": "value_1"},
                        "field_2": {"value": "value_2"},
                    },
                    uuid=mock.ANY,
                    status=entities.custom_object.ValidObjectStatus.active,
                ),
                mock.call(
                    existing_uuid="de50dd2d-c288-40fe-9b01-0d7ca55b79db",
                    custom_fields={
                        "field_1": {"value": "value_1"},
                        "field_2": {"value": "value_2"},
                    },
                    uuid=mock.ANY,
                    status=entities.custom_object.ValidObjectStatus.active,
                ),
            ]
        )
        call_list = self.cmd.update_custom_object.call_args_list
        assert len(call_list) == 2

        # verify updating the relation attribute value
        assert case.custom_fields[relation_field] == {
            "type": "relationship",
            "specifics": None,
            "value": [
                {
                    "type": "relationship",
                    "value": "8e118737-0d65-4713-ad58-52dff8cea5ef",
                    "specifics": {
                        "relationship_type": "custom_object",
                        "metadata": {
                            "description": custom_object.subtitle,
                            "summary": custom_object.title,
                        },
                    },
                },
                {
                    "type": "relationship",
                    "value": "de50dd2d-c288-40fe-9b01-0d7ca55b79db",
                    "specifics": {
                        "relationship_type": "custom_object",
                        "metadata": {
                            "description": custom_object.subtitle,
                            "summary": custom_object.title,
                        },
                    },
                },
                {
                    "type": "relationship",
                    "value": "40ece4e3-dc0b-471d-aaa4-409330fa8339",
                    "specifics": {
                        "relationship_type": "custom_object",
                        "metadata": {
                            "description": "exising_object_subtitle 3",
                            "summary": "existing_object_title 3",
                        },
                    },
                },
            ],
        }

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 5

        update_statement = call_list[4][0][0]
        compiled_update = update_statement.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert str(compiled_update) == (
            "INSERT INTO zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, value, magic_string) VALUES (%(zaak_id)s, %(bibliotheek_kenmerken_id)s, %(value)s, %(magic_string)s) ON CONFLICT (zaak_id, bibliotheek_kenmerken_id) DO UPDATE SET value = %(param_1)s RETURNING zaak_kenmerk.id"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository.find_case_type_version_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object_type.CustomObjectTypeRepository.find_by_version_independent_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    def test_custom_object_not_found(
        self,
        custom_object_by_uuid,
        custom_object_type_uuid,
        mock_case_by_uuid,
        mock_find_case_type_version,
    ):
        # verify that 2 out of 3 objects are updated.
        # The object with uuid '40ece4e3-dc0b-471d-aaa4-409330fa8339' is
        # inactive and should not be affected
        relation_field = "my_relation_field"
        params = {
            "case_uuid": str(uuid4()),
            "action_type": "create",
            "relation_field_magic_string": relation_field,
            "custom_object_fields": {
                "field_1": "value_1",
                "field_2": "value_2",
            },
            "status": True,
        }
        case_mock = mock.MagicMock()
        mock_case_by_uuid.return_value = case_mock
        custom_object_type = mock.MagicMock(uuid=uuid4())
        custom_object_type_uuid.return_value = custom_object_type
        custom_object_by_uuid.return_value = None
        self.cmd.create_custom_object = mock.MagicMock()

        with pytest.raises(exceptions.NotFound):
            self.cmd.custom_object_action_trigger(**params)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository.find_case_type_version_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.commands.custom_object.CustomObjectActionTrigger._is_relation_field_multivalue"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_create_custom_object_relationship_field_not_found(
        self,
        mock_case_by_uuid,
        is_multi_relationfield,
        mock_find_case_type_version,
    ):
        relation_field = "my_relation_field"
        params = {
            "case_uuid": str(uuid4()),
            "action_type": "create",
            "relation_field_magic_string": relation_field,
            "custom_object_fields": {
                "field_1": "value_1",
                "field_2": "value_2",
            },
            "status": True,
        }
        case_mock = mock.MagicMock(case_type=None)
        mock_case_by_uuid.return_value = case_mock
        is_multi_relationfield.return_value = True

        with pytest.raises(exceptions.NotFound):
            self.cmd.custom_object_action_trigger(**params)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case_type.CaseTypeRepository.find_case_type_version_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.commands.custom_object.CustomObjectActionTrigger._is_relation_field_multivalue"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_edit_custom_object_relationship_field_not_found(
        self,
        mock_case_by_uuid,
        is_multi_relationfield,
        case_type_version_mock,
    ):
        relation_field = "my_relation_field"
        params = {
            "case_uuid": str(uuid4()),
            "action_type": "create",
            "relation_field_magic_string": relation_field,
            "custom_object_fields": {
                "field_1": "value_1",
                "field_2": "value_2",
            },
            "status": True,
        }
        case_mock = mock.MagicMock(case_type=None, custom_fields=None)
        mock_case_by_uuid.return_value = case_mock
        is_multi_relationfield.return_value = True

        with pytest.raises(exceptions.NotFound):
            self.cmd.custom_object_action_trigger(**params)
