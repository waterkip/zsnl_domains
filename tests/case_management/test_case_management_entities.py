# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import pytest
import pytz
from datetime import date, datetime, timezone
from dateutil.relativedelta import relativedelta
from freezegun import freeze_time
from minty import date_calculation
from minty.cqrs.events import EventService
from minty.exceptions import ConfigurationConflict, Conflict
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.case_management.entities import (
    Case,
    CaseContactEmployee,
    CaseMessageList,
    Department,
    ExportFile,
    Organization,
    Role,
    Subject,
    SubjectRelation,
)
from zsnl_domains.case_management.entities.case_type_version import (
    CustomFieldDateLimit,
    CustomFieldDateLimitObject,
)


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestCaseEntity:
    @mock.patch("zsnl_domains.case_management.services.CaseTemplateService")
    def setup(self, mock_case_template_service):
        department_uuid = uuid4()
        role_uuid = uuid4()
        mock_case_template_service.render_value.return_value = "test_subject"
        self.template_service = mock_case_template_service
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        self.case = Case(
            id=123,
            number=123,
            uuid=uuid4(),
            department=Department(
                uuid=department_uuid,
                name="Department of Mysteries",
                description="Who knows!",
            ),
            role=Role(uuid=role_uuid, name="Administrator"),
            destruction_date=date(2019, 1, 1),
            archival_state="2019-23-23",
            status="open",
            created_date=date(2019, 10, 22),
            registration_date=date(2019, 10, 23),
            target_completion_date=date(2019, 10, 24),
            completion_date=date(2019, 10, 25),
            stalled_until_date=date(2019, 10, 22),
            milestone=1,
            suspension_reason="no reason",
            completion="yes",
            stalled_since_date=date(2019, 12, 22),
            last_modified_date=date(2019, 4, 19),
            coordinator=None,
            assignee=None,
            _event_service=event_service,
        )

    def test_case_set_parent(self):
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        department_uuid = uuid4()
        role_uuid = uuid4()
        case_1_uuid = uuid4()
        self.case_1 = Case(
            id=111,
            number=111,
            uuid=case_1_uuid,
            department=Department(
                uuid=department_uuid,
                name="Department of Mysteries",
                description="Who knows!",
            ),
            role=Role(uuid=role_uuid, name="Administrator"),
            destruction_date=date(2019, 1, 1),
            archival_state="2019-23-23",
            status="open",
            created_date=date(2019, 10, 22),
            registration_date=date(2019, 10, 23),
            target_completion_date=date(2019, 10, 24),
            completion_date=date(2019, 10, 25),
            stalled_until_date=date(2019, 10, 22),
            milestone=1,
            extension_reason="no reason",
            suspension_reason="no reason",
            related_to={},
            completion="yes",
            stalled_since_date=date(2019, 12, 22),
            last_modified_date=date(2019, 4, 19),
            coordinator=None,
            assignee=None,
            _event_service=event_service,
        )
        self.case.set_case_parent(case_1_uuid)
        assert self.case.parent_uuid == case_1_uuid

    def _format_date(self, date_value):
        return date_value.replace(tzinfo=timezone.utc).astimezone(
            tz=pytz.timezone("Europe/Amsterdam")
        )

    def test_target_completion_date(self):
        self.case.status = "open"
        self.case.registration_date = date(day=1, year=2019, month=1)

        target_completion_date = "2019-01-28"
        self.case.set_target_completion_date(
            target_completion_date=target_completion_date,
            template_service=self.template_service,
        )
        assert self.case.target_completion_date == self.case.str_to_date(
            target_completion_date
        )

        with pytest.raises(Conflict):
            target_completion_date = "2018-01-28"
            self.case.set_target_completion_date(
                target_completion_date=target_completion_date
            )

    def test_set_completion_date(self):
        self.case.status = "resolved"
        self.case.registration_date = date(day=1, year=2019, month=1)

        completion_date = "2019-01-15"
        self.case.set_completion_date(
            completion_date=completion_date,
            template_service=self.template_service,
        )
        assert self.case.completion_date == self.case.str_to_date(
            completion_date
        )

        with pytest.raises(Conflict):
            self.case.status = "open"
            self.case.set_completion_date(
                completion_date=completion_date,
                template_service=self.template_service,
            )

        with pytest.raises(Conflict):
            self.case.status = "resolved"
            self.case.registration_date = date(day=20, year=2020, month=1)
            self.case.set_completion_date(
                completion_date=completion_date,
                template_service=self.template_service,
            )

        with pytest.raises(Conflict):
            completion_date = "2999-01-15"
            self.case.status = "resolved"
            self.case.registration_date = date(day=20, year=2020, month=1)
            self.case.set_completion_date(
                completion_date=completion_date,
                template_service=self.template_service,
            )

    @freeze_time("2012-01-01")
    def test_pause_indefinite(self):
        self.case.status = "open"
        reason = "pausing"
        self.case.pause(
            suspension_reason=reason,
            suspension_term_type="indefinite",
            suspension_term_value=None,
            template_service=self.template_service,
        )

        assert self.case.status == "stalled"
        assert self.case.suspension_reason == reason
        assert self.case.stalled_since_date == date.today()
        assert self.case.stalled_until_date is None

    @freeze_time("2012-01-01")
    def test_pause_weeks(self):
        self.case.status = "open"
        reason = "pausing"
        date_to_check = date_calculation.add_timedelta_to_date("weeks", 6)

        self.case.pause(
            suspension_reason=reason,
            suspension_term_value=6,
            suspension_term_type="weeks",
            template_service=self.template_service,
        )

        assert self.case.status == "stalled"
        assert self.case.suspension_reason == reason
        assert self.case.stalled_since_date == date.today()
        assert self.case.stalled_until_date == date_to_check
        self.case.status = "stalled"
        with pytest.raises(Conflict):
            self.case.pause(
                suspension_reason=reason,
                suspension_term_value=6,
                suspension_term_type="weeks",
                template_service=self.template_service,
            )

    @freeze_time("2012-01-01")
    def test_pause_case_calendar_dates(self):
        self.case.status = "open"
        reason = "pausing"
        date_to_check = date_calculation.add_timedelta_to_date("days", 6)

        self.case.pause(
            suspension_reason=reason,
            suspension_term_value=6,
            suspension_term_type="calendar_days",
            template_service=self.template_service,
        )

        assert self.case.status == "stalled"
        assert self.case.suspension_reason == reason
        assert self.case.stalled_since_date == date.today()
        assert self.case.stalled_until_date == date_to_check
        self.case.status = "stalled"
        with pytest.raises(Conflict):
            self.case.pause(
                suspension_reason=reason,
                suspension_term_value=6,
                suspension_term_type="weeks",
                template_service=self.template_service,
            )

    @freeze_time("2012-01-01")
    def test_pause_case_business_days(self):
        self.case.status = "open"
        reason = "pausing"
        date_to_check = date_calculation.add_timedelta_to_date(
            "business_days", 6
        )

        self.case.pause(
            suspension_reason=reason,
            suspension_term_value=6,
            suspension_term_type="work_days",
            template_service=self.template_service,
        )

        assert self.case.status == "stalled"
        assert self.case.suspension_reason == reason
        assert self.case.stalled_since_date == date.today()

        assert self.case.stalled_until_date == date_to_check
        self.case.status = "stalled"
        with pytest.raises(Conflict):
            self.case.pause(
                suspension_reason=reason,
                suspension_term_value=6,
                suspension_term_type="weeks",
                template_service=self.template_service,
            )

    @freeze_time("2012-01-01")
    def test_pause_case_fixed_date(self):
        self.case.status = "open"
        reason = "pausing"
        postponed_date = self.case.date_to_str(
            date.today() + relativedelta(years=1)
        )

        self.case.pause(
            suspension_reason=reason,
            suspension_term_value=postponed_date,
            suspension_term_type="fixed_date",
            template_service=self.template_service,
        )

        assert self.case.status == "stalled"
        assert self.case.suspension_reason == reason
        assert self.case.stalled_since_date == date.today()
        assert self.case.stalled_until_date == self.case.str_to_date(
            postponed_date
        )

        self.case.status = "stalled"
        with pytest.raises(Conflict):
            self.case.pause(
                suspension_reason="reason",
                suspension_term_value=date.today().isoformat(),
                suspension_term_type="fixed_date",
                template_service=self.template_service,
            )

    def test_pause_case_fixed_date_in_the_past(self):
        self.case.status = "open"
        reason = "pausing"
        past_date = date.today() - relativedelta(years=5)
        with pytest.raises(Conflict):
            self.case.pause(
                suspension_reason=reason,
                suspension_term_value=past_date.isoformat(),
                suspension_term_type="fixed_date",
                template_service=self.template_service,
            )

    def test_pause_case_fixed_date_none(self):
        self.case.status = "open"
        reason = "pausing"
        with pytest.raises(Conflict):
            self.case.pause(
                suspension_reason=reason,
                suspension_term_value=None,
                suspension_term_type="fixed_date",
                template_service=self.template_service,
            )

    @freeze_time("2012-01-01")
    def test__get_stalled_until_date(self):
        stalled_until_date = self.case._get_stalled_until_date(
            term_value=date.today(), term_type="", start_date=date.today()
        )
        assert stalled_until_date is None

        with pytest.raises(Conflict):
            self.case._get_stalled_until_date(
                term_value=None,
                term_type="fixed_date",
                start_date=date.today(),
            )

        fixed_date = self.case.date_to_str(
            date.today() + relativedelta(years=1)
        )
        stalled_until_date = self.case._get_stalled_until_date(
            term_value=fixed_date,
            term_type="fixed_date",
            start_date=date.today(),
        )
        assert stalled_until_date == self.case.str_to_date(fixed_date)

        stalled_until_date = self.case._get_stalled_until_date(
            term_value=date.today(),
            term_type="indefinite",
            start_date=date.today(),
        )
        assert stalled_until_date is None

        weeks_date_to_check = date_calculation.add_timedelta_to_date(
            "weeks", 6
        )
        stalled_until_date = self.case._get_stalled_until_date(
            term_value=6, term_type="weeks", start_date=date.today()
        )
        assert stalled_until_date == weeks_date_to_check

        business_date_to_check = date_calculation.add_timedelta_to_date(
            "business_days", 6
        )
        stalled_until_date = self.case._get_stalled_until_date(
            term_value=6, term_type="work_days", start_date=date.today()
        )
        assert stalled_until_date == business_date_to_check

        days_date_to_check = date_calculation.add_timedelta_to_date("days", 6)
        stalled_until_date = self.case._get_stalled_until_date(
            term_value=6, term_type="calendar_days", start_date=date.today()
        )
        assert stalled_until_date == days_date_to_check

    def test_pause_case_non_existing_interval(self):
        self.case.status = "open"
        reason = "pausing"
        today_date = date.today().isoformat()
        self.case.pause(
            suspension_reason=reason,
            suspension_term_value=today_date,
            suspension_term_type="heheheehehhehe_date",
            template_service=self.template_service,
        )
        assert self.case.stalled_until_date is None

    # Free date is one month after registration date, failure otherwise
    @freeze_time("2018-02-01")
    def test_resume(self):
        today = date.today()
        self.case.stalled_until_date = self.case.str_to_date("2019-01-01")
        self.case.status = "stalled"
        self.case.suspension_reason = "reason"
        self.case.resume_reason = "resume reason"
        self.case.target_completion_date = self.case.str_to_date("2019-01-01")
        self.case.registration_date = date(day=1, year=2018, month=1)

        self.case.resume(
            stalled_since_date="2019-01-01",
            stalled_until_date=self.case.date_to_str(today),
            resume_reason="resume reason",
            template_service=self.template_service,
        )

        assert self.case.target_completion_date == today
        assert self.case.suspension_reason is None
        assert self.case.stalled_until_date == today
        assert self.case.status == "open"
        assert self.case.resume_reason == "resume reason"

        self.case.status = "open"
        with pytest.raises(Conflict):
            self.case.resume(
                stalled_since_date="2019-01-01",
                stalled_until_date="2019-02-01",
                resume_reason="resume reason",
                template_service=self.template_service,
            )

    def test_resume_in_the_past(self):
        self.case.stalled_until_date = self.case.str_to_date("2019-01-10")
        self.case.stalled_since_date = self.case.str_to_date("2019-01-06")
        self.case.status = "stalled"
        self.case.suspension_reason = "reason"
        self.case.resume_reason = "resume reason"
        self.case.target_completion_date = self.case.str_to_date("2019-01-10")
        self.case.registration_date = date(day=1, year=2018, month=1)

        self.case.resume(
            stalled_since_date="2019-01-02",
            stalled_until_date="2019-01-05",
            resume_reason="resume reason",
            template_service=self.template_service,
        )

        assert self.case.target_completion_date == self.case.str_to_date(
            "2019-01-13"
        )
        assert self.case.suspension_reason is None
        assert self.case.stalled_until_date == self.case.str_to_date(
            "2019-01-05"
        )
        assert self.case.stalled_since_date == self.case.str_to_date(
            "2019-01-02"
        )
        assert self.case.status == "open"
        assert self.case.resume_reason == "resume reason"

        self.case.status = "open"
        with pytest.raises(Conflict):
            self.case.resume(
                stalled_since_date="2019-01-01",
                stalled_until_date="2019-02-01",
                resume_reason="resume reason",
                template_service=self.template_service,
            )

    def test_set_assignee(self):
        subject = CaseContactEmployee(
            id="34",
            entity_type="employee",
            uuid=UUID("fa81a55c-c590-47f5-a89a-2337a52aab52"),
            name="auser",
            surname="testname",
            magic_string_prefix=None,
            family_name="testname",
            full_name="auser",
            first_names="auser",
            initials="a",
            used_name="testname",
            unique_name="betrokkene-medewerker-34",
            subject_type="medewerker",
            status="Actief",
            street=None,
            residence_street=None,
            zipcode=None,
            residence_zipcode=None,
            house_number=None,
            residence_house_number=None,
            phone_number=None,
            email="auser@mintlab.nl",
            department="Backoffice",
            correspondence_zipcode=None,
            correspondence_street=None,
            properties={
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            },
        )
        self.case.set_assignee(
            assignee=subject, template_service=self.template_service
        )

        assert self.case.assignee == subject

        with pytest.raises(Conflict) as excinfo:
            self.case.status = "resolved"
            self.case.set_assignee(
                assignee=subject, template_service=self.template_service
            )

        assert excinfo.value.args == (
            "Assigning a case not allowed for resolved cases.",
            "case/assign_case_not_allowed_for_resolved_cases",
        )
        with pytest.raises(TypeError):
            self.case.status = "open"
            self.case.set_assignee("not a Subject")

    def test_set_coordinator(self):
        subject = CaseContactEmployee(
            id="34",
            entity_type="employee",
            uuid=UUID("fa81a55c-c590-47f5-a89a-2337a52aab52"),
            name="auser",
            surname="testname",
            magic_string_prefix=None,
            family_name="testname",
            full_name="auser",
            first_names="auser",
            initials="a",
            used_name="testname",
            unique_name="betrokkene-medewerker-34",
            subject_type="medewerker",
            status="Actief",
            street=None,
            residence_street=None,
            zipcode=None,
            residence_zipcode=None,
            house_number=None,
            residence_house_number=None,
            phone_number=None,
            email="auser@mintlab.nl",
            department="Backoffice",
            correspondence_zipcode=None,
            correspondence_street=None,
            properties={
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            },
        )

        self.case.set_coordinator(
            coordinator=subject, template_service=self.template_service
        )

        assert self.case.coordinator == subject
        assert isinstance(self.case.coordinator, CaseContactEmployee)
        assert self.case.coordinator.entity_type == "employee"
        with pytest.raises(TypeError):
            self.case.status = "open"
            self.case.set_coordinator("not a Subject")

    def test_set_status(self):
        self.case.set_status(
            status="open", template_service=self.template_service
        )

        assert self.case.status == "open"

    # @mock.patch.object(Case, "_custom_fields_allowedness_check")
    # @mock.patch.object(Case, "_custom_fields_requiredness_check")
    # @mock.patch.object(Case, "_custom_fields_type_correctness_check")
    # def test_create(
    #     self,
    #     mock_type_correctness_check,
    #     mok_required_custom_field_check,
    #     mock_allowed_custom_fields_check,
    # ):
    #     department_uuid = uuid4()
    #     role_uuid = uuid4()
    #     case_type_uuid = str(uuid4())

    #     case_type = CaseTypeVersionEntity(
    #         uuid=case_type_uuid,
    #         case_type_uuid=uuid4(),
    #         version=3,
    #         name="casetype1",
    #         active=True,
    #         description=None,
    #         identification=None,
    #         tags=None,
    #         case_summary="subject",
    #         case_public_summary="subject external",
    #         requestor={},
    #         catalog_folder={},
    #         settings={},
    #         terms={
    #             "lead_time_service": {"type": "kalenderdagen", "value": 23}
    #         },
    #         initiator_source=None,
    #         initiator_type=None,
    #         metadata={},
    #         phases=[
    #             {
    #                 "name": "phase_1",
    #                 "milestone": 1,
    #                 "allocation": {
    #                     "department": {
    #                         "id": department_uuid,
    #                         "name": "Development",
    #                     },
    #                     "role": {"id": role_uuid, "name": "Admin"},
    #                 },
    #             }
    #         ],
    #         preset_assignee={"type": "employee", "id": uuid4()},
    #         is_eligible_for_case_creation=False,
    #         created="2019-04-03",
    #         last_modified="2019-04-03",
    #     )
    #     requestor = {"type": "employee", "id": uuid4()}
    #     self.case.create(
    #         contact_channel="email",
    #         case_type=case_type,
    #         requestor=requestor,
    #         custom_fields={"att_text": "text"},
    #         allow_missing_required_fields=False,
    #         confidentiality=None,
    #         contact_information={"mobile_number": "+3106123456789"},
    #     )

    #     assert self.case.contact_channel == "email"
    #     assert self.case.status == "new"
    #     assert self.case.milestone == 1
    #     assert self.case.subject == "subject"
    #     assert self.case.milestone == 1
    #     assert self.case.request_trigger == "extern"
    #     assert self.case.subject_extern == "subject external"
    #     assert self.case.confidentiality == "public"
    #     assert self.case.contact_information == {
    #         "mobile_number": "+3106123456789"
    #     }

    #     with pytest.raises(Conflict) as excinfo:
    #         self.case.create(
    #             contact_channel="email",
    #             case_type=case_type,
    #             requestor=requestor,
    #             custom_fields={"att_text": "text"},
    #             allow_missing_required_fields=False,
    #             confidentiality="test",
    #             contact_information={},
    #         )
    #     assert excinfo.value.args == (
    #         "Value 'test' is not allowed for confidentiality. Allowed values are 'public, internal, confidential'",
    #         "case/invalid_confidentiality",
    #     )

    def test__calculate_target_completion_date(self):
        registration_date = datetime(2019, 4, 4, 0, 0, 0)

        res = self.case._calculate_target_completion_date(
            interval_for_completion=2,
            interval_for_completion_type="kalenderdagen",
            registration_date=registration_date,
        )
        assert res == datetime(2019, 4, 6, 0, 0, 0)

        res = self.case._calculate_target_completion_date(
            interval_for_completion="05-02-2019",
            interval_for_completion_type="einddatum",
            registration_date=registration_date,
        )
        assert res == date(2019, 2, 5)

        with pytest.raises(ConfigurationConflict) as excinfo:
            res = self.case._calculate_target_completion_date(
                interval_for_completion="2019-05-03",
                interval_for_completion_type="einddatum",
                registration_date=registration_date,
            )

        assert excinfo.value.args == (
            "Time data '2019-05-03'  does not match '%d-%m-%Y'",
            "case/invalid_target_completion_date",
        )

        with pytest.raises(ConfigurationConflict) as excinfo:
            res = self.case._calculate_target_completion_date(
                interval_for_completion="06=07-2018",
                interval_for_completion_type="calendardays",
                registration_date=registration_date,
            )

        assert excinfo.value.args == (
            "Invalid completion type 'calendardays' specified.",
            "case/invalid_target_completion_type",
        )

    def test__is_valid_url(self):
        url = "https://xyz.com"
        res = self.case._is_valid_url(url)
        assert res is True

        url = "www.xyz.com"
        res = self.case._is_valid_url(url)
        assert res is False

    def test__is_valid_string(self):
        string = "test"
        res = self.case._is_valid_string(string)
        assert res is True

        string = 1234
        res = self.case._is_valid_string(string)
        assert res is False

    def test_is_valid_uppercase_string(self):
        string = "TEST"
        res = self.case._is_valid_uppercase_string(string)
        assert res is True

        string = "test"
        res = self.case._is_valid_uppercase_string(string)
        assert res is False

        string = 1234
        res = self.case._is_valid_uppercase_string(string)
        assert res is False

    def test__is_valid_float(self):
        number = 123
        res = self.case._is_valid_number(number)
        assert res is True

        string = 123.45
        res = self.case._is_valid_number(string)
        assert res is False

    def test__is_valid_iban(self):
        iban = "RO13RZBR0000060007134800"
        res = self.case._is_valid_iban(iban)
        assert res is True

        iban = "1234"
        res = self.case._is_valid_iban(iban)
        assert res is False

        iban = "helllo123456"
        res = self.case._is_valid_iban(iban)
        assert res is False

    def test__is_valid_currency(self):
        currency = 1234.34
        res = self.case._is_valid_currency(currency)
        assert res is True

        currency = 1234.678
        res = self.case._is_valid_currency(currency)
        assert res is False

    # def test__custom_fields_allowedness_check(self):
    #     case_type_version_uuid = uuid4()
    #     case_type = CaseRelatedTypeVersion(
    #         uuid=str(case_type_version_uuid),
    #         case_type_uuid=str(uuid4()),
    #         version=25,
    #         name="example casetype",
    #         description=None,
    #         identification=None,
    #         tags=None,
    #         case_summary="case summary",
    #         case_public_summary="case summary public",
    #         active=True,
    #         requestor={},
    #         catalog_folder={"id": uuid4(), "name": "test folder"},
    #         metadata={},
    #         phases=[
    #             {
    #                 "name": "Registration",
    #                 "milestone": 1,
    #                 "department": {
    #                     "id": uuid4(),
    #                     "name": "development",
    #                     "table_id": 2,
    #                 },
    #                 "role": {
    #                     "id": uuid4(),
    #                     "name": "developemnt",
    #                     "table_id": 3,
    #                 },
    #                 "phase": "Registering",
    #                 "custom_fields": [
    #                     {
    #                         "field_magic_string": "att_text",
    #                         "field_type": "text",
    #                         "is_required": True,
    #                         "is_hidden_field": False,
    #                     },
    #                     {
    #                         "field_magic_string": "att_url",
    #                         "field_type": "url",
    #                         "is_required": True,
    #                         "is_hidden_field": False,
    #                     },
    #                     {
    #                         "field_magic_string": "att_email",
    #                         "field_type": "email",
    #                         "is_required": True,
    #                         "is_hidden_field": True,
    #                     },
    #                 ],
    #             }
    #         ],
    #         settings={},
    #         terms={},
    #         initiator_source="extern",
    #         initiator_type="aangan",
    #         is_eligible_for_case_creation=True,
    #         preset_assignee={},
    #         created=None,
    #         last_modified=None,
    #     )
    #     custom_fields = {"att_text": ["text"], "att_url": ["https://abc.com"]}
    #     self.case._custom_fields_allowedness_check(
    #         custom_fields=custom_fields, case_type=case_type
    #     )

    #     with pytest.raises(Conflict) as excinfo:
    #         custom_fields = {
    #             "att_url": ["www.abc.com"],
    #             "att_email": ["xyz@gmail.com"],
    #         }
    #         self.case._custom_fields_allowedness_check(
    #             custom_fields=custom_fields, case_type=case_type
    #         )
    #     assert excinfo.value.args == (
    #         "Custom fields 'att_email' is/are not allowed",
    #         "case/custom_fields_not_allowed",
    #     )

    #     with pytest.raises(Conflict) as excinfo:
    #         case_type.phases = []
    #         self.case._custom_fields_allowedness_check(
    #             custom_fields=custom_fields, case_type=case_type
    #         )
    #     assert excinfo.value.args == (
    #         f"Case type '{case_type.uuid}' has no phases in it.",
    #         "case_type/no_phases",
    #     )

    # def test__custom_fields_requiredness_check(self):
    #     case_type_version_uuid = uuid4()
    #     case_type = CaseTypeVersionEntity(
    #         uuid=case_type_version_uuid,
    #         case_type_uuid=uuid4(),
    #         version=25,
    #         name="example casetype",
    #         description=None,
    #         identification=None,
    #         tags=None,
    #         case_summary="case summary",
    #         case_public_summary="case summary public",
    #         active=True,
    #         requestor={},
    #         catalog_folder={"id": uuid4(), "name": "test folder"},
    #         metadata={},
    #         phases=[
    #             {
    #                 "name": "Registration",
    #                 "milestone": 1,
    #                 "department": {
    #                     "id": uuid4(),
    #                     "name": "development",
    #                     "table_id": 2,
    #                 },
    #                 "role": {
    #                     "id": uuid4(),
    #                     "name": "developemnt",
    #                     "table_id": 3,
    #                 },
    #                 "phase": "Registering",
    #                 "custom_fields": [
    #                     {
    #                         "field_magic_string": "att_text",
    #                         "field_type": "text",
    #                         "is_hidden_field": True,
    #                         "is_required": True,
    #                     },
    #                     {
    #                         "field_magic_string": "att_url",
    #                         "field_type": "url",
    #                         "is_hidden_field": True,
    #                         "is_required": True,
    #                     },
    #                 ],
    #             }
    #         ],
    #         settings={},
    #         terms={},
    #         initiator_source="extern",
    #         initiator_type="aangan",
    #         is_eligible_for_case_creation=True,
    #         preset_assignee={},
    #         created=None,
    #         last_modified=None,
    #     )
    #     custom_fields = {"att_text": ["text"], "att_url": ["https://abc.com"]}
    #     self.case._custom_fields_requiredness_check(
    #         custom_fields=custom_fields, case_type=case_type
    #     )

    #     with pytest.raises(Conflict) as excinfo:
    #         custom_fields = {"att_text": ["text"]}
    #         self.case._custom_fields_requiredness_check(
    #             custom_fields=custom_fields, case_type=case_type
    #         )
    #     assert excinfo.value.args == (
    #         "Custom fields 'att_url' is/are required",
    #         "case/required_custom_fields_missing",
    #     )

    # @mock.patch.object(Case, "_assert_valid_geolatlon_type")
    # @mock.patch.object(Case, "_assert_valid_date_type")
    # @mock.patch.object(Case, "_assert_valid_checkbox_type")
    # @mock.patch.object(Case, "_assert_valid_option_or_select_type")
    # @mock.patch.object(Case, "_assert_valid_text_or_numeric_type")
    # def test__custom_fields_type_correctness_check(
    #     self,
    #     mock_assert_valid_text_or_numeric_type,
    #     mock_assert_valid_option_or_select_type,
    #     mock_assert_valid_checkbox_type,
    #     mock_assert_valid_date_type,
    #     mock_assert_valid_geolatlon_type,
    # ):
    #     case_type_version_uuid = uuid4()
    #     case_type = CaseTypeVersionEntity(
    #         uuid=case_type_version_uuid,
    #         case_type_uuid=uuid4(),
    #         version=25,
    #         name="example casetype",
    #         description=None,
    #         identification=None,
    #         tags=None,
    #         case_summary="case summary",
    #         case_public_summary="case summary public",
    #         active=True,
    #         requestor={},
    #         catalog_folder={"id": uuid4(), "name": "test folder"},
    #         metadata={},
    #         phases=[
    #             {
    #                 "name": "Registration",
    #                 "milestone": 1,
    #                 "department": {
    #                     "id": uuid4(),
    #                     "name": "development",
    #                     "table_id": 2,
    #                 },
    #                 "role": {
    #                     "id": uuid4(),
    #                     "name": "developemnt",
    #                     "table_id": 3,
    #                 },
    #                 "phase": "Registering",
    #                 "custom_fields": [
    #                     {
    #                         "field_magic_string": "att_text",
    #                         "field_type": "text",
    #                         "is_hidden_field": True,
    #                         "is_required": True,
    #                         "field_options": [],
    #                         "date_field_limit": {},
    #                     },
    #                     {
    #                         "field_magic_string": "att_url",
    #                         "field_type": "url",
    #                         "is_hidden_field": True,
    #                         "is_required": True,
    #                         "field_options": [],
    #                         "date_field_limit": {},
    #                     },
    #                     {
    #                         "field_magic_string": "att_checkbox",
    #                         "field_type": "checkbox",
    #                         "is_hidden_field": True,
    #                         "is_required": True,
    #                         "field_options": ["option_a", "option_b"],
    #                         "date_field_limit": {},
    #                     },
    #                     {
    #                         "field_magic_string": "att_select",
    #                         "field_type": "select",
    #                         "is_hidden_field": True,
    #                         "is_required": True,
    #                         "field_options": ["option_1", "option_2"],
    #                         "date_field_limit": {},
    #                     },
    #                     {
    #                         "field_magic_string": "att_datum",
    #                         "field_type": "date",
    #                         "is_hidden_field": False,
    #                         "is_required": True,
    #                         "field_options": [],
    #                         "date_field_limit": {
    #                             "start": {
    #                                 "active": True,
    #                                 "interval_type": "days",
    #                                 "interval": 20,
    #                                 "reference": "current",
    #                                 "during": "pre",
    #                             },
    #                             "end": {
    #                                 "active": True,
    #                                 "interval_type": "days",
    #                                 "interval": 20,
    #                                 "reference": "current",
    #                                 "during": "pre",
    #                             },
    #                         },
    #                     },
    #                     {
    #                         "field_magic_string": "att_geolatlon",
    #                         "field_type": "geolatlon",
    #                         "is_hidden_field": False,
    #                         "is_required": True,
    #                         "field_options": [],
    #                         "date_field_limit": {},
    #                     },
    #                 ],
    #             }
    #         ],
    #         settings={},
    #         terms={},
    #         initiator_source="extern",
    #         initiator_type="aangan",
    #         is_eligible_for_case_creation=True,
    #         preset_assignee={},
    #         created=None,
    #         last_modified=None,
    #     )
    #     custom_fields = {
    #         "att_text": ["text"],
    #         "att_url": ["https://abc.com"],
    #         "att_checkbox": [["option_b"]],
    #         "att_select": ["option_1"],
    #         "att_datum": ["2019-05-23"],
    #         "att_geolatlon": [
    #             {
    #                 "type": "FeatureCollection",
    #                 "features": [
    #                     {
    #                         "type": "Feature",
    #                         "properties": {},
    #                         "geometry": {
    #                             "type": "Point",
    #                             "coordinates": [135.703125, 79.30263962053658],
    #                         },
    #                     }
    #                 ],
    #             }
    #         ],
    #     }
    #     self.case._custom_fields_type_correctness_check(
    #         custom_fields=custom_fields, case_type=case_type
    #     )

    #     with pytest.raises(Conflict) as excinfo:
    #         custom_fields = {"att_text": "text"}
    #         self.case._custom_fields_type_correctness_check(
    #             custom_fields=custom_fields, case_type=case_type
    #         )
    #     assert excinfo.value.args == (
    #         "Custom field values 'text' for 'att_text' is not a list",
    #         "case/custom_field_values_not_list",
    #     )

    def test__assert_valid_text_or_numeric_type(self):
        self.case._assert_valid_text_or_numeric_type(
            "att_text", ["text"], "text"
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_text_or_numeric_type(
                "att_email", ["@xyz.com"], "email"
            )
        assert excinfo.value.args == (
            "Value '@xyz.com' for custom field 'att_email' is not of type 'email'",
            "case/custom_field_invalid_type",
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_text_or_numeric_type(
                "att_text", [123], "text"
            )
        assert excinfo.value.args == (
            "Value '123' for custom field 'att_text' is not of type 'text'",
            "case/custom_field_invalid_type",
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_text_or_numeric_type(
                "att_url", ["www.abc.com"], "url"
            )
        assert excinfo.value.args == (
            "Value 'www.abc.com' for custom field 'att_url' is not of type 'url'",
            "case/custom_field_invalid_type",
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_text_or_numeric_type(
                "att_text_uc", ["test"], "text_uc"
            )
        assert excinfo.value.args == (
            "Value 'test' for custom field 'att_text_uc' is not of type 'text_uc'",
            "case/custom_field_invalid_type",
        )

    def test__assert_valid_checkbox_type(self):
        self.case._assert_valid_checkbox_type(
            "att_checkbox", [["a", "b"]], ["a", "b", "c", "d"]
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_checkbox_type(
                "att_checkbox", ["a", "b", "c", "d"], ["a", "b", "c", "d"]
            )
        assert excinfo.value.args == (
            "Value for custom_field 'att_checkbox' is not a list",
            "case/custom_field_check_box_value_not_list",
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_checkbox_type(
                "att_checkbox", [["a", "b"], ["c", "d"]], ["a", "b", "c", "d"]
            )
        assert excinfo.value.args == (
            "Multiple values for check box is not supported",
            "case/multi_value_checkbox_bot_supported",
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_checkbox_type(
                "att_checkbox", [["a", "b", "m"]], ["a", "b", "c", "d"]
            )
        assert excinfo.value.args == (
            "Option 'm' for custom_field 'att_checkbox' is not in 'a,b,c,d'",
            "case/custom_field_invalid_option",
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_checkbox_type(
                "att_checkbox",
                [["a", "b", "m"], ["a", "c"]],
                ["a", "b", "c", "d"],
            )
        assert excinfo.value.args == (
            "Multiple values for check box is not supported",
            "case/multi_value_checkbox_bot_supported",
        )

    def test__assert_valid_option_or_select_type(self):
        self.case._assert_valid_option_or_select_type(
            "att_select", ["a", "b"], ["a", "b", "c", "d"]
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_option_or_select_type(
                "att_select", ["a", "m"], ["a", "b", "c", "d"]
            )
        assert excinfo.value.args == (
            "Option 'm' for custom_field 'att_select' is not in 'a,b,c,d'",
            "case/custom_field_invalid_option",
        )

    @mock.patch("zsnl_domains.case_management.entities.case.date")
    def test__calculate_date_from_custom_field_date_limit(self, mock_datetime):
        mock_datetime.today.return_value = date(2019, 1, 3)
        date_limit = CustomFieldDateLimitObject(
            active=True,
            interval_type="days",
            interval=20,
            reference="current",
            during="pre",
        )
        res = self.case._calculate_date_from_custom_field_date_limit(
            date_limit
        )
        assert res == date(2018, 12, 14)

        date_limit = CustomFieldDateLimitObject(
            active=True,
            interval_type="days",
            interval=20,
            reference="current",
            during="post",
        )
        mock_datetime.today.return_value = date(2019, 1, 3)
        res = self.case._calculate_date_from_custom_field_date_limit(
            date_limit
        )
        assert res == date(2019, 1, 23)

        date_limit = CustomFieldDateLimitObject(
            active=True,
            interval_type="weeks",
            interval=4,
            reference="current",
            during="post",
        )
        mock_datetime.today.return_value = date(2019, 1, 3)
        res = self.case._calculate_date_from_custom_field_date_limit(
            date_limit
        )
        assert res == date(2019, 1, 31)

        date_limit = CustomFieldDateLimitObject(
            active=True,
            interval_type="months",
            interval=4,
            reference="current",
            during="post",
        )
        mock_datetime.today.return_value = date(2019, 1, 3)
        res = self.case._calculate_date_from_custom_field_date_limit(
            date_limit
        )
        assert res == date(2019, 5, 3)

        date_limit = CustomFieldDateLimitObject(
            active=True,
            interval_type="years",
            interval=4,
            reference="current",
            during="post",
        )
        mock_datetime.today.return_value = date(2019, 1, 3)
        res = self.case._calculate_date_from_custom_field_date_limit(
            date_limit
        )
        assert res == date(2023, 1, 3)

        date_limit = CustomFieldDateLimitObject(
            active=False,
            interval_type="years",
            interval=4,
            reference="current",
            during="post",
        )
        res = self.case._calculate_date_from_custom_field_date_limit(
            date_limit
        )
        assert res is None

    @mock.patch("zsnl_domains.case_management.entities.case.date")
    def test_assert_valid_date_type(self, mock_datetime):
        mock_datetime.today.return_value = date(2019, 1, 3)
        date_field_limit = CustomFieldDateLimit(
            start=CustomFieldDateLimitObject(
                active=True,
                interval_type="days",
                interval=2,
                reference="current",
                during="post",
            ),
            end=CustomFieldDateLimitObject(
                active=True,
                interval_type="days",
                interval=4,
                reference="current",
                during="post",
            ),
        )
        self.case._assert_valid_date_type(
            "att_datum", ["2019-01-05"], date_field_limit
        )

        date_field_limit = CustomFieldDateLimit(
            start=CustomFieldDateLimitObject(
                active=True,
                interval_type="days",
                interval=2,
                reference="current",
                during="post",
            ),
            end=CustomFieldDateLimitObject(
                active=False,
                interval_type="days",
                interval=4,
                reference="current",
                during="post",
            ),
        )
        self.case._assert_valid_date_type(
            "att_datum", ["2020-01-05"], date_field_limit
        )

        date_field_limit = CustomFieldDateLimit(start=None, end=None)
        self.case._assert_valid_date_type(
            "att_datum", ["2020-01-05"], date_field_limit
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_date_type(
                "att_datum", ["2019-13-33"], date_field_limit
            )
        assert excinfo.value.args == (
            "Value '2019-13-33' for custom field 'att_datum' is not of type 'date'",
            "case/custom_field_invalid_type",
        )

        with pytest.raises(Conflict) as excinfo:
            date_field_limit = CustomFieldDateLimit(
                start=CustomFieldDateLimitObject(
                    active=True,
                    interval_type="days",
                    interval=2,
                    reference="current",
                    during="post",
                ),
                end=CustomFieldDateLimitObject(
                    active=True,
                    interval_type="days",
                    interval=4,
                    reference="current",
                    during="post",
                ),
            )
            self.case._assert_valid_date_type(
                "att_datum", ["2019-01-01"], date_field_limit
            )
        assert excinfo.value.args == (
            "Date '2019-01-01' is less than start date '2019-01-05'",
            "case/custom_field_date_less_than_start_date",
        )

        with pytest.raises(Conflict) as excinfo:
            date_field_limit = CustomFieldDateLimit(
                start=CustomFieldDateLimitObject(
                    active=True,
                    interval_type="days",
                    interval=2,
                    reference="current",
                    during="post",
                ),
                end=CustomFieldDateLimitObject(
                    active=True,
                    interval_type="days",
                    interval=4,
                    reference="current",
                    during="post",
                ),
            )
            self.case._assert_valid_date_type(
                "att_datum", ["2019-01-08"], date_field_limit
            )
        assert excinfo.value.args == (
            "Date '2019-01-08' is greater than end date '2019-01-07'",
            "case/custom_field_date_greater_than_end_date",
        )

        with pytest.raises(Conflict) as excinfo:
            date_field_limit = CustomFieldDateLimit(
                start=CustomFieldDateLimitObject(
                    active=True,
                    interval_type="days",
                    interval=2,
                    reference="current",
                    during="post",
                ),
                end=CustomFieldDateLimitObject(
                    active=True,
                    interval_type="days",
                    interval=1,
                    reference="current",
                    during="post",
                ),
            )
            self.case._assert_valid_date_type(
                "att_datum", ["2019-01-04"], date_field_limit
            )
        assert excinfo.value.args == (
            "Start date '2019-01-05' is greater than '2019-01-04'",
            "case/custom_field_start_date_greater_than_end_date",
        )

    def test_is_valid_geo_json_point(self):
        geo_json = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Point",
                        "coordinates": [64.99793920061401, 90.87890625],
                    },
                }
            ],
        }
        res = self.case._is_valid_geo_json_point(geo_json)
        assert res is True

        geo_json = "test"
        res = self.case._is_valid_geo_json_point(geo_json)
        assert res is False

        geo_json = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {"type": "Point", "coordinates": [-90, -180]},
                }
            ],
        }
        res = self.case._is_valid_geo_json_point(geo_json)
        assert res is True

        geo_json = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [
                            [
                                [54.66796875, 76.84081641443098],
                                [56.6015625, 76.59854506890699],
                                [61.17187499999999, 75.62863223279015],
                                [74.00390625, 76.55774293896555],
                                [63.80859374999999, 77.87881372624746],
                                [54.66796875, 76.84081641443098],
                            ]
                        ],
                    },
                }
            ],
        }
        res = self.case._is_valid_geo_json_point(geo_json)
        assert res is False

        geo_json = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {"type": "Point", "coordinates": [-97, 64]},
                }
            ],
        }
        res = self.case._is_valid_geo_json_point(geo_json)
        assert res is False

    def test__assert_valid_geolatlon_type(self):
        custom_field_values = [
            {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "Point",
                            "coordinates": [79.30263962053658, 135.703125],
                        },
                    }
                ],
            }
        ]

        self.case._assert_valid_geolatlon_type(
            "att_geolatlon", custom_field_values
        )

        with pytest.raises(Conflict) as excinfo:
            custom_field_values = [(123, 345)]
            self.case._assert_valid_geolatlon_type(
                "att_geolatlon", custom_field_values
            )
        assert excinfo.value.args == (
            "Value '(123, 345)' for custom field 'att_geolatlon' is not a valid geoJSON Point",
            "case/custom_field_invalid_type",
        )

    def test_magic_strings_lookup(self):
        self.case.status = "stalled"
        keys_ = [
            "zaaknummer",
            "alternatief_zaaknummer",
            "startdatum",
            "laatst_gewijzigd",
            "datum_aangemaakt",
            "datum",
            "null",
            "aggregatieniveau",
            "registratiedatum",
            "registratiedatum_volledig",
            "afhandeldatum",
            "afhandeldatum_volledig",
            "uiterste_vernietigingsdatum",
            "zaak_bedrag",
            "bedrag_web",
            "pdc_tarief",
            "status",
            "archiefstatus",
            "streefafhandeldatum",
            "contactkanaal",
            "opgeschort_tot",
            "opgeschort_sinds",
            "reden_opschorting",
            "selectielijst",
            "vertrouwelijkheid",
            "behandelaar_id",
            "behandelaar",
            "behandelaar_nummer",
            "coordinator_id",
            "coordinator",
            "coordinator_nummer",
            "zaak_onderwerp",
            "zaak_onderwerp_external",
            "betaalstatus",
            "resultaat",
            "resultaat_id",
        ]

        result = self.case.magic_strings_lookup()
        for key in keys_:
            assert key in result

    def test__is_valid_phone_number(self):
        telephone_number = "310619376846"
        res = self.case._is_valid_phone_number(telephone_number)
        assert res is True
        telephone_number = "+31061924989801"
        res = self.case._is_valid_phone_number(telephone_number)
        assert res is True
        telephone_number = "+31"
        res = self.case._is_valid_phone_number(telephone_number)
        assert res is False
        telephone_number = "+310616538756398674"
        res = self.case._is_valid_phone_number(telephone_number)
        assert res is False

    def test__assert_valid_contact_information(self):
        contact_information = {
            "mobile_number": "+31061924989801",
            "phone_number": "+31061924989801",
            "email": "bb@bb.com",
        }
        self.case._assert_valid_contact_information(
            contact_information=contact_information
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_contact_information(
                contact_information={"mobile_number": "+31"}
            )
        assert excinfo.value.args == (
            "'+31' is not a valid mobile number",
            "case/invalid_mobile_number",
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_contact_information(
                contact_information={"phone_number": "++31o123"}
            )
        assert excinfo.value.args == (
            "'++31o123' is not a valid phone number",
            "case/invalid_phone_number",
        )

        with pytest.raises(Conflict) as excinfo:
            self.case._assert_valid_contact_information(
                contact_information={"email": "test123"}
            )
        assert excinfo.value.args == (
            "'test123' is not a valid email address",
            "case/invalid_email",
        )

    def test_set_allocation(self):
        department = Department(
            uuid=uuid4(), name="development", description="code code code"
        )
        role = Role(uuid=uuid4(), name="admin")
        self.case.set_allocation(
            department=department,
            role=role,
            template_service=self.template_service,
        )

        assert self.case.department == department
        assert self.case.role == role

    def test_enqueue_assignee_email(self):
        self.case.enqueue_assignee_email()

    def test_send_assignee_email(self):
        queue_id = str(uuid4())
        self.case.send_assignee_email(queue_id=queue_id)

        assert self.case.queue_ids == json.dumps([queue_id])

    def test_clear_assignee(self):
        self.case.clear_assignee(template_service=self.template_service)
        assert self.case.assignee is None

    def test_update_custom_field(self):
        field_value = {
            "value": [
                {
                    "type": "relationship",
                    "value": "8e118737-0d65-4713-ad58-52dff8cea5ef",
                    "specifics": {
                        "relationship_type": "custom_object",
                        "metadata": {
                            "description": "exising_object_subtitle",
                            "summary": "existing_object_title",
                        },
                    },
                },
                {
                    "type": "relationship",
                    "value": uuid4(),
                    "specifics": {
                        "relationship_type": "custom_object",
                        "metadata": {
                            "description": "description value",
                            "summary": "summary value",
                        },
                    },
                },
            ],
            "specifics": None,
            "type": "relationship",
        }

        self.case.update_custom_field(
            magic_string="my_custom_field", new_value=field_value
        )
        assert self.case.custom_fields["my_custom_field"] == field_value


class TestSubjectEntity:
    def setup(self):
        uuid = uuid4()
        self.subject = Subject(
            id=1,
            uuid=uuid,
            subject_type="employee",
            properties={},
            settings={},
            username="username",
            last_modified=date(2019, 4, 19),
            role_ids=[],
            group_ids=[],
            nobody=False,
            system=True,
            entity_id=uuid,
        )

    def test_entity_id(self):
        res = self.subject.entity_id
        assert res == self.subject.uuid


class TestCaseMessageList:
    def setup(self):
        event_service = mock.MagicMock()
        self.case_message_list = CaseMessageList(
            case_uuid=uuid4(), case_id=100
        )
        self.case_message_list.event_service = event_service

    def test_assign_to_user(self):
        subject = Subject(
            id=1,
            uuid=uuid4(),
            subject_type="employee",
            properties={},
            settings={},
            username="username",
            last_modified=date(2019, 4, 19),
            role_ids=[],
            group_ids=[],
            nobody=False,
            system=True,
        )
        self.case_message_list.assign_to_user(user=subject)
        assert self.case_message_list.user == subject

    def test_assign_to_user_conflict(self):
        subject = Subject(
            id=1,
            uuid=uuid4(),
            subject_type="admin",
            properties={},
            settings={},
            username="username",
            last_modified=date(2019, 4, 19),
            role_ids=[],
            group_ids=[],
            nobody=False,
            system=True,
        )
        with pytest.raises(Conflict):
            self.case_message_list.assign_to_user(user=subject)

    def test_entity_id(self):
        res = self.case_message_list.entity_id
        assert res == self.case_message_list.case_uuid


class TestOrganizationEntity:
    def setup(self):
        organization_id = uuid4()
        self.organization = Organization(
            entity_id=organization_id,
            uuid=organization_id,
            name="test org",
            source="Zaaksysteem",
            coc_number="12312312",
            coc_location_number=1,
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            organization_type="service",
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object=None,
            has_valid_address=True,
            authenticated=True,
        )

    def test_entity_id(self):
        res = self.organization.entity_id
        assert res == self.organization.uuid


class TestSubjectRelationEntity:
    def setup(self):
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        uuid = uuid4()
        self.subject_relation = SubjectRelation(
            entity_id=uuid,
            uuid=uuid,
            magic_string_prefix="behandlaar1",
            role="Behandlaar",
            authorized=True,
            case={"type": "case", "id": uuid4()},
            subject={"type": "employee", "id": uuid4()},
            _event_service=event_service,
        )

    def test_entity_id(self):
        res = self.subject_relation.entity_id
        assert res == self.subject_relation.uuid

    def test_create(self):
        self.subject_relation.create(
            role="Advocaat",
            magic_string_prefix="advocaat1",
            authorized=True,
            case={"id": uuid4(), "type": "case"},
            subject={"type": "employee", "id": uuid4()},
            send_confirmation_email=True,
            permission="write",
        )
        assert self.subject_relation.role == "Advocaat"
        assert self.subject_relation.magic_string_prefix == "advocaat1"
        assert self.subject_relation.authorized is False
        assert self.subject_relation.permission == "write"

        self.subject_relation.create(
            role="Advocaat",
            magic_string_prefix="advocaat1",
            authorized=True,
            case={"id": uuid4(), "type": "case"},
            subject={"type": "organization", "id": uuid4()},
            send_confirmation_email=True,
            permission="write",
        )
        assert self.subject_relation.permission == "write"

        with pytest.raises(Conflict) as excinfo:
            self.subject_relation.create(
                role="Advocaat",
                magic_string_prefix="advocaat1",
                authorized=True,
                case={"id": uuid4(), "type": "case"},
                subject={"type": "employee", "id": uuid4()},
                send_confirmation_email=True,
                permission="edit",
            )
        assert excinfo.value.args == (
            "'edit' is not a valid permission",
            "subject_relation/invalid_permission",
        )

    def test_send_email(self):
        self.subject_relation.send_email()
        res = self.subject_relation.entity_id
        assert res == self.subject_relation.uuid

    def test_update(self):
        self.subject_relation.subject = {
            "type": "employee",
            "id": str(uuid4()),
        }
        self.subject_relation.authorized = False
        self.subject_relation.update(
            role="Advocaat",
            magic_string_prefix="advocaat1",
            permission="write",
        )
        assert self.subject_relation.role == "Advocaat"
        assert self.subject_relation.magic_string_prefix == "advocaat1"
        assert self.subject_relation.permission == "write"

        self.subject_relation.update(
            role="Role", magic_string_prefix="rol", permission=None
        )
        assert self.subject_relation.role == "Role"
        assert self.subject_relation.magic_string_prefix == "rol"
        assert self.subject_relation.permission is None

        with pytest.raises(Conflict) as excinfo:
            self.subject_relation.update(
                role="Advocaat",
                magic_string_prefix="advocaat1",
                authorized=True,
                permission="edit",
            )
        assert excinfo.value.args == (
            "'edit' is not a valid permission",
            "subject_relation/invalid_permission",
        )

        self.subject_relation.role = "Aanvrager"
        with pytest.raises(Conflict) as excinfo:
            self.subject_relation.update(
                role="Advocaat",
                magic_string_prefix="advocaat1",
                permission="write",
            )
        assert excinfo.value.args == (
            "Subject relation with role 'aanvrager' cannot be updated",
            "subject_relation/cannot_update_aanvrager",
        )

        self.subject_relation.subject = {
            "type": "organization",
            "id": str(uuid4()),
        }
        self.subject_relation.role = "Aannemer"
        self.subject_relation.authorized = False
        self.subject_relation.permission = None
        self.subject_relation.update(
            role="Advocaat", magic_string_prefix="advocaat1", authorized=True
        )
        assert self.subject_relation.authorized is True
        assert self.subject_relation.magic_string_prefix == "advocaat1"
        assert self.subject_relation.role == "Advocaat"
        assert self.subject_relation.permission is None

    def test_delete(self):
        self.subject_relation.delete()

        self.subject_relation.role = "Aanvrager"
        with pytest.raises(Conflict) as excinfo:
            self.subject_relation.delete()

        assert excinfo.value.args == (
            "You cannot delete a subject_relation with 'Aanvrager' role",
            "subject_relation/cannot_delete_aanvrager",
        )

        self.subject_relation.role = "Advocaat"
        self.subject_relation.delete()


class TestExportFileEntity:
    def setup(self):
        uuid = uuid4()
        self.export_file = ExportFile(
            id=1,
            uuid=uuid,
            name="exportr_file_name",
            expires_at=datetime.strptime("2010-12-31", "%Y-%m-%d"),
            downloads=0,
            entity_id=uuid,
        )

    def test_entity_id(self):
        res = self.export_file.entity_id
        assert res == self.export_file.uuid
