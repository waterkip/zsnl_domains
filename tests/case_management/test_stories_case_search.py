# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from datetime import datetime
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.queries.case_search_result import (
    CaseSearchResultFilter,
)
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidContactChannel,
)


class Test_Case_Search_Queries(TestBase):

    mock_db_rows = [mock.Mock(), mock.Mock()]
    mock_db_rows[0].configure_mock(
        id=2,
        uuid=uuid4(),
        status="new",
        vernietigingsdatum=None,
        archival_state=None,
        percentage_days_left=4,
        onderwerp="subject of case",
        progress=0.5,
        titel="case type title",
        unaccepted_files_count=1,
        unread_communication_count=2,
        unaccepted_attribute_update_count=3,
        requestor_name="Diederik-Jan",
        requestor_type="person",
        requestor_uuid=uuid4(),
        assignee_name="Sjaak",
        assignee_uuid=uuid4(),
        registratiedatum="2022-02-08",
        afhandeldatum="2022-02-08",
    )
    mock_db_rows[1].configure_mock(
        id=3,
        uuid=uuid4(),
        status="pending",
        vernietigingsdatum=datetime.now(),
        archival_state="vernietigen",
        percentage_days_left=152,
        onderwerp="subject of case",
        progress=1.0,
        titel="case type title2",
        unaccepted_files_count=4,
        unread_communication_count=5,
        unaccepted_attribute_update_count=6,
        requestor_name="Donald Duck",
        requestor_type="person",
        requestor_uuid=uuid4(),
        assignee_name="Dagobert",
        assignee_uuid=uuid4(),
        registratiedatum="2022-02-08",
        afhandeldatum="2022-02-08",
    )

    def setup(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_search_case(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        case_search_result = self.qry.search_case(page=1, page_size=10)

        assert len(case_search_result.entities) == 2
        # verify 1st search result
        search_result1 = case_search_result.entities[0]
        assert search_result1.number == 2
        assert search_result1.status == "new"
        assert search_result1.destruction_date is None
        assert search_result1.archival_state is None
        assert search_result1.percentage_days_left == 4
        assert search_result1.subject == "subject of case"
        assert search_result1.case_type_title == "case type title"
        assert search_result1.requestor.type == "person"
        assert search_result1.requestor.name == "Diederik-Jan"
        assert search_result1.assignee.name == "Sjaak"
        assert search_result1.progress == 50
        assert search_result1.unread_message_count == 2
        assert search_result1.unaccepted_files_count == 1
        assert search_result1.unaccepted_attribute_update_count == 3

        # verify 2nd search result
        search_result2 = case_search_result.entities[1]
        assert search_result2.number == 3
        assert search_result2.status == "pending"
        assert search_result2.destruction_date is not None
        assert search_result2.archival_state == "vernietigen"
        assert search_result2.percentage_days_left == 152
        assert search_result2.subject == "subject of case"
        assert search_result2.case_type_title == "case type title2"
        assert search_result2.requestor.type == "person"
        assert search_result2.requestor.name == "Donald Duck"
        assert search_result2.assignee.name == "Dagobert"
        assert search_result2.progress == 100
        assert search_result2.unread_message_count == 5
        assert search_result2.unaccepted_files_count == 4
        assert search_result2.unaccepted_attribute_update_count == 6

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_status(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_status = {ValidCaseStatus.new, ValidCaseStatus.open}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.status IN (%(status_2_1)s, %(status_2_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_casetype_uuid(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.case_type_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaaktype.uuid IN (%(uuid_2_1)s, %(uuid_2_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_assignee(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.assignee_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_betrokkenen_1.subject_id IN (%(subject_id_1_1)s, %(subject_id_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_coordinator(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.coordinator_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_betrokkenen_2.subject_id IN (%(subject_id_1_1)s, %(subject_id_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_requestor(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.requestor_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_betrokkenen_3.subject_id IN (%(subject_id_1_1)s, %(subject_id_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_registration_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_registration_date = ["gt 2022-02-08T11:13:16Z"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.registratiedatum > %(registratiedatum_1)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_completion_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_completion_date = ["gt 2022-01-04T14:02:11Z"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.afhandeldatum > %(afhandeldatum_1)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_payment_status(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_payment_status = {
            ValidCasePaymentStatus.success,
            ValidCasePaymentStatus.pending,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.payment_status IN (%(payment_status_1_1)s, %(payment_status_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_channel_of_contact(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_channel_of_contact = {
            ValidContactChannel.behandelaar,
            ValidContactChannel.post,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.contactkanaal IN (%(contactkanaal_1_1)s, %(contactkanaal_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_confidentiality(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_confidentiality = {
            ValidCaseConfidentiality.public,
            ValidCaseConfidentiality.internal,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.confidentiality IN (%(confidentiality_1_1)s, %(confidentiality_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_archival_state(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_archival_state = {
            ValidCaseArchivalState.vernietigen,
            ValidCaseArchivalState.overdragen,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.archival_state IN (%(archival_state_1_1)s, %(archival_state_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_retention_period_source_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_retention_period_source_date = {
            ValidCaseRetentionPeriodSourceDate.vervallen
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaaktype_resultaten_1.ingang IN (%(ingang_1_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_result(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_result = {ValidCaseResult.aangegaan}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.resultaat IN (%(resultaat_1_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_case_location(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_case_location = {
            "nummeraanduiding-0437200000001964",
            "openbareruimte-0437300000000021",
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak_bag_1.bag_id IN (%(bag_id_1_1)s, %(bag_id_1_2)s) AND zaak_bag_1.bag_type = %(bag_type_1)s OR zaak_bag_1.bag_id IN (%(bag_id_2_1)s, %(bag_id_2_2)s) AND zaak_bag_1.bag_type = %(bag_type_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unread_messages_and_sort_asc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_num_unread_messages = ["gt 2"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, sort="unread_message_count"
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_meta.unread_communication_count > %(unread_communication_count_1)s ORDER BY zaak_meta.unread_communication_count ASC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unaccepted_files_sort_desc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_num_unaccepted_files = ["gt 1"]
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="-unaccepted_files_count",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_meta.unaccepted_files_count > %(unaccepted_files_count_1)s ORDER BY zaak_meta.unaccepted_files_count DESC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unaccepted_updates_sort_asc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_num_unaccepted_updates = ["gt 1"]
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="unaccepted_attribute_update_count",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_meta.unaccepted_attribute_update_count > %(unaccepted_attribute_update_count_1)s ORDER BY zaak_meta.unaccepted_attribute_update_count ASC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_keyword(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_keyword = "test"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.id = zaak_meta.zaak_id AND zaak_meta.text_vector @@ to_tsquery(%(text_vector_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_period_of_preservation_active(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_period_of_preservation_active = True
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaaktype_resultaten_1.trigger_archival IS true ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_subject(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_subject = "test"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.onderwerp ILIKE %(onderwerp_1)s ESCAPE '~' ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_total_count(self):
        self.session.execute().fetchall.return_value = 2
        self.session.reset_mock()

        self.qry.search_case_total_results()

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        query = call_list[0][0][0]
        query = query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s)))"
        )
