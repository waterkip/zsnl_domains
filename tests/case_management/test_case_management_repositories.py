# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from datetime import datetime
from minty.cqrs import EventService, UserInfo, events, test
from minty.exceptions import Conflict, NotFound
from sqlalchemy import sql
from unittest import mock
from urllib.error import HTTPError
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import (
    Case,
    CaseMessageList,
    CaseTypeVersionEntity,
    Country,
    Department,
    Role,
    SubjectRelation,
)
from zsnl_domains.case_management.entities.case_type_version import (
    CaseRelatedToCaseType,
    CaseTypeAllocation,
    CaseTypePhase,
    CustomFieldRelatedToCaseType,
)
from zsnl_domains.case_management.repositories import (
    CaseMessageListRepository,
    CaseRepository,
    CountryRepository,
    ExportFileRepository,
    SubjectRelationRepository,
    case_acl,
)
from zsnl_domains.case_management.repositories.database_queries import (
    case_action_data_from_sub_case_query,
    case_action_label_query,
    case_action_status_query,
)
from zsnl_domains.database import schema


class InfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestCaseRepository:
    @mock.patch("sqlalchemy.orm.Session")
    def setup(self, mock_session):
        self.mock_infra = mock.MagicMock()
        event_service = EventService(
            correlation_id=uuid4(),
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        self.case_repo = CaseRepository(
            infrastructure_factory=self.mock_infra,
            context=None,
            event_service=event_service,
        )

    def test_get_session(self):
        assert self.case_repo.session is self.mock_infra.get_infrastructure(
            context="context", infrastructure_name="database"
        )

    @mock.patch("sqlalchemy.orm.Session")
    def test_get_requestor_by_id(self, mock_session):
        subject_id = str(uuid4())
        requestor = mock.MagicMock()
        requestor.betrokkene_type = "medewerker"
        requestor.subject_id = subject_id
        mock_session.query().filter().one.return_value = requestor

        result = case_acl.get_requestor_by_id(1, mock_session)
        assert result == {"type": "employee", "uuid": subject_id}

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.add_entity_from_natural_person"
    )
    @mock.patch.object(CaseRepository, "_add_snapshots_for_person")
    def test_update_requestor_for_natural_person(
        self, mock_add_snapshots, mock_add_entity
    ):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses
        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}

        uuid = uuid4()
        person = [
            {
                "uuid": uuid,
                "id": "1",
                "voornamen": "Edwin",
                "voorvoegsel": "T",
                "geslachtsnaam": "Jaison",
                "naamgebruik": "T Jaison",
                "geslachtsaanduiding": "M",
                "burgerservicenummer": "1234567",
                "geboorteplaats": "Amsterdam",
                "geboorteland": "Amsterdam",
                "geboortedatum": None,
                "landcode": 6030,
                "active": True,
                "adellijke_titel": "Mr.",
                "datum_huwelijk_ontbinding": None,
                "onderzoek_persoon": None,
                "datum_huwelijk": None,
                "a_nummer": None,
                "indicatie_geheim": None,
                "address": address,
                "contact_data": contact_data,
                "datum_overlijden": None,
            }
        ]

        mock_ses.execute().fetchone.return_value = person
        mock_add_entity.return_value = 243
        requestor = {
            "uuid": uuid,
            "type": "person",
            "id": "1",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": "M",
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": None,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": None,
            "address": address,
            "contact_data": contact_data,
        }

        involved_entity_id, requestor_id = self.case_repo._update_requestor(
            case_id=None, requestor=requestor
        )
        assert involved_entity_id == 243
        assert requestor_id == person[0]["id"]

        with pytest.raises(NotFound) as excinfo:
            mock_ses.execute().fetchone.side_effect = [None]
            self.case_repo._update_requestor(case_id=None, requestor=requestor)
        assert excinfo.value.args == (
            f"Person with uuid '{uuid}' not found.",
            "natuurlijk_persoon/not_found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.add_entity_from_subject"
    )
    @mock.patch.object(CaseRepository, "_generate_entity_from_employee")
    def test_update_requestor_for_employee(
        self, mock_generate_requestor, mock_add_entity
    ):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses

        subject = [
            {
                "id": "1",
                "uuid": "599be3ca-bf98-4ad9-9731-d1fdb6f9155d",
                "properties": '{"display_name":"Admin"}',
            }
        ]

        mock_ses.execute().fetchone.return_value = subject
        mock_add_entity.return_value = 243

        requestor = {
            "type": "employee",
            "uuid": "599be3ca-bf98-4ad9-9731-d1fdb6f9155d",
        }
        involved_entity_id, requestor_id = self.case_repo._update_requestor(
            case_id=None, requestor=requestor
        )
        assert involved_entity_id == 243
        assert requestor_id == subject[0]["id"]

        with pytest.raises(NotFound) as excinfo:
            mock_ses.execute().fetchone.side_effect = [None]
            self.case_repo._update_requestor(case_id=None, requestor=requestor)
        assert excinfo.value.args == (
            "Employee with uuid '599be3ca-bf98-4ad9-9731-d1fdb6f9155d' not found.",
            "employee/not_found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.add_entity_from_organization"
    )
    @mock.patch.object(CaseRepository, "_add_snapshot_for_organization")
    @mock.patch.object(CaseRepository, "_generate_entity_from_organization")
    def test_update_requestor_for_organization(
        self, mock_generate_requestor, mock_add_snapshots, mock_add_entity
    ):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses

        mock_add_entity.return_value = 243
        organization = [
            {
                "id": "1",
                "uuid": "599be3ca-bf98-4ad9-9731-d1fdb6f9155d",
                "handelsnaam": "test",
            }
        ]

        mock_ses.execute().fetchone.return_value = organization
        requestor = {
            "type": "organization",
            "uuid": "599be3ca-bf98-4ad9-9731-d1fdb6f9155d",
        }
        involved_entity_id, requestor_id = self.case_repo._update_requestor(
            case_id=None, requestor=requestor
        )
        assert involved_entity_id == 243
        assert requestor_id == organization[0]["id"]

        with pytest.raises(NotFound) as excinfo:
            mock_ses.execute().fetchone.side_effect = [None]
            self.case_repo._update_requestor(case_id=None, requestor=requestor)
        assert excinfo.value.args == (
            "Organization with uuid '599be3ca-bf98-4ad9-9731-d1fdb6f9155d' not found.",
            "organization/not_found",
        )

        with pytest.raises(Conflict) as excinfo:
            requestor = {
                "type": "group",
                "id": "599be3ca-bf98-4ad9-9731-d1fdb6f9155d",
            }
            self.case_repo._update_requestor(case_id=None, requestor=requestor)
        assert excinfo.value.args == (
            "Requestor of type 'group' is not allowed for a case.",
        )

    def test__add_snapshots_for_person(self):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses

        mock_insert = mock.MagicMock()
        mock_insert.inserted_primary_key = [1]
        mock_ses.execute.return_value = mock_insert

        address = mock.MagicMock()
        address.configure_mock(
            id=3,
            straatnaam="vrijzicht",
            huisnummer=12,
            huisnummertoevoeging=None,
            nadere_aanduiding=None,
            postcode="1875RT",
            woonplaats="Amstelveen",
            landcode=6030,
            natuurlijk_persoon_id=3,
            gemeentedeel=None,
            functie_adres="W",
            datum_aanvang_bewoning=None,
            woonplaats_id=None,
            gemeente_code=None,
            hash=None,
            import_datum="2021-06-15T16:19:45",
            deleted_on=None,
            adres_buitenland1=None,
            adres_buitenland2=None,
            adres_buitenland3=None,
        )
        person = mock.MagicMock()

        person.configure_mock(
            id=1,
            uuid="599be3ca-bf98-4ad9-9731-d1fdb6f9155d",
            object_type=None,
            voornamen="Edwin",
            voorvoegsel=None,
            geslachtsnaam="Jaison",
            active=True,
            naamgebruik="Peter",
            adellijke_titel=None,
            search_term=None,
            burgerservicenummer=1234567,
            a_nummer=None,
            voorletters=None,
            geslachtsaanduiding="M",
            nationaliteitscode1=None,
            nationaliteitscode2=None,
            nationaliteitscode3=None,
            geboorteplaats=None,
            geboorteland=None,
            geboortedatum=None,
            aanhef_aanschrijving=None,
            voorletters_aanschrijving=None,
            voornamen_aanschrijving=None,
            naam_aanschrijving=None,
            voorvoegsel_aanschrijving=None,
            burgerlijke_staat=None,
            indicatie_geheim=None,
            land_waarnaar_vertrokken=None,
            import_datum=None,
            adres_id=None,
            authenticated=None,
            authenticatedby=None,
            deleted_on=None,
            verblijfsobject_id=None,
            datum_overlijden=None,
            aanduiding_naamgebruik=None,
            onderzoek_persoon=None,
            onderzoek_huwelijk=None,
            onderzoek_overlijden=None,
            onderzoek_verblijfplaats=None,
            partner_a_nummer=None,
            partner_burgerservicenummer=None,
            partner_voorvoegsel=None,
            partner_geslachtsnaam=None,
            datum_huwelijk=None,
            datum_huwelijk_ontbinding=None,
            in_gemeente=None,
            landcode=6030,
            address=address,
        )

        res = self.case_repo._add_snapshots_for_person(person=person)
        assert res == 1

    def test__add_snapshot_for_organization(self):

        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses

        mock_insert = mock.MagicMock()
        mock_insert.inserted_primary_key = [2]
        mock_ses.execute.return_value = mock_insert
        org = mock.MagicMock()
        org.configure_mock(
            uuid=str(uuid4()),
            id=3,
            object_type=None,
            handelsnaam="test company",
            email="test@test.nl",
            telefoonnummer=1234456,
            werkzamepersonen=None,
            dossiernummer=1234,
            subdossiernummer=None,
            hoofdvestiging_dossiernummer=None,
            hoofdvestiging_subdossiernummer=None,
            vorig_dossiernummer=None,
            vorig_subdossiernummer=None,
            rechtsvorm=102,
            kamernummer=None,
            faillisement=None,
            surseance=None,
            contact_naam=None,
            contact_aanspreektitel=None,
            contact_voorvoegsel=None,
            contact_voorletters=None,
            contact_geslachtsnaam=None,
            contact_geslachtsaanduiding=None,
            vestiging_adres=None,
            vestiging_straatnaam=None,
            vestiging_huisnummer=None,
            vestiging_huisnummertoevoeging=None,
            vestiging_postcodewoonplaats=None,
            vestiging_postcode=None,
            vestiging_woonplaats=None,
            correspondentie_straatnaam="Overamstel",
            correspondentie_huisnummer=50,
            correspondentie_huisnummertoevoeging=None,
            correspondentie_postcodewoonplaats="Amsterdam",
            correspondentie_postcode="1234ER",
            correspondentie_woonplaats=None,
            correspondentie_adres=None,
            hoofdactiviteitencode=None,
            nevenactiviteitencode1=None,
            nevenactiviteitencode2=None,
            authenticated=None,
            authenticatedby=None,
            fulldossiernummer=None,
            import_datum=None,
            deleted_on=None,
            verblijfsobject_id=None,
            system_of_record=None,
            system_of_record_id=None,
            vestigingsnummer=None,
            vestiging_huisletter=None,
            correspondentie_huisletter=None,
            vestiging_adres_buitenland1=None,
            vestiging_adres_buitenland2=None,
            vestiging_adres_buitenland3=None,
            vestiging_landcode=None,
            correspondentie_adres_buitenland1=None,
            correspondentie_adres_buitenland2=None,
            correspondentie_adres_buitenland3=None,
            correspondentie_landcode=None,
        )
        res = self.case_repo._add_snapshot_for_organization(organization=org)
        assert res == 2

    @mock.patch.object(CaseRepository, "_get_case_number_prefix_from_config")
    @mock.patch.object(CaseRepository, "_update_requestor")
    def test__generate_database_values(
        self, mock_update_requestor, mock_get_case_number_prefix
    ):
        case_type_version_uuid = uuid4()
        uuid = uuid4()
        role_uuid = uuid4()
        department_uuid = uuid4()
        formatted_changes = {
            "requestor": {
                "type": "employee",
                "id": "599be3ca-bf98-4ad9-9731-d1fdb6f9155d",
            },
            "contact_channel": "email",
            "role": {"type": "department", "entity_id": role_uuid},
            "department": {"type": "department", "entity_id": department_uuid},
            "case_type_version": {"uuid": uuid4()},
        }

        mock_update_requestor.return_value = 243, 1
        mock_get_case_number_prefix.return_value = "ZS"

        res = self.case_repo._generate_database_values(
            uuid=uuid, formatted_changes=formatted_changes
        )
        assert res["uuid"] == uuid
        assert res["aanvrager"] == 243
        assert res["aanvrager_gm_id"] == 1
        assert res["contactkanaal"] == "email"
        assert str(res["zaaktype_id"]) == str(
            sql.select([schema.ZaaktypeNode.zaaktype_id])
            .where(schema.ZaaktypeNode.uuid == case_type_version_uuid)
            .scalar_subquery()
        )
        assert str(res["route_role"]) == str(
            sql.select([schema.Role.id])
            .where(schema.Role.uuid == role_uuid)
            .scalar_subquery()
        )
        assert str(res["route_ou"]) == str(
            sql.select([schema.Group.id])
            .where(schema.Group.uuid == department_uuid)
            .scalar_subquery()
        )
        assert res["prefix"] == "ZS"

    def test__generate_entity_from_person(self):
        address = mock.MagicMock()
        address = {
            "straatnaam": "Vrijzicht",
            "huisnummer": 50,
            "huisnummertoevoeging": "ABC",
            "woonplaats": "Amsterdam",
            "postcode": "1234ER",
            "adres_buitenland1": None,
            "adres_buitenland2": None,
            "adres_buitenland3": None,
            "functie_adres": "W",
        }
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        person = mock.MagicMock()
        person = {
            "uuid": uuid4(),
            "id": "1",
            "voornamen": "Edwin",
            "voorvoegsel": "T",
            "geslachtsnaam": "Jaison",
            "naamgebruik": "T Jaison",
            "geslachtsaanduiding": "M",
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Mr.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": None,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": None,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }

        res = self.case_repo._generate_entity_from_person(person=person)
        assert res.initials == "E."
        assert res.name == "Edwin T Jaison"
        assert res.naamgebruik == "T Jaison"
        assert res.noble_title == "Mr."
        assert res.salutation == "meneer"
        assert res.salutation1 == "heer"
        assert res.salutation2 == "de heer"
        assert res.status == "Actief"
        assert res.residence_zipcode == "1234ER"
        assert res.house_number == "50 - ABC"
        assert res.country_of_residence == "Nederland"
        assert res.mobile_number == "064535353"
        assert res.email == "edwin@ed.com"

        person2 = {
            "uuid": str(uuid4()),
            "id": 1,
            "voornamen": None,
            "voorvoegsel": "P",
            "geslachtsnaam": "Paul",
            "naamgebruik": "P Paul",
            "geslachtsaanduiding": "V",
            "burgerservicenummer": "1234567",
            "geboorteplaats": "Amsterdam",
            "geboorteland": "Amsterdam",
            "geboortedatum": None,
            "landcode": 6030,
            "active": True,
            "adellijke_titel": "Miss.",
            "datum_huwelijk_ontbinding": None,
            "onderzoek_persoon": None,
            "datum_huwelijk": None,
            "a_nummer": None,
            "indicatie_geheim": None,
            "address": address,
            "contact_data": contact_data,
            "datum_overlijden": None,
        }

        res = self.case_repo._generate_entity_from_person(person=person2)
        assert res.name == "P Paul"
        assert res.naamgebruik == "P Paul"
        assert res.noble_title == "Miss."
        assert res.salutation == "mevrouw"
        assert res.salutation1 == "heer"
        assert res.salutation2 == "de heer"
        assert res.status == "Actief"
        assert res.residence_zipcode == "1234ER"
        assert res.house_number == "50 - ABC"
        assert res.country_of_residence == "Nederland"
        assert res.mobile_number == "064535353"
        assert res.email == "edwin@ed.com"

    def test__generate_entity_from_employee(self):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses
        employee_address = mock.MagicMock()

        employee_address = [
            {
                "id": 1,
                "parameter": "customer_info_huisnummer",
                "value": 34,
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 2,
                "parameter": "customer_info_straatnaam",
                "value": "xyz",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
            {
                "id": 3,
                "parameter": "customer_info_postcode",
                "value": "1234RT",
                "advanced": "t",
                "uuid": uuid4(),
                "definition_id": uuid4(),
            },
        ]
        properties = '{"default_dashboard": 1,"mail": "xyz@xyz.com","sn": "min","title": "Administrator","displayname": "Admin","initials": "A.","givenname": "Ad min","telephonenumber": "061234560"}'
        mock_subject = mock.MagicMock()
        mock_subject = {
            "id": "1",
            "uuid": "599be3ca-bf98-4ad9-9731-d1fdb6f9155d",
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": employee_address,
            "Group": [],
        }

        mock_ses.query().filter().one.return_value = namedtuple(
            "Department", "department"
        )(department="Development")
        mock_ses.execute().fetchone.return_value = namedtuple(
            "Department", "department"
        )(department="Development")
        res = self.case_repo._generate_entity_from_employee(
            subject=mock_subject
        )

        assert res.initials == "A."
        assert res.name == "Admin"
        assert res.surname == "min"
        assert res.phone_number == "061234560"
        assert res.department == "Development"
        assert res.email == "xyz@xyz.com"
        assert res.house_number == "34"
        assert res.zipcode == "1234RT"
        assert res.street == "xyz"

    def test__generate_entity_from_organization(self):
        contact_data = mock.MagicMock()
        contact_data = {"mobiel": "064535353", "email": "edwin@ed.com"}
        correspondence_info = {
            "correspondentie_woonplaats": "Amsterdam",
            "correspondentie_straatnaam": "Overamstel",
            "correspondentie_postcode": "1234ER",
            "correspondentie_adres_buitenland1": None,
            "correspondentie_adres_buitenland2": None,
            "correspondentie_adres_buitenland3": None,
            "correspondentie_landcode": 6030,
            "correspondentie_huisnummer": 50,
            "correspondentie_huisletter": "M",
            "correspondentie_huisnummertoevoeging": "SS",
        }
        org = mock.MagicMock()
        org = {
            "uuid": str(uuid4()),
            "id": 1,
            "handelsnaam": "Mintlab",
            "vestiging_straatnaam": "Overamstel",
            "vestiging_huisnummer": 23,
            "vestiging_huisletter": "P",
            "vestiging_huisnummertoevoeging": "MM",
            "vestiging_postcode": "1234ER",
            "dossiernummer": 50,
            "rechtsvorm": "Internet banking",
            "vestigingsnummer": 3,
            "vestiging_landcode": 6030,
            "contact_data": contact_data,
            "correspondence_info": correspondence_info,
        }
        res = self.case_repo._generate_entity_from_organization(
            organization=org
        )

        assert res.name == "Mintlab"
        assert res.place_of_residence == "Amsterdam"
        assert res.street == "Overamstel"
        assert res.house_number == "23 P - MM"

    def test__generate_case_attribute_insert_value(self):
        case_uuid = uuid4()
        custom_field_definition = CustomFieldRelatedToCaseType(
            **{
                "name": "multi_select",
                "uuid": uuid4(),
                "field_magic_string": "att_checkbox",
                "field_type": "checkbox",
                "is_hidden_field": False,
                "is_required": True,
                "field_options": ["option_a", "option_b"],
            }
        )
        res = self.case_repo._generate_case_attribute_insert_value(
            case_uuid, custom_field_definition, ["option_a", "option_b"]
        )
        assert str(res["zaak_id"]) == str(
            sql.select([schema.Case.id]).where(schema.Case.uuid == case_uuid)
        )
        assert str(res["bibliotheek_kenmerken_id"]) == str(
            sql.select([schema.BibliotheekKenmerk.id]).where(
                schema.BibliotheekKenmerk.magic_string == "att_checkbox"
            )
        )
        assert res["value"] == ["option_a", "option_b"]

    def test__get_custom_field_definiton_from_case_type(self):
        case_type_version_uuid = uuid4()
        case_type = CaseTypeVersionEntity(
            uuid=case_type_version_uuid,
            id="1",
            case_type_uuid=uuid4(),
            version=25,
            name="example casetype",
            description=None,
            identification="example casetype",
            tags=None,
            case_summary="case summary",
            case_public_summary="case summary public",
            active=True,
            requestor={},
            catalog_folder={"uuid": uuid4(), "name": "test folder"},
            metadata={
                "legal_basis": "legal_basis",
                "process_description": "process_description",
            },
            phases=[
                {
                    "name": "Registration",
                    "milestone": 1,
                    "department": {"uuid": uuid4(), "name": "development"},
                    "role": {"uuid": uuid4(), "name": "developemnt"},
                    "phase": "Registering",
                    "custom_fields": [
                        {
                            "uuid": uuid4(),
                            "public_name": "att_text",
                            "name": "text_field",
                            "field_magic_string": "att_text",
                            "field_type": "text",
                            "is_hidden_field": False,
                            "is_required": True,
                            "field_options": [],
                        },
                        {
                            "uuid": uuid4(),
                            "name": "multi_select",
                            "field_magic_string": "att_checkbox",
                            "field_type": "checkbox",
                            "is_hidden_field": False,
                            "is_required": True,
                            "field_options": ["option_a", "option_b"],
                        },
                    ],
                }
            ],
            settings={"payment": {"assignee": {"amount": 1}}},
            terms={
                "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
                "lead_time_service": {
                    "type": "kalenderdagen",
                    "value": "234",
                },
            },
            initiator_source="extern",
            initiator_type="aangan",
            is_eligible_for_case_creation=True,
            preset_assignee=None,
            created=datetime(2019, 7, 27),
            last_modified=datetime(2019, 7, 27),
            case_type_results=[],
        )
        self.case_repo.cache[str(case_type_version_uuid)] = case_type

        res = self.case_repo._get_custom_field_definiton_from_case_type(
            case_type_version_uuid, "att_text"
        )
        assert (
            res.dict().items()
            > {
                "name": "text_field",
                "field_magic_string": "att_text",
                "field_type": "text",
                "is_hidden_field": False,
                "is_required": True,
                "field_options": [],
            }.items()
        )

    @mock.patch.object(CaseRepository, "_generate_case_attribute_insert_value")
    @mock.patch.object(
        CaseRepository, "_get_custom_field_definiton_from_case_type"
    )
    def test__insert_custom_fields(
        self,
        mock__get_custom_field_definition,
        mock_generate_case_attribute_insert_value,
    ):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses
        custom_fields = {
            "att_text": {"value": ["text1", "text2"]},
            "att_url": {"value": ["https://xyz.nl"]},
            "att_checkbox": {"value": ["option_1", "option_2"]},
            "att_select": {"value": ["a"]},
        }

        self.case_repo._insert_custom_fields(uuid4(), uuid4(), custom_fields)

        custom_fields = {"att_text": {"value": []}}
        mock__get_custom_field_definition.return_value = (
            CustomFieldRelatedToCaseType(
                **{
                    "name": "text_field",
                    "uuid": uuid4(),
                    "field_magic_string": "att_text",
                    "field_type": "text",
                    "is_hidden_field": False,
                    "is_required": True,
                    "field_options": [],
                    "default_value": "test",
                }
            )
        )
        self.case_repo._insert_custom_fields(uuid4(), uuid4(), custom_fields)

    @mock.patch.object(CaseRepository, "_get_metadata_for_queue_item")
    @mock.patch("sqlalchemy.sql.insert")
    @mock.patch("zsnl_domains.case_management.repositories.case.datetime")
    def test__enqueue_subjects(
        self, mock_datetime, mock_insert, mock_get_metadata
    ):
        self.case_repo._event = mock.MagicMock()
        case_uuid = str(uuid4())
        queue_id = str(uuid4())
        mock_case = mock.MagicMock()
        mock_case.id = 42
        mock_subject = mock.MagicMock()
        mock_subject.data = {}
        mock_get_metadata.return_value = {"fake_key": "fake_value"}

        self.case_repo._event.event_name = "SubjectsEnqueued"
        self.case_repo._event.entity_id = case_uuid
        self.case_repo._event.changes = [
            {
                "key": "enqueued_subjects_data",
                "old_value": None,
                "new_value": '{"'
                + queue_id
                + '": {"fake_key": "fake_value"}}',
            }
        ]
        self.case_repo.cache[case_uuid] = mock_case
        mock_datetime.now.return_value = 1

        event = self.case_repo._event
        self.case_repo._enqueue_subjects(event, None)
        values = {
            "id": queue_id,
            "object_id": case_uuid,
            "status": "pending",
            "type": "add_case_subject",
            "label": "Create case subject",
            "data": {"loop_protection_counter": 1, "fake_key": "fake_value"},
            "date_created": 1,
            "date_started": None,
            "date_finished": None,
            "parent_id": None,
            "priority": 1000,
            "metadata": {"fake_key": "fake_value"},
        }

        mock_insert.assert_called_once_with(schema.Queue, values=values)

    @mock.patch("zsnl_domains.case_management.repositories.case.urlopen")
    def test__create_subcases(self, mock_urlopen):
        self.case_repo._event = mock.MagicMock()
        self.case_repo._event.event_name = "SubcasesCreated"
        self.case_repo.context = "dev"
        queue_id = str(uuid4())
        self.case_repo._event.changes = [
            {
                "key": "queue_ids",
                "olde_value": None,
                "new_value": '["' + queue_id + '"]',
            }
        ]

        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'
        event = self.case_repo._event
        self.case_repo._create_subcases(event, None)

    @mock.patch("zsnl_domains.case_management.repositories.case.urlopen")
    def test__create_subcases_exception(self, mock_urlopen):
        self.case_repo._event = mock.MagicMock()
        self.case_repo._event.event_name = "SubcasesCreated"
        self.case_repo.context = "dev"
        queue_id = str(uuid4())
        self.case_repo._event.changes = [
            {
                "key": "queue_ids",
                "old_value": None,
                "new_value": '["' + queue_id + '"]',
            }
        ]

        mock_urlopen.side_effect = HTTPError(
            "/", 500, "Fake Error", None, None
        )
        event = self.case_repo._event
        self.case_repo._create_subcases(event, None)

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.case_action_status_query"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.case_action_label_query"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.case_action_data_from_sub_case_query"
    )
    def test__generate_case_action_value_from_sub_case(
        self,
        mock_case_action_data_query,
        mock_case_action_label_query,
        mock_case_action_status_query,
    ):
        case_type_row = namedtuple("CaseTypeVersion", "uuid name")(
            uuid=uuid4(), name="casetype"
        )
        related_case_type_uuid = uuid4()
        sub_case = CaseRelatedToCaseType(
            **{
                "related_casetype_element": related_case_type_uuid,
                "start_on_transition": True,
                "pip_label": "test",
                "type_of_relation": "deelzaak",
                "copy_custom_fields_from_parent": True,
                "open_case_on_create": True,
                "show_in_pip": True,
            }
        )
        mock_case_action_label_query.return_value = "case_type 123"
        mock_case_action_status_query.return_value = 34

        res = self.case_repo._generate_case_action_value_from_sub_case(
            case_id=350,
            case_type_row=case_type_row,
            milestone=1,
            sub_case=sub_case,
        )

        assert res["case_id"] == 350

    @mock.patch.object(
        CaseRepository, "_get_custom_field_definiton_from_case_type"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.case_action_data_from_sub_case_query"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.case_action_label_query"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.case.case_action_status_query"
    )
    @mock.patch("zsnl_domains.database.schema.CaseAction.__table__.insert")
    def test__insert_case_actions(
        self,
        mock_action_insert,
        mock_case_action_query,
        mock_case_action_label,
        mock_subcase_data,
        mock__get_custom_field_definition,
    ):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses

        related_case_type_uuid = uuid4()
        case_uuid = uuid4()
        case_type_version_uuid = uuid4()

        case_type_phases = [
            CaseTypePhase(
                **{
                    "name": "Registration",
                    "milestone": 1,
                    "phase": "Registering",
                    "allocation": {
                        "department": {
                            "uuid": uuid4(),
                            "name": "front office",
                        },
                        "role": {"uuid": uuid4(), "name": "admin"},
                        "role_set": 1,
                    },
                    "cases": [
                        {
                            "related_casetype_element": related_case_type_uuid,
                            "type_of_relation": "deelzaak",
                            "copy_custom_fields_from_parent": False,
                            "open_case_on_create": False,
                            "start_on_transition": False,
                            "resolve_before_phase": 2,
                            "show_in_pip": False,
                            "label_in_pip": "test",
                            "requestor": {
                                "related_role": "Advocaat",
                                "requestor_type": "anders",
                            },
                            "allocation": {
                                "department": {
                                    "uuid": uuid4(),
                                    "name": "Frontdesk",
                                },
                                "role": {
                                    "uuid": uuid4(),
                                    "name": "Contactbeheerder",
                                },
                            },
                        }
                    ],
                    "documents": [
                        {
                            "automatic": None,
                            "label": "test_doc_create",
                            "data": {
                                "description": "Sjabloon",
                                "bibliotheek_sjablonen_id": 1,
                                "bibliotheek_kenmerken_id": None,
                                "filename": "file-sample_100kB.odt",
                                "target_format": "odt",
                                "automatisch_genereren": None,
                            },
                        }
                    ],
                    "emails": [
                        {
                            "automatic": False,
                            "label": "t",
                            "data": {
                                "description": "E-mail",
                                "rcpt": "aanvrager",
                                "subject": "t",
                                "body": "tdasd",
                                "sender_address": None,
                                "sender": None,
                                "cc": "",
                                "bcc": "",
                                "betrokkene_role": "Advocaat",
                                "email": "",
                                "behandelaar": "",
                                "intern_block": None,
                                "case_document_attachments": [],
                                "automatic_phase": None,
                                "zaaktype_notificatie_id": 2,
                                "bibliotheek_notificaties_id": 3,
                            },
                        }
                    ],
                    "subjects": [
                        {
                            "case_id": 3,
                            "casetype_status_id": "fake_query",
                            "type": "subject",
                            "label": "fake_label",
                            "automatic": 0,
                            "data": {
                                "fake_key": "fake_value",
                                "naam": "fake_label",
                                "betrokkene_type": "person",
                                "uuid": uuid4(),
                            },
                        }
                    ],
                }
            ),
            CaseTypePhase(
                **{
                    "name": "Afgehandeld",
                    "milestone": 2,
                    "phase": "Afhandelen",
                    "allocation": {
                        "department": {
                            "uuid": uuid4(),
                            "name": "front office",
                        },
                        "role": {"uuid": uuid4(), "name": "admin"},
                        "role_set": None,
                    },
                    "cases": [
                        {
                            "related_casetype_element": related_case_type_uuid,
                            "type_of_relation": "deelzaak",
                            "copy_custom_fields_from_parent": False,
                            "open_case_on_create": False,
                            "start_on_transition": False,
                            "resolve_before_phase": 2,
                            "show_in_pip": False,
                            "label_in_pip": "test",
                            "requestor": {
                                "related_role": "Advocaat",
                                "requestor_type": "anders",
                            },
                            "allocation": {
                                "department": {
                                    "uuid": uuid4(),
                                    "name": "Frontdesk",
                                },
                                "role": {
                                    "uuid": uuid4(),
                                    "name": "Contactbeheerder",
                                },
                            },
                        }
                    ],
                    "documents": [],
                }
            ),
        ]

        mock_case_action_query.return_value = "fake_query"
        mock_case_action_label.return_value = "fake_label"
        mock_subcase_data.return_value = "fake_data"
        self.case_repo.cache[case_uuid] = namedtuple("Case", "id uuid")(
            id=3, uuid=case_uuid
        )
        self.case_repo.cache[case_type_version_uuid] = namedtuple(
            "CaseTypeVesrionEntity", "uuid id name phases"
        )(
            uuid=case_type_version_uuid,
            id=7,
            name="case_type_1",
            phases=case_type_phases,
        )

        self.case_repo._insert_case_actions(
            case_uuid=case_uuid, case_type_version_uuid=case_type_version_uuid
        )
        custom_fields = {"att_checkbox": {"value": [["a", "b", "c"]]}}
        mock__get_custom_field_definition.return_value = (
            CustomFieldRelatedToCaseType(
                **{
                    "name": "text_field",
                    "uuid": uuid4(),
                    "field_magic_string": "att_checkbox",
                    "field_type": "checkbox",
                    "is_hidden_field": False,
                    "is_required": True,
                    "field_options": ["a", "b", "c", "d"],
                    "default_value": "test",
                }
            )
        )
        self.case_repo._insert_custom_fields(uuid4(), uuid4(), custom_fields)

    def test__get_custom_field_database_value(self):
        custom_field_definition = CustomFieldRelatedToCaseType(
            **{
                "name": "text_field",
                "uuid": uuid4(),
                "field_magic_string": "att_checkbox",
                "field_type": "checkbox",
                "is_hidden_field": False,
                "is_required": True,
                "field_options": ["a", "b", "c", "d"],
                "default_value": "test",
            }
        )
        res = self.case_repo._get_custom_field_database_value(
            custom_field_definition, [["a", "b"], ["b"]]
        )
        assert res == ["a", "b"]

        custom_field_definition = CustomFieldRelatedToCaseType(
            **{
                "name": "text_field",
                "uuid": uuid4(),
                "field_magic_string": "att_text",
                "field_type": "text",
                "is_hidden_field": False,
                "is_required": True,
                "field_options": [],
                "default_value": "test",
            }
        )
        res = self.case_repo._get_custom_field_database_value(
            custom_field_definition, []
        )
        assert res == ["test"]

        custom_field_definition = CustomFieldRelatedToCaseType(
            **{
                "name": "text_field",
                "uuid": uuid4(),
                "field_magic_string": "att_geolatlong",
                "field_type": "geolatlon",
                "is_hidden_field": False,
                "is_required": True,
                "field_options": [],
                "default_value": None,
            }
        )
        res = self.case_repo._get_custom_field_database_value(
            custom_field_definition,
            [
                {
                    "type": "FeatureCollection",
                    "features": [
                        {
                            "type": "Feature",
                            "geometry": {
                                "type": "Point",
                                "coordinates": [9.078888667, 60.543764527],
                            },
                        }
                    ],
                },
                {
                    "type": "FeatureCollection",
                    "features": [
                        {
                            "type": "Feature",
                            "geometry": {
                                "type": "Point",
                                "coordinates": [4.86667788, 50.25555557777],
                            },
                        }
                    ],
                },
            ],
        )
        assert res == ["60.543764527,9.078888667", "50.25555557777,4.86667788"]

        custom_field_definition = CustomFieldRelatedToCaseType(
            **{
                "name": "text_field",
                "uuid": uuid4(),
                "field_magic_string": "att_numeric",
                "field_type": "numeric",
                "is_hidden_field": False,
                "is_required": True,
                "field_options": [],
                "default_value": 1,
            }
        )
        res = self.case_repo._get_custom_field_database_value(
            custom_field_definition, [1, 2, 3]
        )
        assert res == [1, 2, 3]

    @mock.patch.object(CaseRepository, "_insert_checklist")
    def test__insert_checklist_items(self, mock_insert_checklist):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses
        case_uuid = uuid4()
        case_type_version_uuid = uuid4()

        case_type_phases = [
            CaseTypePhase(
                **{
                    "name": "Registration",
                    "milestone": 1,
                    "phase": "Registering",
                    "allocation": {
                        "department": {
                            "uuid": uuid4(),
                            "name": "front office",
                        },
                        "role": {"uuid": uuid4(), "name": "admin"},
                    },
                    "cases": [],
                    "documents": [],
                    "emails": [],
                    "subjects": [],
                    "checklist_items": ["item_1", "item_2"],
                }
            ),
            CaseTypePhase(
                **{
                    "name": "Handle",
                    "milestone": 2,
                    "phase": "Handling",
                    "allocation": {
                        "department": {
                            "uuid": uuid4(),
                            "name": "front office",
                        },
                        "role": {"uuid": uuid4(), "name": "admin"},
                    },
                    "cases": [],
                    "documents": [],
                    "emails": [],
                    "subjects": [],
                    "checklist_items": ["item_3", "item_4"],
                }
            ),
        ]

        self.case_repo.cache[case_type_version_uuid] = namedtuple(
            "CaseTypeVesrionEntity", "uuid id name phases"
        )(
            uuid=case_type_version_uuid,
            id=7,
            name="case_type_1",
            phases=case_type_phases,
        )

        insert_checklist_calls = [
            mock.call(case_uuid, 1),
            mock.call(case_uuid, 2),
        ]

        self.case_repo._insert_checklist_items(
            case_uuid, case_type_version_uuid
        )

        mock_insert_checklist.assert_has_calls(insert_checklist_calls)

    def test__insert_checklist(self):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses

        mock_insert = mock.MagicMock()
        mock_insert.inserted_primary_key = [1]
        mock_ses.execute.return_value = mock_insert

        res = self.case_repo._insert_checklist(
            case_uuid=uuid4(), status_milestone=1
        )
        assert res == 1

    @mock.patch("sqlalchemy.sql.insert", autospec=True)
    @mock.patch("sqlalchemy.sql.update", autospec=True)
    def test__update_contact_info_for_person(self, mock_update, mock_insert):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses

        mock_ses.execute().fetchone.return_value = namedtuple(
            "ResultProxy", "contact_data_id person_id"
        )(contact_data_id=1, person_id=3)
        self.case_repo._update_contact_info_for_person(
            uuid4(), contact_info_value={"mobiel": "1234567"}
        )
        mock_update.assert_called_once_with(schema.ContactData)

        mock_ses.execute().fetchone.return_value = namedtuple(
            "ResultProxy", "contact_data_id person_id"
        )(contact_data_id=None, person_id=3)
        self.case_repo._update_contact_info_for_person(
            uuid4(), contact_info_value={"mobiel": "1234567"}
        )
        mock_insert.assert_called_once_with(schema.ContactData)

        with pytest.raises(Conflict) as excinfo:
            person_uuid = uuid4()
            mock_ses.execute().fetchone.return_value = None
            self.case_repo._update_contact_info_for_person(
                person_uuid, contact_info_value={"mobiel": "1234567"}
            )
        assert excinfo.value.args == (
            f"Person with uuid '{person_uuid}' not found",
            "person_not_found",
        )

    @mock.patch("sqlalchemy.sql.insert", autospec=True)
    @mock.patch("sqlalchemy.sql.update", autospec=True)
    def test__update_contact_info_for_organization(
        self, mock_update, mock_insert
    ):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses

        mock_ses.execute().fetchone.return_value = namedtuple(
            "ResultProxy", "contact_data_id organization_id"
        )(contact_data_id=1, organization_id=3)
        self.case_repo._update_contact_info_for_organization(
            uuid4(), contact_info_value={"mobiel": "1234567"}
        )
        mock_update.assert_called_once_with(schema.ContactData)

        mock_ses.execute().fetchone.return_value = namedtuple(
            "ResultProxy", "contact_data_id organization_id"
        )(contact_data_id=None, organization_id=3)
        self.case_repo._update_contact_info_for_organization(
            uuid4(), contact_info_value={"mobiel": "1234567"}
        )
        mock_insert.assert_called_once_with(schema.ContactData)

        with pytest.raises(Conflict) as excinfo:
            organization_uuid = uuid4()
            mock_ses.execute().fetchone.return_value = None
            self.case_repo._update_contact_info_for_organization(
                organization_uuid, contact_info_value={"mobiel": "1234567"}
            )
        assert excinfo.value.args == (
            f"Organization with uuid '{organization_uuid}' not found",
            "organization_not_found",
        )

    @mock.patch.object(CaseRepository, "_update_contact_info_for_person")
    @mock.patch.object(CaseRepository, "_update_contact_info_for_organization")
    def test__update_contact_info_for_requestor(
        self, mock_update_organization_info, mock_update_person_info
    ):
        requestor_uuid = uuid4()
        contact_info = {
            "mobile_number": "619674353",
            "phone_number": "12345678",
            "email": "xx@xx.nl",
        }
        self.case_repo._update_contact_info_for_requestor(
            requestor_uuid, "organization", contact_info
        )
        mock_update_organization_info.assert_called_once_with(
            requestor_uuid,
            {
                "mobiel": "619674353",
                "telefoonnummer": "12345678",
                "email": "xx@xx.nl",
            },
        )

        self.case_repo._update_contact_info_for_requestor(
            requestor_uuid, "person", contact_info
        )
        mock_update_person_info.assert_called_once_with(
            requestor_uuid,
            {
                "mobiel": "619674353",
                "telefoonnummer": "12345678",
                "email": "xx@xx.nl",
            },
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.case_action_status_query"
    )
    def test__generate_case_action_for_allocation(
        self, mock_case_action_status_query
    ):
        mock_ses = mock.MagicMock()
        self.case_repo.session = mock_ses
        case_type_row = namedtuple("CaseType", "uuid")(uuid=uuid4())
        allocation = CaseTypeAllocation(
            **{
                "department": {
                    "type": "department",
                    "uuid": uuid4(),
                    "name": "dep",
                },
                "role": {"type": "role", "uuid": uuid4(), "name": "rol"},
                "role_set": 1,
            }
        )

        mock_ses.execute().fetchone.return_value = namedtuple(
            "ResultProxy", "department_id role_id"
        )(department_id=1, role_id=12)
        mock_case_action_status_query.return_value = 340

        res = self.case_repo._generate_case_action_for_allocation(
            1215, case_type_row, 2, allocation
        )

        assert res["type"] == "allocation"
        assert res["case_id"] == 1215
        assert res["casetype_status_id"] == 340
        assert res["automatic"] == bool(allocation.role_set)
        assert res["label"] == "1, 12"
        assert res["data"] == {
            "original_ou_id": 1,
            "ou_id": 1,
            "original_role_id": 12,
            "role_id": 12,
            "description": "Toewijzing",
            "role_set": 1,
        }

    def test__get_case_number_prefix_from_config(self):
        self.case_repo.session.execute().fetchone.return_value = ("ZS-2019",)
        res = self.case_repo._get_case_number_prefix_from_config()
        assert res == "ZS-2019"

        self.case_repo.session.execute().fetchone.return_value = None
        res = self.case_repo._get_case_number_prefix_from_config()
        assert res == ""

    @mock.patch("sqlalchemy.sql.update", autospec=True)
    @mock.patch("sqlalchemy.sql")
    def test__clear_assignee(self, mock_sql, mock_update):
        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.changes = [
            {
                "key": "assignee",
                "old_value": {"type": "subject", "entity_id": uuid4()},
                "new_value": None,
            }
        ]
        self.case_repo._event = event
        self.case_repo._clear_assignee(event, None)
        calls = [mock.call(schema.Case), mock.call(schema.ZaakBetrokkenen)]
        mock_update.assert_has_calls(calls, any_order=True)

    @mock.patch("sqlalchemy.sql.update", autospec=True)
    def test__update_allocation(self, mock_update):
        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.changes = [
            {
                "key": "department",
                "old_value": None,
                "new_value": {"type": "department", "entity_id": uuid4()},
            },
            {
                "key": "role",
                "old_value": None,
                "new_value": {"type": "role", "entity_id": uuid4()},
            },
        ]
        self.case_repo._event = event
        self.case_repo.cache[case_uuid] = Case(
            id=16,
            uuid=case_uuid,
            department=Department(
                uuid=uuid4(), name="Frontoffice", description="Front"
            ),
            role=Role(uuid=uuid4(), name="Administrator"),
            destruction_date=None,
            archival_state=None,
            status="new",
            created_date=datetime(2019, 3, 12),
            registration_date=datetime(2019, 3, 12),
            target_completion_date=datetime(2019, 6, 15),
            completion_date=None,
            stalled_until_date=None,
            milestone=1,
            suspension_reason=None,
            completion=None,
            stalled_since_date=None,
            last_modified_date=datetime(2019, 3, 30),
            assignee=None,
            coordinator=None,
            requestor=None,
            subject="Parking permit",
            subject_extern="Parking permit amsterdam",
            case_type_uuid=uuid4(),
            case_type_version_uuid=uuid4(),
            request_trigger="extern",
            contact_channel="post",
            resume_reason=None,
            summary="Perking permit Amsterdam",
            payment_amount=None,
            confidentiality="public",
            meta={},
            attributes={},
        )

        self.case_repo.session.execute().fetchone.return_value = [3]
        self.case_repo._update_allocation(event=event)

        mock_update.assert_called_once_with(schema.Case)

    @mock.patch("zsnl_domains.case_management.repositories.case.datetime")
    @mock.patch.object(CaseRepository, "_get_metadata_for_queue_item")
    @mock.patch("sqlalchemy.sql.insert", autospec=True)
    def test__enqueue_assignee_email(
        self, mock_insert, mock_get_meta_data_for_queue, mock_datetime
    ):
        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        queue_id = str(uuid4())
        event.changes = [
            {
                "key": "assignee_email_queue_id",
                "olde_value": None,
                "new_value": queue_id,
            }
        ]

        self.case_repo._event = event

        mock_get_meta_data_for_queue.return_value = "test"
        mock_datetime.now.return_value = "2015-06-12"

        self.case_repo._enqueue_assignee_email(event, None)
        mock_insert.assert_called_once_with(
            schema.Queue,
            values={
                "id": queue_id,
                "object_id": case_uuid,
                "status": "pending",
                "type": "send_case_assignee_email",
                "label": "Send email to case assignee",
                "data": {"case_uuid": str(case_uuid)},
                "date_created": "2015-06-12",
                "date_started": None,
                "date_finished": None,
                "parent_id": None,
                "priority": 1000,
                "metadata": "test",
            },
        )

    @mock.patch("zsnl_domains.case_management.repositories.case.urlopen")
    @mock.patch("zsnl_domains.case_management.repositories.case.ssl")
    @mock.patch("zsnl_domains.case_management.repositories.case.os")
    def test__send_assignee_email(self, mock_os, mock_ssl, mock_urlopen):
        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.context = "context"
        queue_id = str(uuid4())
        event.changes = [
            {
                "key": "queue_ids",
                "olde_value": None,
                "new_value": '["' + queue_id + '"]',
            }
        ]
        self.case_repo.context = "context"

        mock_os.envron = {"REQUESTS_CA_BUNDLE": True}
        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'
        self.case_repo._send_assignee_email(event, None)

    def test__update_status(self):
        case_uuid = uuid4()
        status_changed_event = events.Event(
            uuid=uuid4(),
            created_date="2019-04-19",
            correlation_id="req-12345",
            domain="case_management",
            context="localhost",
            user_uuid=uuid4(),
            entity_type="Case",
            entity_id=case_uuid,
            event_name="CasesStatusSet",
            changes=[
                {"key": "status", "old_value": "new", "new_value": "open"}
            ],
            entity_data={},
        )
        self.case_repo.session.execute().fetchone.return_value = [80]

        case_entity_mock = mock.MagicMock()
        case_entity_mock.status = "new"
        self.case_repo.cache[case_uuid] = case_entity_mock

        self.case_repo._update_status(status_changed_event)
        assert self.case_repo.cache[case_uuid].status == "open"


class TestCaseMessageRepository:
    @mock.patch("sqlalchemy.orm.Session")
    def setup(self, mock_session):
        infra = InfraFactoryMock(infra={"database": mock_session})
        self.cml_repo = CaseMessageListRepository(
            infrastructure_factory=infra,
            context=None,
            event_service=mock.MagicMock(),
        )

    def test_get_messages_for_case(self):
        message_list = self.cml_repo.get_messages_for_case(uuid4())

        assert isinstance(message_list, CaseMessageList)

    @mock.patch("sqlalchemy.sql.and_")
    @mock.patch("sqlalchemy.sql.select")
    def test_case_action_status_query(self, mock_select, mock_and_):
        case_action_status_query("fake_uuid", 42)
        mock_select.assert_called_once()
        mock_and_.assert_called_once()

    @mock.patch("sqlalchemy.sql.select")
    def test_case_action_label_query(self, mock_select):
        case_action_label_query("fake_uuid")
        mock_select.assert_called_once()

    @mock.patch("sqlalchemy.sql.and_")
    @mock.patch("sqlalchemy.sql.select")
    def test_case_action_data_from_sub_case_query(
        self, mock_select, mock_and_
    ):
        case_action_data_from_sub_case_query("fake_uuid", "fake_sub_uuid", 42)
        mock_select.assert_called_once()
        mock_and_.assert_called_once()


class TestSubjectRelationRepository:
    @mock.patch("sqlalchemy.orm.Session")
    def setup(self, mock_session):
        self.mock_session = mock_session
        infra = InfraFactoryMock(infra={"database": mock_session})
        self.subject_relation_repo = SubjectRelationRepository(
            infrastructure_factory=infra,
            context=None,
            event_service=mock.MagicMock(),
        )

    @mock.patch.object(SubjectRelationRepository, "_sqla_to_entity")
    def test_find_subject_relations_for_case(self, mock__sqla_to_entity):
        case_uuid = uuid4()
        user_uuid = uuid4()
        user_info = UserInfo(user_uuid=user_uuid, permissions={"admin": True})

        self.subject_relation_repo.session.execute().fetchall.return_value = [
            "test"
        ]

        self.subject_relation_repo.find_subject_relations_for_case(
            case_uuid, user_info
        )
        mock__sqla_to_entity.assert_called_once_with("test")
        (args, kwargs) = self.subject_relation_repo.session.execute.call_args
        query = args[0].compile()

        assert str(query) == (
            "SELECT zaak_betrokkenen.uuid AS uuid, zaak_betrokkenen.betrokkene_type AS subject_type, CASE WHEN (zaak_betrokkenen.betrokkene_type = :betrokkene_type_1) THEN CAST(subject.properties AS JSON) ->> :param_1 WHEN (zaak_betrokkenen.betrokkene_type = :betrokkene_type_2) THEN concat_ws(:concat_ws_1, natuurlijk_persoon.voorletters, natuurlijk_persoon.voorvoegsel, natuurlijk_persoon.geslachtsnaam) WHEN (zaak_betrokkenen.betrokkene_type = :betrokkene_type_3) THEN bedrijf.handelsnaam ELSE zaak_betrokkenen.naam END AS name, coalesce(zaak_betrokkenen.magic_string_prefix, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN :param_2 WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN :param_3 WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN :param_4 END) AS magic_string_prefix, CASE WHEN (zaak_betrokkenen.rol IS NOT NULL) THEN zaak_betrokkenen.pip_authorized WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN :param_5 ELSE :param_6 END AS pip_authorized, zaak.uuid AS case_uuid, zaak_betrokkenen.subject_id AS subject_uuid, zaak_betrokkenen.authorisation AS permission, coalesce(zaak_betrokkenen.rol, CASE WHEN (zaak_betrokkenen.id = zaak.aanvrager) THEN :param_7 WHEN (zaak_betrokkenen.id = zaak.behandelaar) THEN :param_8 WHEN (zaak_betrokkenen.id = zaak.coordinator) THEN :param_9 END) AS role, zaak.preset_client AS is_preset_client, bibliotheek_kenmerken.uuid AS source_custom_field_type_id \n"
            "FROM zaak JOIN zaak_betrokkenen ON zaak_betrokkenen.zaak_id = zaak.id AND zaak_betrokkenen.deleted IS NULL AND zaak.deleted IS NULL AND zaak.status != :status_1 LEFT OUTER JOIN bibliotheek_kenmerken ON zaak_betrokkenen.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id LEFT OUTER JOIN subject ON zaak_betrokkenen.betrokkene_type = :betrokkene_type_4 AND subject.subject_type = :subject_type_1 AND subject.uuid = zaak_betrokkenen.subject_id LEFT OUTER JOIN natuurlijk_persoon ON zaak_betrokkenen.betrokkene_type = :betrokkene_type_5 AND natuurlijk_persoon.uuid = zaak_betrokkenen.subject_id LEFT OUTER JOIN bedrijf ON zaak_betrokkenen.betrokkene_type = :betrokkene_type_6 AND bedrijf.uuid = zaak_betrokkenen.subject_id \n"
            "WHERE zaak.uuid = :uuid_1"
        )

    def test__sqla_to_entity(self):
        subject_relation_uuid = uuid4()
        case_uuid = uuid4()
        subject_uuid = uuid4()
        source_custom_field_type_id = uuid4()
        query_result = namedtuple(
            "ResultProxy",
            [
                "uuid",
                "subject_type",
                "subject_uuid",
                "role",
                "pip_authorized",
                "name",
                "magic_string_prefix",
                "permission",
                "case_uuid",
                "is_preset_client",
                "source_custom_field_type_id",
            ],
        )(
            uuid=subject_relation_uuid,
            subject_type="medewerker",
            subject_uuid=subject_uuid,
            role="Behandlaar",
            pip_authorized=True,
            name="beheerder",
            magic_string_prefix="behandler1",
            permission="write",
            case_uuid=case_uuid,
            is_preset_client=True,
            source_custom_field_type_id=source_custom_field_type_id,
        )

        subject_relation = self.subject_relation_repo._sqla_to_entity(
            query_result
        )

        assert isinstance(subject_relation, SubjectRelation)
        assert subject_relation.entity_id == subject_relation_uuid
        assert subject_relation.type == "subject_relation"
        assert subject_relation.role == "Behandlaar"
        assert subject_relation.magic_string_prefix == "behandler1"
        assert subject_relation.authorized is True
        assert subject_relation.permission == "write"
        assert subject_relation.subject == {
            "id": subject_uuid,
            "type": "employee",
            "name": "beheerder",
        }
        assert subject_relation.case == {"type": "case", "id": case_uuid}
        assert subject_relation.is_preset_client is True
        assert (
            subject_relation.source_custom_field_type_id
            == source_custom_field_type_id
        )

    def test_create_subject_relation(self):
        case_uuid = uuid4()
        subject_uuid = uuid4()
        case = namedtuple("Case", "uuid")(uuid=case_uuid)
        subject = namedtuple("Employee", "entity_type uuid name")(
            entity_type="employee", uuid=subject_uuid, name="beheerder"
        )

        res = self.subject_relation_repo.create_subject_relation(
            case=case,
            subject=subject,
            role="Advocaat",
            magic_string_prefix="advocaat",
            authorized=False,
            send_confirmation_email=False,
            permission="write",
            source_custom_field_type_id=None,
        )
        assert isinstance(res, SubjectRelation)
        assert res.role == "Advocaat"
        assert res.magic_string_prefix == "advocaat"
        assert res.case == {"id": case_uuid, "type": "case"}
        assert res.subject == {
            "type": "employee",
            "id": subject_uuid,
            "name": "beheerder",
        }
        assert res.permission == "write"
        assert res.source_custom_field_type_id is None

    @mock.patch.object(SubjectRelationRepository, "_get_subject_snapshot_id")
    @mock.patch.object(SubjectRelationRepository, "_get_subject_id")
    def test__generate_database_values(
        self, mock_get_subject_id, mock_get_subject_snapshot_id
    ):
        case_uuid = uuid4()
        subject_uuid = uuid4()
        mock_get_subject_id.return_value = 1
        mock_get_subject_snapshot_id.return_value = 13

        self.subject_relation_repo._event = mock.MagicMock()
        self.subject_relation_repo._event.entity_id = subject_uuid
        changes = {
            "role": "Advocaat",
            "subject": {
                "type": "employee",
                "id": subject_uuid,
                "name": "beheerder",
            },
            "case": {"type": "case", "id": case_uuid},
            "send_confirmation_email": False,
        }

        res = self.subject_relation_repo._generate_database_values(
            subject_uuid, changes
        )
        assert str(res["zaak_id"]) == str(
            sql.select([schema.Case.id])
            .where(schema.Case.uuid == case_uuid)
            .scalar_subquery()
        )
        assert res["rol"] == "Advocaat"
        assert res["naam"] == "beheerder"
        assert res["betrokkene_type"] == "medewerker"
        assert res["betrokkene_id"] == 13
        assert res["gegevens_magazijn_id"] == 1

    def test__get_subject_id(self):
        uuid = uuid4()

        res = self.subject_relation_repo._get_subject_id(
            subject_type="person", subject_uuid=uuid
        )
        assert str(res) == str(
            sql.select([schema.NatuurlijkPersoon.id])
            .where(schema.NatuurlijkPersoon.uuid == uuid)
            .scalar_subquery()
        )

        res = self.subject_relation_repo._get_subject_id(
            subject_type="organization", subject_uuid=uuid
        )
        assert str(res) == str(
            sql.select([schema.Bedrijf.id])
            .where(schema.Bedrijf.uuid == uuid)
            .scalar_subquery()
        )

        res = self.subject_relation_repo._get_subject_id(
            subject_type="employee", subject_uuid=uuid
        )
        assert str(res) == str(
            sql.select([schema.Subject.id])
            .where(
                sql.and_(
                    schema.Subject.uuid == uuid,
                    schema.Subject.subject_type == "employee",
                )
            )
            .scalar_subquery()
        )

    def test__sync_acl(self):
        self.subject_relation_repo._sync_acl(
            123, {"type": "employee", "id": str(uuid4())}, "read"
        )

        select_subject = self.mock_session.mock_calls[0]
        select_subject_fetch = self.mock_session.mock_calls[1]
        select_subject_fetch_check = self.mock_session.mock_calls[2]
        delete_old_acl = self.mock_session.mock_calls[3]
        insert_new_acl = self.mock_session.mock_calls[4]

        assert select_subject[0] == "execute"
        assert isinstance(select_subject[1][0], sql.expression.Select)

        assert select_subject_fetch[0] == "execute().fetchone"
        assert select_subject_fetch_check[0] == "execute().fetchone().__bool__"
        assert delete_old_acl[0] == "execute"
        assert isinstance(delete_old_acl[1][0], sql.expression.Delete)
        assert (
            str(delete_old_acl[1][0])
            == "DELETE FROM zaak_authorisation WHERE zaak_authorisation.zaak_id = :zaak_id_1 AND zaak_authorisation.entity_type = :entity_type_1 AND zaak_authorisation.entity_id = :entity_id_1"
        )
        assert insert_new_acl[0] == "execute"
        assert isinstance(insert_new_acl[1][0], sql.expression.Insert)

    def test__sync_acl_no_subject(self):
        self.mock_session.execute().fetchone.return_value = None
        self.mock_session.reset_mock()

        self.subject_relation_repo._sync_acl(
            123, {"type": "employee", "id": str(uuid4())}, "read"
        )

        assert len(self.mock_session.mock_calls) == 2

    @mock.patch.object(
        SubjectRelationRepository,
        "_create_and_return_snapshot_for_organization",
    )
    @mock.patch.object(
        SubjectRelationRepository, "_create_and_return_snapshot_for_person"
    )
    def test__get_subject_snapshot_id(
        self, mock_create_snapshot_for_person, mock_create_snpshot_organization
    ):
        uuid = uuid4()
        mock_create_snapshot_for_person.return_value = 1
        mock_create_snpshot_organization.return_value = 3

        res = self.subject_relation_repo._get_subject_snapshot_id(
            subject_type="person", subject_uuid=uuid
        )
        assert res == 1

        res = self.subject_relation_repo._get_subject_snapshot_id(
            subject_type="organization", subject_uuid=uuid
        )
        assert res == 3

        res = self.subject_relation_repo._get_subject_snapshot_id(
            subject_type="employee", subject_uuid=uuid
        )
        assert str(res) == str(
            sql.select([schema.Subject.id]).where(
                sql.and_(
                    schema.Subject.uuid == uuid,
                    schema.Subject.subject_type == "employee",
                )
            )
        )

    def test__assert_valid_role(self):
        event = mock.MagicMock()
        event.entity_id = uuid4()
        self.subject_relation_repo._event = event

        self.subject_relation_repo.session.execute().fetchone.return_value = [
            ["custom_role_1", "custom_role_2"]
        ]

        with pytest.raises(Conflict) as excinfo:
            self.subject_relation_repo._assert_valid_role(
                uuid=event.entity_id,
                role="random_role",
                subject_id=1,
                subject_name="beheerder",
                case_id=1,
            )
        assert excinfo.value.args == (
            "Role 'random_role' is not a valid role",
            "subject_relation/invalid_role",
        )

        with pytest.raises(Conflict) as excinfo:
            self.subject_relation_repo._assert_valid_role(
                uuid=event.entity_id,
                role="Advocaat",
                subject_id=1,
                subject_name="beheerder",
                case_id=1,
            )
        assert excinfo.value.args == (
            "Related subject with role 'Advocaat' already exists for 'beheerder'",
            "subject_relation/already_exists",
        )

    def test__assert_valid_magic_string_prefix(self):
        event = mock.MagicMock()
        event.entity_id = uuid4()
        self.subject_relation_repo._event = event

        self.subject_relation_repo.session.execute().fetchone.return_value = 1

        with pytest.raises(Conflict) as excinfo:
            self.subject_relation_repo._assert_valid_magic_string_prefix(
                uuid=event.entity_id,
                magic_string_prefix="advocaat1",
                case_id=1,
            )

        assert excinfo.value.args == (
            "Related subject with magic_string_prefix 'advocaat1' already exists",
            "subject_relation/invalid_magic_string",
        )

    def test_find_subject_relation_by_uuid(self):
        subject_relation_uuid = uuid4()
        subject_uuid = uuid4()
        case_uuid = uuid4()
        user_uuid = uuid4()
        user_info = UserInfo(user_uuid=user_uuid, permissions={"admin": True})
        self.subject_relation_repo.session.execute().fetchone.return_value = (
            namedtuple(
                "SubjectRelation",
                [
                    "uuid",
                    "subject_type",
                    "subject_uuid",
                    "name",
                    "role",
                    "magic_string_prefix",
                    "pip_authorized",
                    "permission",
                    "case_uuid",
                    "is_preset_client",
                    "source_custom_field_type_id",
                ],
            )(
                uuid=subject_relation_uuid,
                subject_type="medewerker",
                subject_uuid=subject_uuid,
                name="beheerder",
                role="Advocaat",
                magic_string_prefix="advocaat",
                pip_authorized=False,
                permission="write",
                case_uuid=case_uuid,
                is_preset_client=False,
                source_custom_field_type_id=None,
            )
        )
        res = self.subject_relation_repo.find_subject_relation_by_uuid(
            uuid=subject_relation_uuid, user_info=user_info
        )
        assert isinstance(res, SubjectRelation)
        assert res.uuid == subject_relation_uuid
        assert res.subject == {
            "type": "employee",
            "id": subject_uuid,
            "name": "beheerder",
        }
        assert res.authorized is False
        assert res.magic_string_prefix == "advocaat"
        assert res.role == "Advocaat"
        assert res.permission == "write"
        assert res.case == {"type": "case", "id": case_uuid}
        assert res.is_preset_client is False

        with pytest.raises(NotFound) as excinfo:
            self.subject_relation_repo.session.execute().fetchone.return_value = (
                None
            )
            self.subject_relation_repo.find_subject_relation_by_uuid(
                subject_relation_uuid, user_info=user_info
            )

        assert excinfo.value.args == (
            f"Subject relation with uuid '{subject_relation_uuid}' not found.",
            "subject_relation/not_found",
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case_acl.get_subject_by_uuid"
    )
    def test__get_metadata_for_queue_item(self, mock_get_subject_by_uuid):
        user_uuid = uuid4()
        mock_subject = mock.MagicMock()
        mock_subject.Subject.id = 2
        mock_get_subject_by_uuid.return_value = mock_subject

        res = self.subject_relation_repo._get_metadata_for_queue_item(
            user_uuid
        )
        assert res == {
            "require_object_model": 1,
            "subject_id": 2,
            "target": "backend",
        }

    @mock.patch.object(
        SubjectRelationRepository, "_get_metadata_for_queue_item"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.subject_relation.datetime"
    )
    @mock.patch("sqlalchemy.sql.insert")
    def test__enqueue_email(
        self, mock_insert, mock_datetime, mock_get_meta_data_for_queue
    ):
        subject_relation_uuid = uuid4()
        case_uuid = uuid4()
        subject_uuid = uuid4()
        mock_get_meta_data_for_queue.return_value = {"subject_id": 1}
        mock_datetime.now.return_value = 1

        self.subject_relation_repo._event = mock.MagicMock()
        self.subject_relation_repo._event.entity_id = subject_relation_uuid
        self.subject_relation_repo._event.changes = [
            {
                "key": "enqueued_email_data",
                "old_value": None,
                "new_value": {
                    "case_uuid": case_uuid,
                    "subject_type": "person",
                    "subject_uuid": subject_uuid,
                },
            }
        ]

        self.subject_relation_repo._enqueue_email(
            event=self.subject_relation_repo._event, userinfo=None
        )
        mock_insert.assert_called_once()

    @mock.patch(
        "zsnl_domains.case_management.repositories.subject_relation.urlopen"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.subject_relation.os"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.subject_relation.ssl"
    )
    def test_send_email(self, mock_ssl, mock_os, mock_urlopen):
        self.subject_relation_repo.event = mock.MagicMock()
        self.subject_relation_repo.event.event_name = (
            "SubjectRelationEmailSend"
        )
        self.subject_relation_repo.event.context = "dev"
        self.subject_relation_repo.event.entity_id = uuid4()

        self.subject_relation_repo.event.changes = {}
        mock_urlopen().read.return_value = '{"status_code": "200","result": {"data": {"object_id": "fake_object_id"}}}'
        mock_os.envron = {"REQUESTS_CA_BUNDLE": True}
        self.subject_relation_repo._send_email(
            event=self.subject_relation_repo.event, userinfo=None
        )

        mock_os.envron = {}
        self.subject_relation_repo._send_email(
            event=self.subject_relation_repo.event, userinfo=None
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.subject_relation.urlopen"
    )
    def test__send_email_http_error(self, mock_urlopen):
        self.subject_relation_repo._event = mock.MagicMock()
        self.subject_relation_repo._event.event_name = (
            "SubjectRelationEmailSend"
        )
        self.subject_relation_repo._event.context = "dev"
        self.subject_relation_repo._event.entity_id = uuid4()
        self.subject_relation_repo._event.changes = {}
        mock_urlopen.side_effect = HTTPError(
            "/", 500, "Fake Error", None, None
        )
        self.subject_relation_repo._send_email(
            self.subject_relation_repo._event, userinfo=None
        )

    def test__delete_subject_relation(
        self,
    ):
        subject_relation_uuid = str(uuid4())
        event = mock.MagicMock()
        event.entity_id = subject_relation_uuid
        subject_uuid = str(uuid4())
        case_uuid = str(uuid4())
        event.entity_data = {
            "subject": {
                "type": "organization",
                "id": subject_uuid,
                "name": "beheerder",
            },
            "case": {"type": "case", "id": case_uuid},
        }
        event.changes = {}
        self.subject_relation_repo._event = event
        self.subject_relation_repo._delete_subject_relation_by_uuid(
            event=event, userinfo=None
        )


class TestExportFileRepository:
    @mock.patch("sqlalchemy.orm.Session")
    def setup(self, mock_session):
        self.mock_session = mock_session
        infra = InfraFactoryMock(infra={"database": mock_session})
        self.export_file_repo = ExportFileRepository(
            infrastructure_factory=infra,
            context=None,
            event_service=mock.MagicMock(),
        )

    @mock.patch.object(ExportFileRepository, "_entity_from_row")
    @mock.patch(
        "zsnl_domains.shared.repositories.case_acl.allowed_cases_subquery"
    )
    def test_get_export_file_list(
        self, mock_acl_subquery, mock_entity_from_row
    ):
        page = 1
        page_size = 100
        user_uuid = uuid4()

        self.export_file_repo.session.execute().fetchall.return_value = [
            "test"
        ]

        self.export_file_repo.get_export_file_list(
            page=page, page_size=page_size, user_uuid=user_uuid
        )
        mock_entity_from_row.assert_called_once_with(row="test")
        (args, kwargs) = self.export_file_repo.session.execute.call_args
        query = args[0].compile()
        assert (
            str(query)
            == "SELECT export_queue.uuid AS uuid, export_queue.subject_uuid AS subject_uuid, export_queue.expires AS expires, export_queue.downloaded AS downloaded, export_queue.filestore_uuid AS filestore_uuid, filestore.original_name AS original_name \n"
            "FROM export_queue JOIN filestore ON export_queue.filestore_id = filestore.id \n"
            "WHERE export_queue.subject_uuid = :subject_uuid_1 AND export_queue.expires > :expires_1 ORDER BY export_queue.expires ASC\n"
            " LIMIT :param_1 OFFSET :param_2"
        )


class TestAs_Set_Parent(test.TestBase):
    def setup(self):
        self.load_command_instance(case_management)
        self.event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        self.mock_infra = mock.MagicMock()
        self.case_repo = CaseRepository(
            infrastructure_factory=self.mock_infra,
            context="devtest",
            event_service=self.event_service,
        )

        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True},
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_id_pid_by_uuid"
    )
    def test_set_case_parent_success(self, mock_get_case_id_pid_by_uuid):
        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.context = "context"
        parent_uuid = uuid4()
        event.changes = [
            {
                "key": "parent_uuid",
                "olde_value": None,
                "new_value": str(parent_uuid),
            }
        ]
        mock_get_case_id_pid_by_uuid.return_value = {"id": 3, "pid": None}
        self.case_repo._set_case_parent(event, self.cmd.user_info)
        (args, kwargs) = self.case_repo.session.execute.call_args
        query = args[0].compile()
        assert str(query) == (
            "UPDATE zaak SET pid=:pid WHERE zaak.uuid = :uuid_1"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository._get_case_id_pid_by_uuid"
    )
    def test_set_case_parent_not_allowed(self, mock_get_case_id_pid_by_uuid):
        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.context = "context"
        parent_uuid = uuid4()
        event.changes = [
            {
                "key": "parent_uuid",
                "olde_value": None,
                "new_value": str(parent_uuid),
            }
        ]
        mock_get_case_id_pid_by_uuid.return_value = {"id": 3, "pid": 1}
        with pytest.raises(Conflict) as excinfo:
            self.case_repo._set_case_parent(event, self.cmd.user_info)
        assert excinfo.value.args == (
            "Deze zaak kan niet worden gerelateerd aan '3'.",
        )

    def test_get_case_id_pid_raises_error(self):

        invalid_case_uuid = UUID("12345678-1234-1234-1234-123456789012")

        with pytest.raises(NotFound) as exception_info:
            self.case_repo.session.execute().fetchone.side_effect = [None]
            self.case_repo._get_case_id_pid_by_uuid(
                case_uuid=invalid_case_uuid,
                user_info=self.cmd.user_info,
                permission="write",
            )

        assert (
            exception_info.value.args[0]
            == f"Case with uuid '{invalid_case_uuid}' not found."
        )


class TestCountry:
    @mock.patch("sqlalchemy.orm.Session")
    def setup(self, mock_session):
        self.mock_session = mock_session
        infra = InfraFactoryMock(infra={"database": mock_session})
        self.country_repo = CountryRepository(
            infrastructure_factory=infra,
            context=None,
            event_service=mock.MagicMock(),
        )

    @mock.patch.object(CountryRepository, "_entity_from_row")
    def test_get_countries_list_query(self, mock_entity_from_row):

        self.country_repo.session.execute().fetchall.return_value = ["test"]

        self.country_repo.get_countries_list()
        mock_entity_from_row.assert_called_once_with(row="test")
        (args, kwargs) = self.country_repo.session.execute.call_args
        query = args[0].compile()
        assert (
            str(query)
            == "SELECT country_code.uuid AS uuid, country_code.label AS name, country_code.dutch_code AS code \n"
            "FROM country_code"
        )

    def test_get_countries_list(self):
        country_uuid = uuid4()
        self.country_repo.session.execute().fetchall.return_value = [
            namedtuple("Country", ["uuid", "name", "code"],)(
                uuid=str(country_uuid),
                name="country_name",
                code="6030",
            )
        ]

        res = self.country_repo.get_countries_list()
        assert isinstance(res[0], Country)
        assert res[0].uuid == country_uuid
        assert res[0].name == "country_name"
