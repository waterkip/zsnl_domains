# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import CaseSummary


class Test_AsUser_GetListOfCaseSummaryByUUID(TestBase):
    def setup(self):
        self.load_query_instance(case_management)

    def test_get_case_summaries_by_uuid(self):
        case_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_case_summary_rows = [mock.MagicMock()]
        mock_case_summary_rows[0].configure_mock(
            uuid=case_uuid,
            id=73,
            case_type_version={"name": "Parking permit", "uuid": uuid4()},
            progress_status=0.2,
            summary="Parking permit in Amsterdam",
            result="afgehandeld",
            result_description="Afgehandeld",
            result_uuid=uuid4(),
            archival_state="vernietigen",
            active_selection_list="12",
            assignee={"uuid": assignee_uuid, "display_name": "beheerder"},
            event_service=mock.MagicMock(),
        )

        self.session.execute().fetchall.return_value = mock_case_summary_rows
        self.session.execute.reset_mock()
        mock_case_summary_list = self.qry.get_case_summaries_by_uuid(
            case_uuids=[str(case_uuid)]
        )

        assert isinstance(mock_case_summary_list["result"][0], CaseSummary)
        assert mock_case_summary_list["result"][0].entity_id == case_uuid
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]

        assert isinstance(select_statement, sql.selectable.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT view_case_v2.uuid, view_case_v2.id, view_case_v2.case_type_version, view_case_v2.onderwerp AS summary, view_case_v2.result, view_case_v2.result_uuid, view_case_v2.result_description, view_case_v2.archival_state, view_case_v2.active_selection_list, view_case_v2.assignee_obj AS assignee, CAST(view_case_v2.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = view_case_v2.zaaktype_node_id) AS progress_status \n"
            "FROM view_case_v2 \n"
            "WHERE view_case_v2.uuid IN (%(uuid_1_1)s)"
        )
