# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from .... import DatabaseRepositoryBase
from .. import entities
from ..entities.case_type import CaseTypeEntity
from . import database_queries
from minty.exceptions import Conflict, NotFound
from sqlalchemy import sql
from sqlalchemy.orm.exc import NoResultFound
from uuid import UUID, uuid4
from zsnl_domains.database import schema


class CaseTypeRepository(DatabaseRepositoryBase):
    case_type_mapping = {
        "id": "id",
        "current_version": "version",
        "active": "active",
        "last_modified": "last_modified",
        "current_version_uuid": "current_version_uuid",
        "reason": None,
        "deleted": "deleted",
    }

    def get_related_case_types(self, case_type_id: int):
        """Get related case types for case_type entity.

        :param case_type_id: case_type id
        :type case_type_id: int
        :return: list of related case_types
        :rtype: list
        """
        used_in_case_types = self.session.execute(
            database_queries.case_types_related_to_case_type(case_type_id)
        ).fetchall()

        related_case_types = [
            entities.RelatedCaseType(
                uuid=ct.uuid,
                name=ct.name,
                active=ct.active,
                version=ct.version,
                is_current_version=ct.is_current_version,
            )
            for ct in used_in_case_types
        ]

        return related_case_types

    def get_case_type(self, uuid: UUID) -> entities.CaseTypeEntity:
        """Initialize case type entity from database.

        :param uuid: case type uuid
        :type uuid: UUID
        :raises NotFound: record not found
        :return: case type
        :rtype: entities.CaseTypeEntity
        """
        try:
            query_result = (
                self.session.query(
                    schema.Zaaktype,
                    schema.ZaaktypeNode.uuid.label("current_node_uuid"),
                    schema.ZaaktypeNode.code.label("current_node_code"),
                    schema.ZaaktypeNode.titel.label("current_node_title"),
                    schema.ZaaktypeNode.trigger.label(
                        "current_node_initiator_source"
                    ),
                    schema.Zaaktype.uuid,
                    schema.BibliotheekCategorie.uuid.label("folder_uuid"),
                    schema.BibliotheekCategorie.naam.label("folder_name"),
                )
                .join(
                    schema.BibliotheekCategorie,
                    schema.BibliotheekCategorie.id
                    == schema.Zaaktype.bibliotheek_categorie_id,
                    isouter=True,
                )
                .filter(schema.Zaaktype.uuid == uuid)
                .filter(schema.Zaaktype.id == schema.ObjectData.object_id)
                .filter(
                    schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id
                )
                .filter(schema.Zaaktype.deleted.is_(None))
            ).one()
        except NoResultFound as err:
            raise NotFound(
                f"Case type with uuid: '{uuid}' not found.",
                "case_type/not_found",
            ) from err
        return self._sqla_to_entity(query_result=query_result)

    def _sqla_to_entity(self, query_result) -> entities.CaseTypeEntity:
        """Initialize case type entity from sqla objects.

        :param query_result: sqla query results
        :type query_result: object
        :return: case_type entity
        :rtype: entities.CaseTypeEntity
        """
        case_type_row = query_result[0]
        current_node_uuid = query_result[1]
        current_node_code = query_result[2]
        current_node_title = query_result[3]
        current_node_initiator_source = query_result[4]
        object_data_uuid = query_result[5]
        folder_uuid = query_result[6]
        folder_name = query_result[7]

        folder = entities.Folder(
            id=None,
            uuid=folder_uuid,
            name=folder_name,
            parent_name=None,
            parent_uuid=None,
            last_modified=None,
        )

        context_mapping = {
            "intern": ["internal"],
            "extern": ["external"],
            "internextern": ["internal", "external"],
        }
        case_type = entities.CaseTypeEntity(
            id=case_type_row.id,
            uuid=object_data_uuid,
            name=current_node_title,
            active=case_type_row.active,
            identification=current_node_code,
            current_version=case_type_row.version,
            last_modified=case_type_row.last_modified,
            current_version_uuid=current_node_uuid,
            initiator_source=context_mapping[current_node_initiator_source],
            folder=folder,
        )

        case_type.event_service = self.event_service
        return case_type

    def delete_case_type(self, uuid: UUID, reason: str):
        case_type = CaseTypeEntity(
            id=None,
            uuid=uuid,
            name=None,
            active=None,
            identification=None,
            current_version=None,
            last_modified=None,
            current_version_uuid=None,
            initiator_source=None,
        )
        case_type.event_service = self.event_service
        case_type.delete(reason=reason)
        return case_type

    def save(self):
        """Save changes on case type entity back to database.

        :param case_type: case type
        :type case_type: entities.CaseTypeEntity
        """
        for event in self.event_service.event_list:
            if event.entity_type == "CaseTypeEntity":
                if event.event_name == "CaseTypeDeleted":
                    self._delete_case_type(event)
                elif event.event_name == "CaseTypeOnlineStatusChanged":
                    self._edit_case_type(event)
                elif event.event_name == "CaseTypeVersionUpdated":
                    self._update_case_type_version(event)
                else:
                    raise NotImplementedError(
                        f"Event {event.event_name} for CaseType is unknown and cannot be saved"
                    )

    def _edit_case_type(self, event):
        case_type_uuid = event.entity_id
        update_values = self._generate_database_values(event=event)
        update_stmt = (
            sql.update(schema.Zaaktype)
            .where(
                schema.Zaaktype.uuid == case_type_uuid,
            )
            .values(**update_values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_stmt)

    def create_new_version_from_existing_version(
        self, existing_version_uuid: UUID
    ):
        """Create new version for case_type from existing version"""

        existing_case_type_node_id = self.session.execute(
            sql.select([schema.ZaaktypeNode.id]).where(
                schema.ZaaktypeNode.uuid == existing_version_uuid
            )
        ).fetchone()[0]

        new_case_type_definition_id = (
            self._copy_case_type_definition_of_existing_version(
                existing_case_type_node_id
            )
        )

        (
            new_case_type_version_uuid,
            new_case_type_node_id,
            new_version,
        ) = self._copy_case_type_node_of_existing_version(
            existing_case_type_node_id, new_case_type_definition_id
        )

        self._copy_case_type_subjects_of_existing_version(
            existing_case_type_node_id, new_case_type_node_id
        )

        self._copy_case_type_statuses_of_existing_version(
            existing_case_type_node_id, new_case_type_node_id
        )

        return (new_case_type_version_uuid, new_version)

    def _update_case_type_version(self, event):
        """Update the new activated Case type version id in Case Type.

        :param new_value: New value from the event change
        """

        database_values = self._generate_database_values(event)
        new_version_uuid = database_values["current_version_uuid"]

        new_case_type_node = self.session.execute(
            sql.select(
                [
                    schema.ZaaktypeNode.zaaktype_id,
                    schema.ZaaktypeNode.id,
                    schema.ZaaktypeNode.version,
                ]
            ).where(schema.ZaaktypeNode.uuid == new_version_uuid)
        ).fetchone()

        case_type_version_update_stmt = (
            sql.update(schema.Zaaktype)
            .where(schema.Zaaktype.id == new_case_type_node.zaaktype_id)
            .values(
                zaaktype_node_id=new_case_type_node.id,
                version=new_case_type_node.version,
            )
            .execution_options(synchronize_session=False)
        )

        self.session.execute(case_type_version_update_stmt)

    def _copy_case_type_definition_of_existing_version(
        self, existing_case_type_node_id: int
    ):
        """
        Copy case_type_defnition of existing_version and
        create a new case_type_defnition one for new version
        """

        case_type_definition = self.session.execute(
            sql.select(
                schema.ZaaktypeDefinitie.openbaarheid,
                schema.ZaaktypeDefinitie.handelingsinitiator,
                schema.ZaaktypeDefinitie.grondslag,
                schema.ZaaktypeDefinitie.procesbeschrijving,
                schema.ZaaktypeDefinitie.afhandeltermijn,
                schema.ZaaktypeDefinitie.afhandeltermijn_type,
                schema.ZaaktypeDefinitie.selectielijst,
                schema.ZaaktypeDefinitie.servicenorm,
                schema.ZaaktypeDefinitie.servicenorm_type,
                schema.ZaaktypeDefinitie.pdc_voorwaarden,
                schema.ZaaktypeDefinitie.pdc_meenemen,
                schema.ZaaktypeDefinitie.pdc_description,
                schema.ZaaktypeDefinitie.pdc_tarief,
                schema.ZaaktypeDefinitie.omschrijving_upl,
                schema.ZaaktypeDefinitie.aard,
                schema.ZaaktypeDefinitie.preset_client,
                schema.ZaaktypeDefinitie.extra_informatie,
                schema.ZaaktypeDefinitie.extra_informatie_extern,
            ).where(
                sql.and_(
                    schema.ZaaktypeDefinitie.id
                    == schema.ZaaktypeNode.zaaktype_definitie_id,
                    schema.ZaaktypeNode.id == existing_case_type_node_id,
                )
            )
        ).fetchone()

        case_type_defintion_insert_values = {
            "openbaarheid": case_type_definition.openbaarheid,
            "handelingsinitiator": case_type_definition.handelingsinitiator,
            "grondslag": case_type_definition.grondslag,
            "procesbeschrijving": case_type_definition.procesbeschrijving,
            "afhandeltermijn": case_type_definition.afhandeltermijn,
            "afhandeltermijn_type": case_type_definition.afhandeltermijn_type,
            "selectielijst": case_type_definition.selectielijst,
            "servicenorm": case_type_definition.servicenorm,
            "servicenorm_type": case_type_definition.servicenorm_type,
            "pdc_voorwaarden": case_type_definition.pdc_voorwaarden,
            "pdc_meenemen": case_type_definition.pdc_meenemen,
            "pdc_description": case_type_definition.pdc_description,
            "pdc_tarief": case_type_definition.pdc_tarief,
            "omschrijving_upl": case_type_definition.omschrijving_upl,
            "aard": case_type_definition.aard,
            "preset_client": case_type_definition.preset_client,
            "extra_informatie": case_type_definition.extra_informatie,
            "extra_informatie_extern": case_type_definition.extra_informatie_extern,
        }

        return self.session.execute(
            sql.insert(schema.ZaaktypeDefinitie).values(
                case_type_defintion_insert_values
            )
        ).inserted_primary_key[0]

    def _copy_case_type_node_of_existing_version(
        self,
        existing_case_type_node_id: int,
        new_case_type_definition_id: int,
    ):
        """
        Duplicate a `zaaktype_node` row (case type version) to create a new
        version of a case type.
        """
        case_type_node = self.session.execute(
            sql.select(
                schema.ZaaktypeNode.zaaktype_id,
                schema.ZaaktypeNode.titel,
                schema.ZaaktypeNode.code,
                schema.ZaaktypeNode.trigger,
                schema.ZaaktypeNode.active,
                schema.ZaaktypeNode.properties,
                schema.ZaaktypeNode.is_public,
                schema.ZaaktypeNode.zaaktype_trefwoorden,
                schema.ZaaktypeNode.zaaktype_omschrijving,
                schema.ZaaktypeNode.webform_toegang,
                schema.ZaaktypeNode.webform_authenticatie,
                schema.ZaaktypeNode.adres_relatie,
                schema.ZaaktypeNode.aanvrager_hergebruik,
                schema.ZaaktypeNode.automatisch_behandelen,
                schema.ZaaktypeNode.toewijzing_zaakintake,
                schema.ZaaktypeNode.toelichting,
                schema.ZaaktypeNode.online_betaling,
                schema.ZaaktypeNode.adres_andere_locatie,
                schema.ZaaktypeNode.adres_aanvrager,
                schema.ZaaktypeNode.bedrijfid_wijzigen,
                schema.ZaaktypeNode.zaaktype_vertrouwelijk,
                schema.ZaaktypeNode.extra_relaties_in_aanvraag,
                schema.ZaaktypeNode.contact_info_intake,
                schema.ZaaktypeNode.prevent_pip,
                schema.ZaaktypeNode.contact_info_email_required,
                schema.ZaaktypeNode.contact_info_phone_required,
                schema.ZaaktypeNode.contact_info_mobile_phone_required,
                schema.ZaaktypeNode.moeder_zaaktype_id,
                schema.ZaaktypeNode.logging_id,
            ).where(schema.ZaaktypeNode.id == existing_case_type_node_id)
        ).fetchone()

        case_type_latest_version = self.session.execute(
            sql.select([schema.ZaaktypeNode.version])
            .where(
                schema.ZaaktypeNode.zaaktype_id == case_type_node.zaaktype_id
            )
            .order_by(schema.ZaaktypeNode.version.desc())
        ).fetchone()[0]

        new_version = case_type_latest_version + 1
        new_case_type_version_uuid = uuid4()

        case_type_node_insert_value = {
            "uuid": new_case_type_version_uuid,
            "zaaktype_id": case_type_node.zaaktype_id,
            "created": datetime.datetime.now(datetime.timezone.utc),
            "last_modified": datetime.datetime.now(datetime.timezone.utc),
            "zaaktype_definitie_id": new_case_type_definition_id,
            "titel": case_type_node.titel,
            "code": case_type_node.code,
            "trigger": case_type_node.trigger,
            "version": new_version,
            "active": case_type_node.active,
            "properties": case_type_node.properties,
            "is_public": case_type_node.is_public,
            "zaaktype_trefwoorden": case_type_node.zaaktype_trefwoorden,
            "zaaktype_omschrijving": case_type_node.zaaktype_omschrijving,
            "webform_toegang": case_type_node.webform_toegang,
            "webform_authenticatie": case_type_node.webform_authenticatie,
            "adres_relatie": case_type_node.adres_relatie,
            "aanvrager_hergebruik": case_type_node.aanvrager_hergebruik,
            "automatisch_behandelen": case_type_node.automatisch_behandelen,
            "toewijzing_zaakintake": case_type_node.toewijzing_zaakintake,
            "toelichting": case_type_node.toelichting,
            "online_betaling": case_type_node.online_betaling,
            "adres_andere_locatie": case_type_node.adres_andere_locatie,
            "adres_aanvrager": case_type_node.adres_aanvrager,
            "bedrijfid_wijzigen": case_type_node.bedrijfid_wijzigen,
            "zaaktype_vertrouwelijk": case_type_node.zaaktype_vertrouwelijk,
            "extra_relaties_in_aanvraag": case_type_node.extra_relaties_in_aanvraag,
            "contact_info_intake": case_type_node.contact_info_intake,
            "prevent_pip": case_type_node.prevent_pip,
            "contact_info_email_required": case_type_node.contact_info_email_required,
            "contact_info_phone_required": case_type_node.contact_info_phone_required,
            "contact_info_mobile_phone_required": case_type_node.contact_info_mobile_phone_required,
            "moeder_zaaktype_id": case_type_node.moeder_zaaktype_id,
            "logging_id": case_type_node.logging_id,
        }

        new_casetype_node_id = self.session.execute(
            sql.insert(schema.ZaaktypeNode).values(case_type_node_insert_value)
        ).inserted_primary_key[0]

        return (
            new_case_type_version_uuid,
            new_casetype_node_id,
            new_version,
        )

    def _copy_case_type_subjects_of_existing_version(
        self, existing_case_type_node_id: int, new_case_type_node_id: int
    ):
        """
        Copy zaaktype_betrokkenen of existing_version and
        create new zaaktype_betrokkenen for new version
        """
        case_type_subjects = self.session.execute(
            sql.select(schema.ZaaktypeBetrokkenen.betrokkene_type).where(
                schema.ZaaktypeBetrokkenen.zaaktype_node_id
                == existing_case_type_node_id
            )
        ).fetchall()

        for case_type_subject in case_type_subjects:
            case_type_subject_insert_values = {
                "zaaktype_node_id": new_case_type_node_id,
                "betrokkene_type": case_type_subject.betrokkene_type,
                "created": datetime.datetime.now(datetime.timezone.utc),
                "last_modified": datetime.datetime.now(datetime.timezone.utc),
            }

            self.session.execute(
                sql.insert(schema.ZaaktypeBetrokkenen).values(
                    case_type_subject_insert_values
                )
            )

    def _copy_case_type_statuses_of_existing_version(
        self, existing_case_type_node_id: int, new_case_type_node_id: int
    ):
        """
        Copy case_type_statuses for existing version and
        create new case_type_statuses(along with it contents) for new version
        """
        case_type_statuses = self.session.execute(
            sql.select(
                schema.ZaaktypeStatus.id,
                schema.ZaaktypeStatus.status,
                schema.ZaaktypeStatus.status_type,
                schema.ZaaktypeStatus.naam,
                schema.ZaaktypeStatus.fase,
                schema.ZaaktypeStatus.ou_id,
                schema.ZaaktypeStatus.role_id,
                schema.ZaaktypeStatus.checklist,
                schema.ZaaktypeStatus.role_set,
            ).where(
                sql.and_(
                    schema.ZaaktypeStatus.zaaktype_node_id
                    == existing_case_type_node_id,
                )
            )
        ).fetchall()

        for case_type_status in case_type_statuses:
            case_type_status_insert_values = {
                "created": datetime.datetime.now(datetime.timezone.utc),
                "last_modified": datetime.datetime.now(datetime.timezone.utc),
                "zaaktype_node_id": new_case_type_node_id,
                "status": case_type_status.status,
                "status_type": case_type_status.status_type,
                "naam": case_type_status.naam,
                "fase": case_type_status.fase,
                "ou_id": case_type_status.ou_id,
                "role_id": case_type_status.role_id,
                "checklist": case_type_status.checklist,
                "role_set": case_type_status.role_set,
            }

            new_case_type_status_id = self.session.execute(
                sql.insert(schema.ZaaktypeStatus).values(
                    case_type_status_insert_values
                )
            ).inserted_primary_key[0]

            # copying case_type_attributes for status
            self._copy_case_type_attributes_of_case_type_status(
                existing_case_type_node_id,
                case_type_status.id,
                new_case_type_node_id,
                new_case_type_status_id,
            )
            # copying case_type_attributes for status
            self._copy_case_type_notifications_of_case_type_status(
                existing_case_type_node_id,
                case_type_status.id,
                new_case_type_node_id,
                new_case_type_status_id,
            )
            # copying case_type_results for status
            self._copy_case_type_results_of_case_type_status(
                existing_case_type_node_id,
                case_type_status.id,
                new_case_type_node_id,
                new_case_type_status_id,
            )
            # copying case_type_rules for status
            self._copy_case_type_rules_of_case_type_status(
                existing_case_type_node_id,
                case_type_status.id,
                new_case_type_node_id,
                new_case_type_status_id,
            )
            # copying case_type_relations for status
            self._copy_case_type_relations_of_case_type_status(
                existing_case_type_node_id,
                case_type_status.id,
                new_case_type_node_id,
                new_case_type_status_id,
            )
            # copying case_type_templates for status
            self._copy_case_type_templates_of_case_type_status(
                existing_case_type_node_id,
                case_type_status.id,
                new_case_type_node_id,
                new_case_type_status_id,
            )
            # copying case_type_subjects for status
            self._copy_case_type_subjects_of_case_type_status(
                existing_case_type_node_id,
                case_type_status.id,
                new_case_type_node_id,
                new_case_type_status_id,
            )

    def _copy_case_type_attributes_of_case_type_status(
        self,
        existing_case_type_node_id: int,
        existing_case_type_status_id: int,
        new_case_type_node_id: int,
        new_case_type_status_id: int,
    ):
        """
        Copy the case_type_attributes from a phase/status of existing version
        and create new case_type_attributes for phase/status of new version.
        """
        case_type_attributes = self.session.execute(
            sql.select(
                schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id,
                schema.ZaaktypeKenmerk.object_id,
                schema.ZaaktypeKenmerk.value_mandatory,
                schema.ZaaktypeKenmerk.label,
                schema.ZaaktypeKenmerk.help,
                schema.ZaaktypeKenmerk.pip,
                schema.ZaaktypeKenmerk.zaakinformatie_view,
                schema.ZaaktypeKenmerk.bag_zaakadres,
                schema.ZaaktypeKenmerk.value_default,
                schema.ZaaktypeKenmerk.pip_can_change,
                schema.ZaaktypeKenmerk.publish_public,
                schema.ZaaktypeKenmerk.referential,
                schema.ZaaktypeKenmerk.is_systeemkenmerk,
                schema.ZaaktypeKenmerk.required_permissions,
                schema.ZaaktypeKenmerk.version,
                schema.ZaaktypeKenmerk.help_extern,
                schema.ZaaktypeKenmerk.object_metadata,
                schema.ZaaktypeKenmerk.label_multiple,
                schema.ZaaktypeKenmerk.properties,
                schema.ZaaktypeKenmerk.is_group,
            )
            .where(
                sql.and_(
                    schema.ZaaktypeKenmerk.zaaktype_node_id
                    == existing_case_type_node_id,
                    schema.ZaaktypeKenmerk.zaak_status_id
                    == existing_case_type_status_id,
                )
            )
            .order_by(schema.ZaaktypeKenmerk.id)
        ).fetchall()

        for case_type_attribute in case_type_attributes:
            case_type_attribute_insert_values = {
                "uuid": uuid4(),
                "created": datetime.datetime.now(datetime.timezone.utc),
                "last_modified": datetime.datetime.now(datetime.timezone.utc),
                "zaaktype_node_id": new_case_type_node_id,
                "zaak_status_id": new_case_type_status_id,
                "bibliotheek_kenmerken_id": case_type_attribute.bibliotheek_kenmerken_id,
                "object_id": case_type_attribute.object_id,
                "value_mandatory": case_type_attribute.value_mandatory,
                "label": case_type_attribute.label,
                "help": case_type_attribute.help,
                "pip": case_type_attribute.pip,
                "zaakinformatie_view": case_type_attribute.zaakinformatie_view,
                "bag_zaakadres": case_type_attribute.bag_zaakadres,
                "value_default": case_type_attribute.value_default,
                "pip_can_change": case_type_attribute.pip_can_change,
                "publish_public": case_type_attribute.publish_public,
                "referential": case_type_attribute.referential,
                "is_systeemkenmerk": case_type_attribute.is_systeemkenmerk,
                "required_permissions": case_type_attribute.required_permissions,
                "version": case_type_attribute.version,
                "help_extern": case_type_attribute.help_extern,
                "object_metadata": case_type_attribute.object_metadata,
                "label_multiple": case_type_attribute.label_multiple,
                "properties": case_type_attribute.properties,
                "is_group": case_type_attribute.is_group,
            }

            self.session.execute(
                sql.insert(schema.ZaaktypeKenmerk).values(
                    case_type_attribute_insert_values
                )
            )

    def _copy_case_type_notifications_of_case_type_status(
        self,
        existing_case_type_node_id: int,
        existing_case_type_status_id: int,
        new_case_type_node_id: int,
        new_case_type_status_id: int,
    ):
        """
        Copy notifications from a phase/status of existing version
        and create a new one for phase/status of new version
        """
        case_type_notifications = self.session.execute(
            sql.select(
                schema.ZaaktypeNotificatie.bibliotheek_notificatie_id,
                schema.ZaaktypeNotificatie.label,
                schema.ZaaktypeNotificatie.rcpt,
                schema.ZaaktypeNotificatie.onderwerp,
                schema.ZaaktypeNotificatie.bericht,
                schema.ZaaktypeNotificatie.intern_block,
                schema.ZaaktypeNotificatie.email,
                schema.ZaaktypeNotificatie.behandelaar,
                schema.ZaaktypeNotificatie.automatic,
                schema.ZaaktypeNotificatie.cc,
                schema.ZaaktypeNotificatie.bcc,
                schema.ZaaktypeNotificatie.betrokkene_role,
            ).where(
                sql.and_(
                    schema.ZaaktypeNotificatie.zaaktype_node_id
                    == existing_case_type_node_id,
                    schema.ZaaktypeNotificatie.zaak_status_id
                    == existing_case_type_status_id,
                )
            )
        ).fetchall()

        for case_type_notification in case_type_notifications:
            case_type_notification_insert_values = {
                "created": datetime.datetime.now(datetime.timezone.utc),
                "last_modified": datetime.datetime.now(datetime.timezone.utc),
                "zaaktype_node_id": new_case_type_node_id,
                "zaak_status_id": new_case_type_status_id,
                "bibliotheek_notificaties_id": case_type_notification.bibliotheek_notificatie_id,
                "label": case_type_notification.label,
                "rcpt": case_type_notification.rcpt,
                "onderwerp": case_type_notification.onderwerp,
                "bericht": case_type_notification.bericht,
                "intern_block": case_type_notification.intern_block,
                "email": case_type_notification.email,
                "behandelaar": case_type_notification.behandelaar,
                "automatic": case_type_notification.automatic,
                "cc": case_type_notification.cc,
                "bcc": case_type_notification.bcc,
                "betrokkene_role": case_type_notification.betrokkene_role,
            }

            self.session.execute(
                sql.insert(schema.ZaaktypeNotificatie).values(
                    case_type_notification_insert_values
                )
            )

    def _copy_case_type_templates_of_case_type_status(
        self,
        current_case_type_node_id: int,
        existing_case_type_status_id: int,
        new_case_type_node_id: int,
        new_case_type_status_id: int,
    ):
        """
        Copy case_type_templates from a phase/status of existing version
        and create new one for phase/status of new version
        """
        case_type_templates = self.session.execute(
            sql.select(
                schema.ZaaktypeSjabloon.bibliotheek_sjablonen_id,
                schema.ZaaktypeSjabloon.bibliotheek_kenmerken_id,
                schema.ZaaktypeSjabloon.automatisch_genereren,
                schema.ZaaktypeSjabloon.target_format,
            ).where(
                sql.and_(
                    schema.ZaaktypeSjabloon.zaaktype_node_id
                    == current_case_type_node_id,
                    schema.ZaaktypeSjabloon.zaak_status_id
                    == existing_case_type_status_id,
                )
            )
        ).fetchall()

        for case_type_template in case_type_templates:
            case_type_template_insert_values = {
                "created": datetime.datetime.now(datetime.timezone.utc),
                "last_modified": datetime.datetime.now(datetime.timezone.utc),
                "zaaktype_node_id": new_case_type_node_id,
                "zaak_status_id": new_case_type_status_id,
                "bibliotheek_sjablonen_id": case_type_template.bibliotheek_sjablonen_id,
                "bibliotheek_kenmerken_id": case_type_template.bibliotheek_kenmerken_id,
                "automatisch_genereren": case_type_template.automatisch_genereren,
                "target_format": case_type_template.target_format,
            }

            self.session.execute(
                sql.insert(schema.ZaaktypeSjabloon).values(
                    case_type_template_insert_values
                )
            )

    def _copy_case_type_results_of_case_type_status(
        self,
        existing_case_type_node_id: int,
        existing_case_type_status_id: int,
        new_case_type_node_id: int,
        new_case_type_status_id: int,
    ):
        """
        Copy case_type_results from a phase/status of existing version
        and create new one for phase/status of new version
        """
        case_type_results = self.session.execute(
            sql.select(
                schema.ZaaktypeResultaten.resultaat,
                schema.ZaaktypeResultaten.ingang,
                schema.ZaaktypeResultaten.bewaartermijn,
                schema.ZaaktypeResultaten.dossiertype,
                schema.ZaaktypeResultaten.label,
                schema.ZaaktypeResultaten.selectielijst,
                schema.ZaaktypeResultaten.archiefnominatie,
                schema.ZaaktypeResultaten.comments,
                schema.ZaaktypeResultaten.external_reference,
                schema.ZaaktypeResultaten.trigger_archival,
                schema.ZaaktypeResultaten.selectielijst_brondatum,
                schema.ZaaktypeResultaten.selectielijst_einddatum,
                schema.ZaaktypeResultaten.properties,
                schema.ZaaktypeResultaten.standaard_keuze,
            ).where(
                sql.and_(
                    schema.ZaaktypeResultaten.zaaktype_node_id
                    == existing_case_type_node_id,
                    schema.ZaaktypeResultaten.zaaktype_status_id
                    == existing_case_type_status_id,
                )
            )
        ).fetchall()

        for case_type_result in case_type_results:
            case_type_result_insert_values = {
                "zaaktype_node_id": new_case_type_node_id,
                "zaaktype_status_id": new_case_type_status_id,
                "resultaat": case_type_result.resultaat,
                "ingang": case_type_result.ingang,
                "bewaartermijn": case_type_result.bewaartermijn,
                "created": datetime.datetime.now(datetime.timezone.utc),
                "last_modified": datetime.datetime.now(datetime.timezone.utc),
                "dossiertype": case_type_result.dossiertype,
                "label": case_type_result.label,
                "selectielijst": case_type_result.selectielijst,
                "archiefnominatie": case_type_result.archiefnominatie,
                "comments": case_type_result.comments,
                "external_reference": case_type_result.external_reference,
                "trigger_archival": case_type_result.trigger_archival,
                "selectielijst_brondatum": case_type_result.selectielijst_brondatum,
                "selectielijst_einddatum": case_type_result.selectielijst_einddatum,
                "properties": case_type_result.properties,
                "standaard_keuze": case_type_result.standaard_keuze,
            }

            self.session.execute(
                sql.insert(schema.ZaaktypeResultaten).values(
                    case_type_result_insert_values
                )
            )

    def _copy_case_type_rules_of_case_type_status(
        self,
        existing_case_type_node_id: int,
        existing_case_type_status_id: int,
        new_case_type_node_id: int,
        new_case_type_status_id: int,
    ):
        """
        Copy case_type_rules from phase/status of existing version
        and create a new one for phase/status of new version
        """
        case_type_rules = self.session.execute(
            sql.select(
                schema.ZaaktypeRegel.naam,
                schema.ZaaktypeRegel.settings,
                schema.ZaaktypeRegel.active,
                schema.ZaaktypeRegel.is_group,
            ).where(
                sql.and_(
                    schema.ZaaktypeRegel.zaaktype_node_id
                    == existing_case_type_node_id,
                    schema.ZaaktypeRegel.zaak_status_id
                    == existing_case_type_status_id,
                )
            )
        ).fetchall()

        for case_type_rule in case_type_rules:
            case_type_rule_insert_values = {
                "zaaktype_node_id": new_case_type_node_id,
                "zaak_status_id": new_case_type_status_id,
                "naam": case_type_rule.naam,
                "created": datetime.datetime.now(datetime.timezone.utc),
                "last_modified": datetime.datetime.now(datetime.timezone.utc),
                "settings": case_type_rule.settings,
                "active": case_type_rule.active,
                "is_group": case_type_rule.is_group,
            }

            self.session.execute(
                sql.insert(schema.ZaaktypeRegel).values(
                    case_type_rule_insert_values
                )
            )

    def _copy_case_type_relations_of_case_type_status(
        self,
        existing_case_type_node_id: int,
        existing_case_type_status_id: int,
        new_case_type_node_id: int,
        new_case_type_status_id: int,
    ):
        """
        Copy case_type_relations from phase/status of existing version
        and create a new one for phase/status of new version
        """
        case_type_relations = self.session.execute(
            sql.select(
                schema.ZaaktypeRelatie.relatie_zaaktype_id,
                schema.ZaaktypeRelatie.relatie_type,
                schema.ZaaktypeRelatie.start_delay,
                schema.ZaaktypeRelatie.status,
                schema.ZaaktypeRelatie.kopieren_kenmerken,
                schema.ZaaktypeRelatie.ou_id,
                schema.ZaaktypeRelatie.role_id,
                schema.ZaaktypeRelatie.automatisch_behandelen,
                schema.ZaaktypeRelatie.required,
                schema.ZaaktypeRelatie.subject_role,
                schema.ZaaktypeRelatie.copy_subject_role,
                schema.ZaaktypeRelatie.betrokkene_authorized,
                schema.ZaaktypeRelatie.betrokkene_notify,
                schema.ZaaktypeRelatie.betrokkene_id,
                schema.ZaaktypeRelatie.betrokkene_role,
                schema.ZaaktypeRelatie.betrokkene_role_set,
                schema.ZaaktypeRelatie.betrokkene_prefix,
                schema.ZaaktypeRelatie.eigenaar_type,
                schema.ZaaktypeRelatie.eigenaar_role,
                schema.ZaaktypeRelatie.eigenaar_id,
                schema.ZaaktypeRelatie.show_in_pip,
                schema.ZaaktypeRelatie.pip_label,
            ).where(
                sql.and_(
                    schema.ZaaktypeRelatie.zaaktype_node_id
                    == existing_case_type_node_id,
                    schema.ZaaktypeRelatie.zaaktype_status_id
                    == existing_case_type_status_id,
                )
            )
        ).fetchall()

        for case_type_relation in case_type_relations:
            case_type_relation_insert_values = {
                "created": datetime.datetime.now(datetime.timezone.utc),
                "last_modified": datetime.datetime.now(datetime.timezone.utc),
                "zaaktype_node_id": new_case_type_node_id,
                "zaaktype_status_id": new_case_type_status_id,
                "relatie_zaaktype_id": case_type_relation.relatie_zaaktype_id,
                "relatie_type": case_type_relation.relatie_type,
                "start_delay": case_type_relation.start_delay,
                "status": case_type_relation.status,
                "kopieren_kenmerken": case_type_relation.kopieren_kenmerken,
                "ou_id": case_type_relation.ou_id,
                "role_id": case_type_relation.role_id,
                "automatisch_behandelen": case_type_relation.automatisch_behandelen,
                "required": case_type_relation.required,
                "subject_role": case_type_relation.subject_role,
                "copy_subject_role": case_type_relation.copy_subject_role,
                "betrokkene_authorized": case_type_relation.betrokkene_authorized,
                "betrokkene_notify": case_type_relation.betrokkene_notify,
                "betrokkene_id": case_type_relation.betrokkene_id,
                "betrokkene_role": case_type_relation.betrokkene_role,
                "betrokkene_role_set": case_type_relation.betrokkene_role_set,
                "betrokkene_prefix": case_type_relation.betrokkene_prefix,
                "eigenaar_type": case_type_relation.eigenaar_type,
                "eigenaar_role": case_type_relation.eigenaar_role,
                "eigenaar_id": case_type_relation.eigenaar_id,
                "show_in_pip": case_type_relation.show_in_pip,
                "pip_label": case_type_relation.pip_label,
            }
            self.session.execute(
                sql.insert(schema.ZaaktypeRelatie).values(
                    case_type_relation_insert_values
                )
            )

    def _copy_case_type_subjects_of_case_type_status(
        self,
        existing_case_type_node_id: int,
        existing_case_type_status_id: int,
        new_case_type_node_id: int,
        new_case_type_status_id: int,
    ):
        """
        Copy case_type_standard_betrokkene from a phase/status of existing_version
        and create a new one for phase/status of new version
        """
        case_type_standard_subjects = self.session.execute(
            sql.select(
                schema.ZaaktypeStandaardBetrokkenen.betrokkene_type,
                schema.ZaaktypeStandaardBetrokkenen.betrokkene_identifier,
                schema.ZaaktypeStandaardBetrokkenen.naam,
                schema.ZaaktypeStandaardBetrokkenen.rol,
                schema.ZaaktypeStandaardBetrokkenen.magic_string_prefix,
                schema.ZaaktypeStandaardBetrokkenen.gemachtigd,
                schema.ZaaktypeStandaardBetrokkenen.notify,
            ).where(
                sql.and_(
                    schema.ZaaktypeStandaardBetrokkenen.zaaktype_node_id
                    == existing_case_type_node_id,
                    schema.ZaaktypeStandaardBetrokkenen.zaak_status_id
                    == existing_case_type_status_id,
                )
            )
        ).fetchall()

        for case_type_standard_subject in case_type_standard_subjects:
            case_type_standard_subject_insert_values = {
                "uuid": uuid4(),
                "zaaktype_node_id": new_case_type_node_id,
                "zaak_status_id": new_case_type_status_id,
                "betrokkene_type": case_type_standard_subject.betrokkene_type,
                "betrokkene_identifier": case_type_standard_subject.betrokkene_identifier,
                "naam": case_type_standard_subject.naam,
                "rol": case_type_standard_subject.rol,
                "magic_string_prefix": case_type_standard_subject.magic_string_prefix,
                "gemachtigd": case_type_standard_subject.gemachtigd,
                "notify": case_type_standard_subject.notify,
            }
            self.session.execute(
                sql.insert(schema.ZaaktypeStandaardBetrokkenen).values(
                    case_type_standard_subject_insert_values
                )
            )

    def _delete_case_type(self, event):
        uuid = event.entity_id

        if self._get_case_types_related_to_case_type(uuid=event.entity_id):
            raise Conflict(
                "Case type is used in case types",
                "case_type/used_in_case_types",
            )
        if self._get_cases_related_to_case_type(uuid=event.entity_id):
            raise Conflict(
                "Case type is used in cases", "case_type/used_in_cases"
            )

        delete_values = self._generate_database_values(event=event)
        delete_values["bibliotheek_categorie_id"] = None
        delete_stmt = (
            sql.update(schema.Zaaktype)
            .where(schema.Zaaktype.uuid == uuid)
            .values(**delete_values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(delete_stmt)

    def _generate_database_values(self, event):
        values = {}
        for change in event.changes:
            db_fieldname = self.case_type_mapping.get(change["key"])
            if db_fieldname is None:
                pass
            else:
                values[db_fieldname] = change["new_value"]
        return values

    def _get_case_types_related_to_case_type(self, uuid: UUID):
        case_type_id = (
            sql.select([schema.Zaaktype.id])
            .where(schema.Zaaktype.uuid == uuid)
            .scalar_subquery()
        )

        zaaktype_joined = sql.join(
            schema.ZaaktypeRelatie,
            schema.ZaaktypeNode,
            schema.ZaaktypeRelatie.zaaktype_node_id == schema.ZaaktypeNode.id,
        ).join(
            schema.Zaaktype,
            schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
        )

        case_types_stmt = (
            sql.select([sql.func.count().label("cnt")])
            .select_from(zaaktype_joined)
            .where(
                sql.and_(
                    schema.Zaaktype.deleted.is_(None),
                    schema.ZaaktypeRelatie.relatie_zaaktype_id == case_type_id,
                )
            )
        )

        related_case_types = self.session.execute(case_types_stmt).fetchone()
        return related_case_types.cnt

    def _get_cases_related_to_case_type(self, uuid: UUID):
        cases_stmt = sql.select([sql.func.count().label("cnt")]).where(
            sql.and_(
                schema.Zaaktype.uuid == uuid,
                schema.Case.zaaktype_id == schema.Zaaktype.id,
                schema.Case.deleted.is_(None),
            )
        )
        related_cases = self.session.execute(cases_stmt).fetchone()
        return related_cases.cnt
