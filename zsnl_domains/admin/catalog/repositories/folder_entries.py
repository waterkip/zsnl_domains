# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import DatabaseRepositoryBase
from ..entities import FolderEntry
from . import database_queries
from minty.exceptions import NotFound
from sqlalchemy import sql
from typing import List
from uuid import UUID
from zsnl_domains.database import schema


class FolderEntriesRepository(DatabaseRepositoryBase):
    @property
    def folder_entry_statements(self):
        """Return class `FolderEntryStatements`."""
        try:
            return self.entry_statements
        except AttributeError:
            self.entry_statements = FolderEntryStatements()
            return self.entry_statements

    @property
    def folder_entry_update_statements(self):
        """Return class `FolderEntryUpdateStatements`."""
        try:
            return self.entry_update_statements
        except AttributeError:
            self.entry_update_statements = FolderEntryUpdateStatements()
            return self.entry_update_statements

    def get_folder_entries(self, folder_entries: List) -> List[FolderEntry]:
        """Get list of `FolderEntry` for given list of id & type.

        :param folder_entries: list of `id` and `type`
        :type folder_entries: list
        :return: list with FolderEntry's
        :rtype: List[FolderEntry]
        """
        entities = []

        for fe in folder_entries:
            folder_entry = self.get_folder_entry(
                uuid=fe["id"], type=fe["type"]
            )
            entities.append(folder_entry)

        return entities

    def get_folder_entry(self, uuid: UUID, type: str) -> FolderEntry:
        """Get `FolderEntry` for given  uuid & type.

        :param uuid: object uuid
        :type uuid: UUID
        :param type: type of object
        :type type: str
        :raises NotFound: when object not found for given `uuid`
        :return: folder entry
        :rtype: FolderEntry
        """
        q_statement = self.folder_entry_statements.select(
            folder_entry_type=type
        )
        query_result = self.session.execute(q_statement(uuid=uuid))
        query_result = query_result.fetchone()
        if not query_result:
            raise NotFound(
                f"Folder entry of type: '{type}'"
                + f" with uuid: '{uuid}' not found.",
                "folder_entry/not_found",
            )
        return self._transform_to_entity(query_result=query_result)

    def save(self, folder_entry):
        """Save changes on folder_entity type entity back to database.

        :param folder_entry: folder entry
        :type folder_entry: Folder
        """
        update_stmt = self.folder_entry_update_statements.select(
            folder_entry_type=folder_entry.entry_type
        )
        self.session.execute(
            update_stmt(
                uuid=folder_entry.uuid, folder_id=folder_entry.folder_id
            )
        )

    def save_list(self, folder_entries: List[FolderEntry]):
        """save list of folder entries.

        :param folder_entries: list of folder entries
        :type folder_entries: List[FolderEntry]
        """
        for entry in folder_entries:
            self.save(folder_entry=entry)

    def _transform_to_entity(self, query_result):
        """Initialize case type entity from sqla objects.

        :param query_result: sqla query results
        :type query_result: object
        :return: case_type entity
        :rtype: entities.CaseTypeEntity
        """
        folder_entry = FolderEntry(
            id=query_result.object_id,
            entry_type=query_result.type,
            uuid=query_result.uuid,
            name=query_result.name,
            active=query_result.active,
            folder_id=query_result.parent_folder_id,
        )
        folder_entry.event_service = self.event_service
        return folder_entry


class FolderEntryStatements:
    def select(self, folder_entry_type: str):
        """Return function to create query statement for given `folder_entry_type`.

        :param folder_entry_type: type of folder entry
        :type folder_entry_type: str
        :return: function to create statement
        :rtype: function
        """
        queries = {
            "folder": self._folder_statement,
            "case_type": self._case_type_statement,
            "attribute": self._attribute_statement,
            "email_template": self._email_template_statement,
            "document_template": self._document_template_statement,
            "object_type": self._object_type_statement,
            "custom_object_type": self._custom_object_type_statement,
        }

        return queries[folder_entry_type]

    def _folder_statement(self, uuid: UUID):
        return database_queries.folder_query.where(
            schema.BibliotheekCategorie.uuid == uuid
        )

    def _case_type_statement(self, uuid: UUID):
        return database_queries.case_type_query.where(
            schema.Zaaktype.uuid == uuid
        )

    def _attribute_statement(self, uuid: UUID):
        return database_queries.attribute_query.where(
            schema.BibliotheekKenmerk.uuid == uuid
        )

    def _email_template_statement(self, uuid: UUID):
        return database_queries.email_template_query.where(
            schema.BibliotheekNotificatie.uuid == uuid
        )

    def _document_template_statement(self, uuid: UUID):
        return database_queries.document_template_query.where(
            schema.BibliotheekSjabloon.uuid == uuid
        )

    def _object_type_statement(self, uuid: UUID):
        return database_queries.object_type_query.where(
            schema.ObjectBibliotheekEntry.object_uuid == uuid
        )

    def _custom_object_type_statement(self, uuid: UUID):
        return database_queries.custom_object_type_query.where(
            schema.CustomObjectType.uuid == uuid
        )


class FolderEntryUpdateStatements:
    def select(self, folder_entry_type: str):
        """Return function to create update statement for given `folder_entry_type`.

        :param folder_entry_type: type of folder entry
        :type folder_entry_type: str
        :return: function to create statement
        :rtype: function
        """
        update_statements = {
            "folder": self._folder_update_statement,
            "case_type": self._case_type_update_statement,
            "attribute": self._attribute_update_statement,
            "email_template": self._email_template_update_statement,
            "document_template": self._document_template_update_statement,
            "object_type": self._object_type_update_statement,
            "custom_object_type": self._custom_object_type_update_statement,
        }

        return update_statements[folder_entry_type]

    def _folder_update_statement(self, uuid: UUID, folder_id: int):
        return (
            sql.update(schema.BibliotheekCategorie)
            .where(sql.and_(schema.BibliotheekCategorie.uuid == uuid))
            .values(pid=folder_id)
            .execution_options(synchronize_session=False)
        )

    def _case_type_update_statement(self, uuid: UUID, folder_id: int):
        return (
            sql.update(schema.Zaaktype)
            .where(
                sql.and_(
                    schema.Zaaktype.deleted.is_(None),
                    schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id,
                    schema.Zaaktype.uuid == uuid,
                )
            )
            .values(bibliotheek_categorie_id=folder_id)
            .execution_options(synchronize_session=False)
        )

    def _attribute_update_statement(self, uuid: UUID, folder_id: int):
        return (
            sql.update(schema.BibliotheekKenmerk)
            .where(
                sql.and_(
                    schema.BibliotheekKenmerk.deleted.is_(None),
                    schema.BibliotheekKenmerk.uuid == uuid,
                )
            )
            .values(bibliotheek_categorie_id=folder_id)
            .execution_options(synchronize_session=False)
        )

    def _email_template_update_statement(self, uuid: UUID, folder_id: int):
        return (
            sql.update(schema.BibliotheekNotificatie)
            .where(
                sql.and_(
                    schema.BibliotheekNotificatie.deleted.is_(None),
                    schema.BibliotheekNotificatie.uuid == uuid,
                )
            )
            .values(bibliotheek_categorie_id=folder_id)
            .execution_options(synchronize_session=False)
        )

    def _document_template_update_statement(self, uuid: UUID, folder_id: int):
        return (
            sql.update(schema.BibliotheekSjabloon)
            .where(
                sql.and_(
                    schema.BibliotheekSjabloon.deleted.is_(None),
                    schema.BibliotheekSjabloon.uuid == uuid,
                )
            )
            .values(bibliotheek_categorie_id=folder_id)
            .execution_options(synchronize_session=False)
        )

    def _object_type_update_statement(self, uuid: UUID, folder_id: int):
        return (
            sql.update(schema.ObjectBibliotheekEntry)
            .where(schema.ObjectBibliotheekEntry.object_uuid == uuid)
            .values(bibliotheek_categorie_id=folder_id)
            .execution_options(synchronize_session=False)
        )

    def _custom_object_type_update_statement(self, uuid: UUID, folder_id: int):
        return (
            sql.update(schema.CustomObjectType)
            .where(schema.CustomObjectType.uuid == uuid)
            .values(catalog_folder_id=folder_id)
            .execution_options(synchronize_session=False)
        )
