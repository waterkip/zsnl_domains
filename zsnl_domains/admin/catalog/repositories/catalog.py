# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import DatabaseRepositoryBase
from .. import entities
from . import database_queries
from minty.exceptions import NotFound
from sqlalchemy.sql import alias, join, select
from uuid import UUID
from zsnl_domains.database import schema


def _inflate_folder_entry(entry_row):
    """Create a FolderEntry instance from a database row as returned by the
    folder content queries"""
    return entities.FolderEntry(
        entry_type=entry_row.type,
        uuid=entry_row.uuid,
        name=entry_row.name,
        active=entry_row.active,
    )


class Catalog(DatabaseRepositoryBase):
    def _expect_one(self, result, what):
        if len(result) != 1:
            self.logger.error(f"Expected 1 {what}, got {len(result)}")
            raise KeyError

        return result[0]

    def get_folder_contents(
        self, folder_uuid: UUID = None, page: int = None, page_size: int = None
    ):
        """Retrieve catalog folder contents, given a folder's UUID.

        If the folder_uuid is None, the root folder contents will be retrieved.
        :rtype: list[FolderEntry]
        """

        db = self._get_infrastructure("database")

        folder_id = None
        if folder_uuid:
            try:
                folder = self._expect_one(
                    db.execute(
                        select([schema.BibliotheekCategorie.id]).where(
                            schema.BibliotheekCategorie.uuid == folder_uuid
                        )
                    ).fetchall(),
                    "folder",
                )
            except KeyError:
                raise NotFound(
                    "Catalog item not found", "admin/catalog/not_found"
                )

            folder_id = folder.id
            self.logger.info(
                f"Found folder id '{folder_id}' for folder UUID '{folder_uuid}''"
            )

        return [
            _inflate_folder_entry(entry_row)
            for entry_row in db.execute(
                database_queries.get_folder_contents_query(
                    folder_id, page, page_size
                )
            ).fetchall()
        ]

    def search_catalog(
        self,
        keyword: str = None,
        type: str = None,
        page: int = None,
        page_size: int = None,
    ):

        """Retrieve catalog contents, given a search keyword.

        If the keyword is None return all result.

        :return: A result set of FolderEntry objects
        :rtype: list[FolderEntry]
        """

        db = self._get_infrastructure("database")

        if keyword:
            keyword = keyword.lower()
            self.logger.info(
                f"Found folder entries of keyword '{keyword}' in catalog"
            )

        return [
            _inflate_folder_entry(entry_row)
            for entry_row in db.execute(
                database_queries.search_catalog_query(
                    keyword=keyword, type=type, page=page, page_size=page_size
                )
            ).fetchall()
        ]

    def get_folder_details(self, uuid):
        """Retrieve details on the specified folder

        If the uuid is None, some details about the root folder will be
        returned.

        :param uuid: UUID of the folder to retrieve details for
        :type uuid: UUID
        :raises KeyError: If the specified folder can't be found
        :return: Dictionary with folder details
        :rtype: dict
        """

        if uuid is None:
            return entities.Folder(
                id=None,
                name="Catalog",
                uuid=None,
                parent_name=None,
                parent_uuid=None,
                last_modified=None,
            )

        db = self._get_infrastructure("database")

        parent_cat = alias(schema.BibliotheekCategorie)
        folder_detail_query = select(
            [
                schema.BibliotheekCategorie.uuid,
                schema.BibliotheekCategorie.naam.label("name"),
                schema.BibliotheekCategorie.last_modified,
                parent_cat.c.uuid.label("parent_uuid"),
                parent_cat.c.naam.label("parent_name"),
            ]
        ).select_from(
            join(
                schema.BibliotheekCategorie,
                parent_cat,
                schema.BibliotheekCategorie.pid == parent_cat.c.id,
                isouter=True,
            )
        )

        try:
            folder_detail = self._expect_one(
                db.execute(
                    folder_detail_query.where(
                        schema.BibliotheekCategorie.uuid == uuid
                    )
                ).fetchall(),
                "folder",
            )
        except KeyError:
            raise NotFound("Catalog item not found", "admin/catalog/not_found")

        return self._transform_to_entity(folder_detail)

    def _transform_to_entity(self, folder_row):
        return entities.Folder(
            id=None,
            uuid=folder_row.uuid,
            name=folder_row.name,
            parent_name=folder_row.parent_name,
            parent_uuid=folder_row.parent_uuid,
            last_modified=folder_row.last_modified,
        )

    def _thin_transform_to_entity(self, folder_uuid, folder_name):
        return entities.Folder(
            id=None,
            uuid=folder_uuid,
            name=folder_name,
            parent_name=None,
            parent_uuid=None,
            last_modified=None,
        )

    def get_custom_object_type_details(self, uuid):
        """Retrieve details on the specified object type

        :param uuid: UUID of the object type to retrieve details for
        :type uuid: UUID
        :raises KeyError: If the specified object type can't be found
        :return: Dictionary with object type details
        :rtype: dict
        """

        db = self._get_infrastructure("database")

        custom_object_type_query = (
            database_queries.custom_object_type_detail.where(
                schema.CustomObjectType.uuid == uuid
            )
        )

        try:
            custom_object_type_detail = self._expect_one(
                db.execute(custom_object_type_query).fetchall(), "object type"
            )
        except KeyError:
            raise NotFound("Catalog item not found", "admin/catalog/not_found")

        folder = self._thin_transform_to_entity(
            folder_uuid=custom_object_type_detail.folder_uuid,
            folder_name=custom_object_type_detail.folder_name,
        )

        return entities.CustomObjectTypeDetail(
            name=custom_object_type_detail.name,
            uuid=custom_object_type_detail.uuid,
            title=custom_object_type_detail.title,
            version=custom_object_type_detail.version,
            date_created=custom_object_type_detail.date_created,
            last_modified=custom_object_type_detail.last_modified,
            status=custom_object_type_detail.status,
            external_reference=custom_object_type_detail.external_reference,
            version_independent_uuid=custom_object_type_detail.version_independent_uuid,
            folder=folder,
        )

    def get_object_type_details(self, uuid):
        """Retrieve details on the specified object type

        :param uuid: UUID of the object type to retrieve details for
        :type uuid: UUID
        :raises KeyError: If the specified object type can't be found
        :return: Dictionary with object type details
        :rtype: dict
        """

        db = self._get_infrastructure("database")

        object_type_query = database_queries.object_type_detail.where(
            schema.ObjectData.uuid == uuid
        )

        try:
            object_type_detail = self._expect_one(
                db.execute(object_type_query).fetchall(), "object type"
            )
        except KeyError:
            raise NotFound("Catalog item not found", "admin/catalog/not_found")

        folder = self._thin_transform_to_entity(
            folder_uuid=object_type_detail.folder_uuid,
            folder_name=object_type_detail.folder_name,
        )

        return entities.ObjectTypeDetail(
            name=object_type_detail.name,
            uuid=object_type_detail.uuid,
            used_in_case_types=[
                # object_relationships
            ],
            used_in_object_types=[
                # _inflate_attribute_related_object_type(ct)
                # for ct in attribute_related_object_types
            ],
            folder=folder,
        )

    def get_attribute_details(self, uuid):
        """Retrieve details on the specified attribute

        :param uuid: UUID of the attribute to retrieve details for
        :type uuid: UUID
        :raises KeyError: If the specified attribute can't be found
        :return: Dictionary with attribute details
        :rtype: dict
        """

        db = self._get_infrastructure("database")

        attribute_query = database_queries.attribute_detail_query.where(
            schema.BibliotheekKenmerk.uuid == uuid
        )

        try:
            attribute_detail = self._expect_one(
                db.execute(attribute_query).fetchall(), "attribute"
            )
        except KeyError:
            raise NotFound("Catalog item not found", "admin/catalog/not_found")

        used_in_case_types = db.execute(
            database_queries.case_types_related_to_attribute(
                attribute_detail.id
            )
        ).fetchall()

        folder = self._thin_transform_to_entity(
            folder_uuid=attribute_detail.folder_uuid,
            folder_name=attribute_detail.folder_name,
        )

        return entities.AttributeDetail(
            name=attribute_detail.name,
            uuid=attribute_detail.uuid,
            magic_string=attribute_detail.magic_string,
            value_type=attribute_detail.value_type,
            is_multiple=attribute_detail.type_multiple,
            last_modified=attribute_detail.last_modified,
            used_in_case_types=[
                entities.RelatedCaseType(
                    uuid=ct.uuid,
                    name=ct.name,
                    active=ct.active,
                    version=ct.version,
                    is_current_version=ct.is_current_version,
                )
                for ct in used_in_case_types
            ],
            used_in_object_types=[
                # _inflate_attribute_related_object_type(ct)
                # for ct in attribute_related_object_types
            ],
            folder=folder,
        )

    def get_email_template_details(self, uuid):
        """Retrieve details of the specified email template

        :param uuid: UUID of the email template
        :type uuid: UUID
        """
        db = self._get_infrastructure("database")

        email_template_query = (
            database_queries.email_template_detail_query.where(
                schema.BibliotheekNotificatie.uuid == uuid
            )
        )

        try:
            email_detail = self._expect_one(
                db.execute(email_template_query).fetchall(),
                "email template",
            )
        except KeyError:
            raise NotFound("Catalog item not found", "admin/catalog/not_found")

        used_in_case_types = db.execute(
            database_queries.case_types_related_to_email_template(
                email_detail.id
            )
        ).fetchall()

        folder = self._thin_transform_to_entity(
            folder_uuid=email_detail.folder_uuid,
            folder_name=email_detail.folder_name,
        )

        return entities.EmailTemplateDetail(
            name=email_detail.name,
            uuid=email_detail.uuid,
            last_modified=email_detail.last_modified,
            used_in_case_types=[
                entities.RelatedCaseType(
                    uuid=ct.uuid,
                    name=ct.name,
                    active=ct.active,
                    version=ct.version,
                    is_current_version=ct.is_current_version,
                )
                for ct in used_in_case_types
            ],
            folder=folder,
        )

    def get_document_template_details(self, uuid):
        """Retrieve details of the specified document template

        :param uuid: UUID of the document template
        :type uuid: UUID
        """
        db = self._get_infrastructure("database")

        document_template_query = (
            database_queries.document_template_detail_query.where(
                schema.BibliotheekSjabloon.uuid == uuid
            )
        )

        try:
            document_detail = self._expect_one(
                db.execute(document_template_query).fetchall(),
                "document template",
            )
        except KeyError:
            raise NotFound("Catalog item not found", "admin/catalog/not_found")

        used_in_case_types = db.execute(
            database_queries.case_types_related_to_document_template(
                document_detail.id
            )
        ).fetchall()

        folder = self._thin_transform_to_entity(
            folder_uuid=document_detail.folder_uuid,
            folder_name=document_detail.folder_name,
        )

        return entities.DocumentTemplateDetail(
            name=document_detail.name,
            uuid=document_detail.uuid,
            filename=document_detail.original_name,
            has_default_integration=(document_detail.interface_id is None),
            last_modified=document_detail.last_modified,
            used_in_case_types=[
                entities.RelatedCaseType(
                    uuid=ct.uuid,
                    name=ct.name,
                    active=ct.active,
                    version=ct.version,
                    is_current_version=ct.is_current_version,
                )
                for ct in used_in_case_types
            ],
            folder=folder,
        )
