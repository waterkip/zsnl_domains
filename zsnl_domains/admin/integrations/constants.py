# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

APPOINTMENT_INTEGRATION_TYPES = {"appointment"}
APPOINTMENT_V2_INTEGRATION_TYPES = {"koppelapp_appointments"}
DOCUMENT_INTEGRATION_TYPES = {"stuf_dcr", "xential"}
