# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .queries import Queries
from .repositories import integration

REQUIRED_REPOSITORIES = {"integration": integration.IntegrationRepository}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )
