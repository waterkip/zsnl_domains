# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import EntityBase
from typing import Union
from uuid import UUID


class Case(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        id: int,
        uuid: UUID,
        description: Union[str, None],
        description_public: Union[str, None],
        case_type_name: Union[str, None],
        status: str,
    ):
        self.id = id
        self.uuid = uuid
        self.description = description
        self.description_public = description_public
        self.case_type_name = case_type_name
        self.status = status
