# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .base import Base
from .communication_module import (  # noqa: F401
    Thread,
    ThreadMessage,
    ThreadMessageAttachment,
    ThreadMessageAttachmentDerivative,
    ThreadMessageContactmoment,
    ThreadMessageExternal,
    ThreadMessageNote,
)
from .objects import (  # noqa: F401
    CustomObject,
    CustomObjectRelationship,
    CustomObjectType,
    CustomObjectTypeVersion,
    CustomObjectVersion,
    CustomObjectVersionContent,
)
from .types import GUID, JSONEncodedDict, UTCDate, UTCDateTime
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import relationship
from sqlalchemy.schema import (
    Column,
    FetchedValue,
    ForeignKey,
    PrimaryKeyConstraint,
    UniqueConstraint,
)
from sqlalchemy.types import (
    JSON,
    BigInteger,
    Boolean,
    Date,
    Enum,
    Integer,
    Numeric,
    SmallInteger,
    String,
    Text,
)

# TODO Split schema, automate filling of __all__ (remove the F401 above)


class Queue(Base):
    """Legacy background task queue"""

    __tablename__ = "queue"

    id = Column(GUID, primary_key=True)
    object_id = Column(GUID)
    status = Column(
        Enum("pending", "running", "finished", "failed", "waiting"),
        nullable=False,
    )
    type = Column(String, nullable=False)
    label = Column(String, nullable=False)
    data = Column(JSON, nullable=False)
    date_created = Column(UTCDateTime, nullable=False)
    date_started = Column(UTCDateTime)
    date_finished = Column(UTCDateTime)
    parent_id = Column(GUID, ForeignKey("queue.id"))
    priority = Column(Integer, nullable=False)
    _metadata = Column("metadata", JSON, nullable=False)


class CaseMeta(Base):
    """Case-specific metadata

    This includes reasons for stalling and resumption, and the "stalled-since"
    date.
    """

    __tablename__ = "zaak_meta"

    id = Column(Integer, primary_key=True)
    zaak_id = Column(Integer, ForeignKey("zaak.id"))
    verlenging = Column(String(255))
    opschorten = Column(String(255))
    deel = Column(String(255))
    gerelateerd = Column(String(255))
    vervolg = Column(String(255))
    afhandeling = Column(String(255))
    stalled_since = Column(UTCDate)
    current_deadline = Column(postgresql.JSONB, default={}, nullable=False)
    deadline_timeline = Column(postgresql.JSONB, default="[]", nullable=False)
    last_modified = Column(UTCDateTime, nullable=False)
    unread_communication_count = Column(Integer, default=0, nullable=False)
    unaccepted_attribute_update_count = Column(
        Integer, default=0, nullable=False
    )
    unaccepted_files_count = Column(Integer, default=0, nullable=False)
    pending_changes = Column(postgresql.JSONB, default="{}", nullable=False)
    text_vector = Column(postgresql.TSVECTOR)


class Case(Base):
    """Main "case" storage"""

    __tablename__ = "zaak"

    meta = relationship("CaseMeta")
    id = Column(Integer, primary_key=True)
    pid = Column(Integer, ForeignKey("zaak.id"))
    uuid = Column(GUID, nullable=False)

    coordinator = Column(Integer)
    behandelaar = Column(Integer)
    aanvrager = Column(Integer)

    route_ou = Column(Integer)
    route_role = Column(Integer)

    behandelaar_gm_id = Column(Integer)
    coordinator_gm_id = Column(Integer)
    aanvrager_gm_id = Column(Integer)
    aanvrager_type = Column(Text)

    archival_state = Column(Enum("overdragen", "vernietigen"))
    status = Column(
        Enum("new", "open", "stalled", "resolved", "deleted"), nullable=False
    )
    created = Column(UTCDateTime, nullable=False)
    registratiedatum = Column(UTCDate, nullable=False)
    streefafhandeldatum = Column(UTCDate)
    afhandeldatum = Column(UTCDate)

    stalled_until = Column(UTCDate)
    vernietigingsdatum = Column(UTCDateTime)

    milestone = Column(Integer, nullable=False)

    last_modified = Column(UTCDateTime, nullable=False)

    relates_to = Column(Integer)
    zaaktype_id = Column(Integer, nullable=False)
    zaaktype_node_id = Column(Integer, nullable=False)
    contactkanaal = Column(String, nullable=False)
    aanvraag_trigger = Column(Enum("extern", "intern"))
    onderwerp = Column(String)
    resultaat = Column(String)
    besluit = Column(String)
    aanvrager = Column(Integer, nullable=False)
    locatie_zaak = Column(Integer)
    locatie_correspondentie = Column(Integer)
    deleted = Column(UTCDateTime)
    vervolg_van = Column(Integer)
    aanvrager_gm_id = Column(Integer)
    payment_status = Column(String)
    payment_amount = Column(Numeric(100, 2))
    confidentiality = Column(Enum("public", "internal", "confidential"))
    onderwerp_extern = Column(String)
    duplicate_prevention_token = Column(GUID, nullable=False)
    resultaat_id = Column(Integer)
    urgency = Column(String)
    preset_client = Column(Boolean, nullable=False, default=False)
    prefix = Column(String)
    html_email_template = Column(String)
    betrokkenen_cache = Column(JSON, nullable=False)
    requestor_v1_json = Column(JSON, nullable=False)
    assignee_v1_json = Column(JSON, nullable=False)
    urgency_date_medium = Column(UTCDateTime)
    urgency_date_high = Column(UTCDateTime)


class ZaakBetrokkenen(Base):
    """Table containing links to entities "involved" with a case

    For instance, its assignee, the requestor, etc."""

    __tablename__ = "zaak_betrokkenen"
    id = Column(Integer, primary_key=True)
    zaak_id = Column(Integer, ForeignKey("zaak.id"), nullable=False)

    deleted = Column(UTCDateTime)

    betrokkene_type = Column(String, nullable=False)
    betrokkene_id = Column(Integer, nullable=False)
    gegevens_magazijn_id = Column(Integer, nullable=False)

    uuid = Column(GUID, nullable=False)
    subject_id = Column(GUID, nullable=False)

    naam = Column(String, nullable=False)
    rol = Column(String)
    magic_string_prefix = Column(String)
    verificatie = Column(String)

    pip_authorized = Column(Boolean, nullable=False, default=False)
    authorisation = Column(String)

    bibliotheek_kenmerken_id = Column(
        Integer, ForeignKey("bibliotheek_kenmerken.id"), nullable=True
    )


class ObjectData(Base):
    """Object storage (including mirrors of cases)"""

    __tablename__ = "object_data"
    uuid = Column(GUID, primary_key=True, nullable=False)
    object_class = Column(String, nullable=False)
    object_id = Column(Integer, nullable=True)
    class_uuid = Column(GUID, nullable=True)
    acl_groupname = Column(Enum("confidential", "public"), nullable=True)
    invalid = Column(Boolean, nullable=False)
    properties = Column(String)
    text_vector = Column(postgresql.TSVECTOR)
    index_hstore = Column(postgresql.HSTORE)
    date_created = Column(
        UTCDateTime,
        server_default=sql.text("NOW()"),
        nullable=False,
    )
    date_modified = Column(UTCDateTime)


class ObjectAclEntry(Base):
    """ACL entries for object_data rows"""

    __tablename__ = "object_acl_entry"

    uuid = Column(GUID, primary_key=True, nullable=False)

    object_uuid = Column(GUID, ForeignKey("object_data.uuid"), nullable=False)
    entity_type = Column(String, nullable=False)
    entity_id = Column(String, nullable=False)
    capability = Column(String, nullable=False)
    scope = Column(Enum("instance", "type"), nullable=False)
    groupname = Column(String, nullable=False)


class Group(Base):
    """User groups"""

    __tablename__ = "groups"

    id = Column(Integer, primary_key=True)
    path = Column(postgresql.ARRAY(Integer))
    name = Column(String)
    description = Column(String)
    date_created = Column(UTCDateTime)
    date_modified = Column(UTCDateTime)
    uuid = Column(GUID, nullable=False)

    roles = relationship("Role", back_populates="parent_group")


class Role(Base):
    """User roles"""

    __tablename__ = "roles"

    id = Column(Integer, primary_key=True)
    parent_group_id = Column(Integer, ForeignKey("groups.id"))
    parent_group = relationship("Group", back_populates="roles", uselist=False)

    name = Column(String)
    description = Column(String)
    system_role = Column(Boolean)
    date_created = Column(UTCDateTime)
    date_modified = Column(UTCDateTime)
    uuid = Column(GUID, nullable=False)


class Subject(Base):
    """Users of the system"""

    __tablename__ = "subject"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)
    subject_type = Column(Enum("person", "company", "employee"))
    properties = Column(JSONEncodedDict, nullable=False)
    settings = Column(JSONEncodedDict, nullable=False)
    username = Column(String, nullable=False)
    last_modified = Column(UTCDateTime, nullable=False)
    role_ids = Column(postgresql.ARRAY(Integer))
    group_ids = Column(postgresql.ARRAY(Integer))
    nobody = Column(Boolean, nullable=False, default=False)
    system = Column(Boolean, nullable=False, default=False)

    related_custom_object_id = Column(Integer, ForeignKey("custom_object.id"))
    related_custom_object = relationship("CustomObject")


class ContactRelationship(Base):
    "Relations between contacts"

    __tablename__ = "contact_relationship_view"
    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, unique=True, nullable=False)

    contact = Column(Integer, nullable=False)
    contact_uuid = Column(GUID, nullable=False)
    contact_type = Column(String, nullable=False)

    relation = Column(Integer, nullable=False)
    relation_uuid = Column(GUID, nullable=False)
    relation_type = Column(String, nullable=False)


class Logging(Base):
    """Log entries"""

    __tablename__ = "logging"

    id = Column(Integer, primary_key=True)
    zaak_id = Column(Integer, ForeignKey("zaak.id"))
    betrokkene_id = Column(String)
    aanvrager_id = Column(String)
    is_bericht = Column(Integer)
    component = Column(String)
    component_id = Column(Integer)
    seen = Column(Integer)
    onderwerp = Column(String)
    bericht = Column(String)
    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    deleted_on = Column(UTCDateTime)
    event_type = Column(String)
    event_data = Column(JSON)
    created_by = Column(String)
    modified_by = Column(String)
    deleted_by = Column(String)
    created_for = Column(String)
    created_by_name_cache = Column(String)
    object_uuid = Column(GUID, ForeignKey("object_data.uuid"))
    restricted = Column(Boolean, nullable=False, default=False)
    uuid = Column(GUID, nullable=False)


class Message(Base):
    """Messages for users of the system"""

    __tablename__ = "message"

    id = Column(Integer, primary_key=True)
    message = Column(String, nullable=False)
    subject_id = Column(String)
    logging_id = Column(Integer, ForeignKey("logging.id"), nullable=False)
    is_read = Column(Boolean)
    is_archived = Column(Boolean)


class BibliotheekCategorie(Base):
    """Catalog categories ("directories")"""

    __tablename__ = "bibliotheek_categorie"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)
    pid = Column(
        Integer, ForeignKey("bibliotheek_categorie.id"), nullable=True
    )
    naam = Column(String, nullable=False)
    created = Column(UTCDateTime, nullable=False)
    last_modified = Column(UTCDateTime, nullable=False)


class Directory(Base):
    """Directory data"""

    __tablename__ = "directory"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    case_id = Column(Integer, primary_key=True)
    original_name = Column(String, nullable=False)
    path = Column(postgresql.ARRAY(Integer))
    uuid = Column(GUID, nullable=False)


class File(Base):
    """File data"""

    __tablename__ = "file"

    id = Column(Integer, primary_key=True)
    filestore_id = Column(Integer, nullable=False)
    name = Column(String, nullable=False)
    extension = Column(String, nullable=False)
    root_file_id = Column(Integer)
    version = Column(Integer)
    case_id = Column(Integer)
    metadata_id = Column(Integer)
    subject_id = Column(String)
    directory_id = Column(Integer)
    creation_reason = Column(String, nullable=False)
    accepted = Column(Boolean, nullable=False)
    rejection_reason = Column(String)
    rejected_by_display_name = Column(String)
    reject_to_queue = Column(Boolean)
    is_duplicate_name = Column(Boolean, nullable=False)
    publish_pip = Column(Boolean, nullable=False)
    publish_website = Column(Boolean, nullable=False, default=False)
    date_created = Column(UTCDateTime, nullable=False)
    created_by = Column(String, nullable=False)
    date_modified = Column(UTCDateTime)
    modified_by = Column(String)
    date_deleted = Column(UTCDateTime)
    deleted_by = Column(String)
    destroyed = Column(Boolean, default=False)
    scheduled_jobs_id = Column(Integer)
    intake_owner = Column(String)
    active_version = Column(Boolean, nullable=False, default=False)
    is_duplicate_of = Column(Integer)

    # queue(bool) potentially show this file in "intake"-queue, if false then always excluded.
    queue = Column(Boolean, nullable=False, default=True)
    document_status = Column(
        Enum("original", "copy", "replaced", "converted"),
        nullable=False,
        default="original",
    )
    generator = Column(String)
    lock_timestamp = Column(UTCDateTime)
    lock_subject_id = Column(GUID)
    lock_subject_name = Column(String)
    uuid = Column(GUID, nullable=False)
    confidential = Column(Boolean, nullable=False, default=False)
    search_term = Column(String)
    search_index = Column(postgresql.TSVECTOR)

    object_type = Column(String)
    searchable_id = Column(Integer, nullable=False)
    search_order = Column(String)
    intake_group_id = Column(Integer, ForeignKey("groups.id"))
    intake_role_id = Column(Integer, ForeignKey("roles.id"))

    intake_group = relationship("Group", foreign_keys=[intake_group_id])
    intake_role = relationship("Role", foreign_keys=[intake_role_id])

    skip_intake = Column(Boolean, nullable=False, default=False)


class Filestore(Base):
    """File storage reference"""

    __tablename__ = "filestore"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)
    original_name = Column(String, nullable=False)
    size = Column(Integer, nullable=False)
    mimetype = Column(String, nullable=False)
    md5 = Column(String, nullable=False)

    date_created = Column(UTCDateTime, nullable=False)
    storage_location = Column(postgresql.ARRAY(String))
    is_archivable = Column(Boolean, nullable=False)
    virus_scan_status = Column(String, nullable=False)
    thumbnail_uuid = Column(GUID)


class BibliotheekSjabloon(Base):
    """Document templates for use in case types"""

    __tablename__ = "bibliotheek_sjablonen"
    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)
    bibliotheek_categorie_id = Column(
        Integer, ForeignKey("bibliotheek_categorie.id")
    )

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    deleted = Column(UTCDateTime)

    naam = Column(String, nullable=False)
    filestore_id = Column(Integer, ForeignKey("filestore.id"))
    help = Column(String)

    interface_id = Column(Integer, ForeignKey("interface.id"))
    template_external_name = Column(String)

    magic_strings = relationship("BibliotheekSjabloonMagicString")


class BibliotheekSjabloonMagicString(Base):
    """Cache table for magic strings used in document templates"""

    __tablename__ = "bibliotheek_sjablonen_magic_string"
    id = Column(Integer, primary_key=True)
    bibliotheek_sjablonen_id = Column(
        Integer, ForeignKey("bibliotheek_sjablonen.id"), nullable=False
    )
    value = Column(String, nullable=False)


class BibliotheekNotificatieKenmerk(Base):
    """Association table for attachments to email templates"""

    __tablename__ = "bibliotheek_notificatie_kenmerk"
    id = Column(Integer, primary_key=True)
    bibliotheek_notificatie_id = Column(
        Integer, ForeignKey("bibliotheek_notificaties.id"), nullable=False
    )
    bibliotheek_kenmerken_id = Column(
        Integer, ForeignKey("bibliotheek_kenmerken.id"), nullable=False
    )


class BibliotheekNotificatie(Base):
    """Email templates for use in case types"""

    __tablename__ = "bibliotheek_notificaties"
    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)
    bibliotheek_categorie_id = Column(
        Integer, ForeignKey("bibliotheek_categorie.id")
    )

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    deleted = Column(UTCDateTime)

    label = Column(String, nullable=False)
    subject = Column(String, nullable=False)
    message = Column(String, nullable=False)
    sender = Column(String)
    sender_address = Column(String)
    search_term = Column(String)

    attachments = relationship(
        "BibliotheekKenmerk",
        secondary="bibliotheek_notificatie_kenmerk",
        back_populates="attachment_to",
    )


VALUE_TYPES = [
    "address_v2",
    "appointment_v2",
    "appointment",
    "bag_adres",
    "bag_adressen",
    "bag_openbareruimte",
    "bag_openbareruimtes",
    "bag_straat_adres",
    "bag_straat_adressen",
    "bankaccount",
    "calendar_supersaas",
    "calendar",
    "checkbox",
    "date",
    "email",
    "file",
    "geojson",
    "geolatlon",
    "googlemaps",
    "image_from_url",
    "numeric",
    "option",
    "relationship",
    "richtext",
    "select",
    "subject",
    "text_uc",
    "text",
    "textarea",
    "url",
    "valuta",
    "valutaex",
    "valutaex21",
    "valutaex6",
    "valutain",
    "valutain21",
    "valutain6",
]


class BibliotheekKenmerk(Base):
    """Attributes for use in cases or object types"""

    __tablename__ = "bibliotheek_kenmerken"
    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)
    bibliotheek_categorie_id = Column(
        Integer, ForeignKey("bibliotheek_categorie.id")
    )

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    deleted = Column(UTCDateTime)

    naam = Column(String, nullable=False)
    naam_public = Column(String, nullable=False)
    magic_string = Column(String, nullable=False)
    value_type = Column(Enum(*VALUE_TYPES), nullable=False)
    value_default = Column(String)
    help = Column(String)
    document_categorie = Column(String)
    file_metadata_id = Column(Integer, ForeignKey("file_metadata.id"))

    type_multiple = Column(Boolean, nullable=False)
    properties = Column(JSONEncodedDict, nullable=False)
    search_term = Column(String)
    search_order = Column(String)
    version = Column(Integer)

    relationship_type = Column(String)
    relationship_name = Column(String)
    relationship_uuid = Column(GUID)

    attachment_to = relationship(
        "BibliotheekNotificatie",
        secondary="bibliotheek_notificatie_kenmerk",
        back_populates="attachments",
    )


class BibliotheekKenmerkenValues(Base):
    """Values of an attribute(BibliotheekKenmerk) of type option or checkbox"""

    __tablename__ = "bibliotheek_kenmerken_values"
    id = Column(Integer, primary_key=True)
    bibliotheek_kenmerken_id = Column(
        Integer, ForeignKey("bibliotheek_kenmerken.id")
    )
    value = Column(String)
    active = Column(Boolean)
    sort_order = Column(Integer)


class FileMetaData(Base):
    """FileMetaData of an attribute(BibliotheekKenmerk) of type file"""

    __tablename__ = "file_metadata"
    id = Column(Integer, primary_key=True)
    description = Column(String)
    trust_level = Column(String)
    origin = Column(String)
    document_category = Column(String)
    origin_date = Column(Date)
    pronom_format = Column(String)
    appearance = Column(String)
    structure = Column(String)


class ObjectBibliotheekEntry(Base):
    """Catalog entries for object types"""

    __tablename__ = "object_bibliotheek_entry"
    id = Column(Integer, primary_key=True)
    bibliotheek_categorie_id = Column(
        Integer, ForeignKey("bibliotheek_categorie.id"), nullable=True
    )

    object_uuid = Column(GUID, ForeignKey("object_data.uuid"), nullable=False)
    name = Column(String, nullable=False)


class Zaaktype(Base):
    """Case types"""

    __tablename__ = "zaaktype"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False, server_default=FetchedValue())

    bibliotheek_categorie_id = Column(
        Integer, ForeignKey("bibliotheek_categorie.id"), nullable=False
    )

    zaaktype_node_id = Column(
        Integer, ForeignKey("zaaktype_node.id"), nullable=False
    )
    version = Column(Integer, nullable=False)

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    deleted = Column(UTCDateTime)

    active = Column(Boolean, nullable=False)

    zaaktype_nodes = relationship(
        "ZaaktypeNode",
        foreign_keys="[ZaaktypeNode.zaaktype_id]",
        back_populates="zaaktype",
    )
    current_zaaktype_node = relationship(
        "ZaaktypeNode", foreign_keys=[zaaktype_node_id]
    )


class ZaaktypeNode(Base):
    """Case type versions"""

    __tablename__ = "zaaktype_node"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False, server_default=FetchedValue())

    zaaktype_id = Column(Integer, ForeignKey("zaaktype.id"))
    zaaktype_definitie_id = Column(
        Integer, ForeignKey("zaaktype_definitie.id")
    )

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    deleted = Column(UTCDateTime)

    titel = Column(String, nullable=False)
    code = Column(String, nullable=False)
    trigger = Column(String, nullable=False)
    version = Column(Integer)
    active = Column(Integer)
    properties = Column(JSONEncodedDict)
    is_public = Column(Boolean)
    zaaktype_trefwoorden = Column(String)
    zaaktype_omschrijving = Column(String)

    webform_toegang = Column(Integer)
    webform_authenticatie = Column(String)
    adres_relatie = Column(String)
    aanvrager_hergebruik = Column(Integer)
    automatisch_aanvragen = Column(Integer)
    automatisch_behandelen = Column(Integer)
    toewijzing_zaakintake = Column(Integer)
    toelichting = Column(String)
    online_betaling = Column(Integer)
    adres_andere_locatie = Column(Integer)
    adres_aanvrager = Column(Integer)
    adres_geojson = Column(Boolean)

    bedrijfid_wijzigen = Column(Integer)
    zaaktype_vertrouwelijk = Column(Integer)
    extra_relaties_in_aanvraag = Column(Boolean)
    contact_info_intake = Column(Boolean)
    prevent_pip = Column(Boolean, default=False)
    contact_info_email_required = Column(Boolean, default=False)
    contact_info_phone_required = Column(Boolean, default=False)
    contact_info_mobile_phone_required = Column(Boolean, default=False)
    moeder_zaaktype_id = Column(Integer)

    logging_id = Column(Integer, ForeignKey("logging.id"), nullable=True)
    zaaktype = relationship(
        "Zaaktype", foreign_keys=[zaaktype_id], back_populates="zaaktype_nodes"
    )
    uuid = Column(GUID, nullable=False)
    v0_json = Column(postgresql.JSONB)


class ZaaktypeStatus(Base):
    """A single phase in a case type version"""

    __tablename__ = "zaaktype_status"

    id = Column(Integer, primary_key=True, nullable=False)

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)

    zaaktype_node_id = Column(Integer, ForeignKey("zaaktype_node.id"))

    status = Column(Integer)
    status_type = Column(Text)

    naam = Column(String)
    fase = Column(String)
    ou_id = Column(Integer)
    role_id = Column(Integer)
    checklist = Column(Integer)
    role_set = Column(Integer)


class ZaaktypeKenmerk(Base):
    """Attributes, as used in case type versions"""

    __tablename__ = "zaaktype_kenmerken"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)

    zaaktype_node_id = Column(
        Integer, ForeignKey("zaaktype_node.id"), nullable=False
    )
    zaak_status_id = Column(
        Integer, ForeignKey("zaaktype_status.id"), nullable=False
    )

    bibliotheek_kenmerken_id = Column(
        Integer, ForeignKey("bibliotheek_kenmerken.id"), nullable=True
    )
    object_id = Column(Integer, ForeignKey("object_data.uuid"), nullable=True)

    value_mandatory = Column(Boolean)
    label = Column(Text)
    help = Column(Text)
    pip = Column(Integer)
    zaakinformatie_view = Column(Integer, nullable=True, default=1)
    bag_zaakadres = Column(Integer)
    value_default = Column(Text)
    pip_can_change = Column(Boolean)
    publish_public = Column(Integer)
    referential = Column(String(12))

    is_systeemkenmerk = Column(Boolean, nullable=True, default=False)
    required_permissions = Column(JSONEncodedDict)
    version = Column(Integer)
    help_extern = Column(Text)
    object_metadata = Column(JSONEncodedDict, nullable=False, default={})
    label_multiple = Column(Text)
    properties = Column(JSONEncodedDict, nullable=True)
    is_group = Column(Boolean, nullable=False, default=False)


class ZaaktypeDocumentKenmerkenMap(Base):
    """
    Represents the materialized view `zaaktype_document_kenmerken_map`, which
    contains the zaaktype_kenmerken_id (and some other useful fields) for the
    first instance of a document field in a case type.
    """

    __tablename__ = "zaaktype_document_kenmerken_map"

    zaaktype_node_id = Column(Integer, primary_key=True)
    bibliotheek_kenmerken_id = Column(
        Integer, nullable=False, primary_key=True
    )

    case_document_id = Column(Integer, nullable=False)
    case_document_uuid = Column(GUID, nullable=False)

    referential = Column(Boolean, nullable=False)
    magic_string = Column(Text, nullable=False)
    name = Column(Text)
    public_name = Column(Text)

    show_on_pip = Column(Boolean, nullable=False)
    show_on_website = Column(Boolean, nullable=False)


class ZaaktypeSjabloon(Base):
    """Document templates, as used in case type versions"""

    __tablename__ = "zaaktype_sjablonen"

    id = Column(Integer, primary_key=True)

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)

    zaaktype_node_id = Column(
        Integer, ForeignKey("zaaktype_node.id"), nullable=False
    )
    zaak_status_id = Column(
        Integer, ForeignKey("zaaktype_status.id"), nullable=False
    )

    bibliotheek_sjablonen_id = Column(
        Integer, ForeignKey("bibliotheek_sjablonen.id"), nullable=True
    )

    bibliotheek_kenmerken_id = Column(
        Integer, ForeignKey("bibliotheek_kenmerken.id"), nullable=True
    )
    automatisch_genereren = Column(Integer)  # TODO Make boolean
    target_format = Column(String)


class ZaaktypeNotificatie(Base):
    """Email templates, as used in case type versions"""

    __tablename__ = "zaaktype_notificatie"

    id = Column(Integer, primary_key=True)

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)

    zaaktype_node_id = Column(
        Integer, ForeignKey("zaaktype_node.id"), nullable=False
    )
    zaak_status_id = Column(
        Integer, ForeignKey("zaaktype_status.id"), nullable=False
    )
    bibliotheek_notificatie_id = Column(
        "bibliotheek_notificaties_id",
        Integer,
        ForeignKey("bibliotheek_notificaties.id"),
        nullable=False,
    )

    label = Column(String)
    rcpt = Column(String)
    onderwerp = Column(String)
    bericht = Column(String)
    intern_block = Column(Integer)
    email = Column(String)
    behandelaar = Column(String)
    automatic = Column(Integer)
    cc = Column(String)
    bcc = Column(String)
    betrokkene_role = Column(String)


class ZaaktypeRelatie(Base):
    """Casetypes related to a casetype"""

    __tablename__ = "zaaktype_relatie"

    id = Column(Integer, primary_key=True, nullable=False)

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)

    zaaktype_node_id = Column(Integer, ForeignKey("zaaktype_node.id"))
    zaaktype_status_id = Column(Integer, ForeignKey("zaaktype_status.id"))
    relatie_zaaktype_id = Column(Integer, ForeignKey("zaaktype.id"))
    relatie_type = Column(String)

    start_delay = Column(String(255))
    status = Column(Integer)
    kopieren_kenmerken = Column(Integer)
    ou_id = Column(Integer, ForeignKey("groups.id"))
    role_id = Column(Integer, ForeignKey("roles.id"))
    automatisch_behandelen = Column(Boolean)
    required = Column(String(12))

    subject_role = Column(postgresql.ARRAY(String))
    copy_subject_role = Column(Boolean, default=False)

    betrokkene_authorized = Column(Boolean)
    betrokkene_notify = Column(String)
    betrokkene_id = Column(String)
    betrokkene_role = Column(String)
    betrokkene_role_set = Column(String)
    betrokkene_prefix = Column(String)

    eigenaar_type = Column(String, nullable=False, default="aanvrager")
    eigenaar_role = Column(String)
    eigenaar_id = Column(String)

    show_in_pip = Column(Boolean, nullable=False, default=False)
    pip_label = Column(String)


class ZaaktypeResultaten(Base):
    __tablename__ = "zaaktype_resultaten"

    id = Column(Integer, primary_key=True)

    uuid = Column(GUID, nullable=False)
    zaaktype_node_id = Column(Integer)
    zaaktype_status_id = Column(Integer)
    resultaat = Column(String)
    ingang = Column(String)
    bewaartermijn = Column(Integer)
    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    dossiertype = Column(String)
    label = Column(String)
    selectielijst = Column(String)
    archiefnominatie = Column(String)
    comments = Column(String)
    external_reference = Column(String)
    trigger_archival = Column(Boolean)
    selectielijst_brondatum = Column(Date)
    selectielijst_einddatum = Column(Date)
    properties = Column(JSONEncodedDict)
    standaard_keuze = Column(Boolean)


class ZaaktypeStandaardBetrokkenen(Base):
    """Involved entities related to a casetype"""

    __tablename__ = "zaaktype_standaard_betrokkenen"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID)

    zaaktype_node_id = Column(
        Integer, ForeignKey("zaaktype_node.id"), nullable=False
    )
    zaak_status_id = Column(
        Integer, ForeignKey("zaaktype_status.id"), nullable=False
    )
    betrokkene_type = Column(String)
    betrokkene_identifier = Column(String)
    naam = Column(String)
    rol = Column(String)
    magic_string_prefix = Column(String)
    gemachtigd = Column(Boolean)
    notify = Column(Boolean)


class ZaaktypeRegel(Base):
    __tablename__ = "zaaktype_regel"

    id = Column(Integer, primary_key=True)

    zaaktype_node_id = Column(Integer)
    zaak_status_id = Column(Integer)
    naam = Column(String)
    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    settings = Column(String)
    active = Column(Boolean)
    is_group = Column(Boolean)


class ResultPreservationTerms(Base):
    __tablename__ = "result_preservation_terms"

    id = Column(Integer, primary_key=True)

    code = Column(Integer)
    label = Column(String)
    unit = Column(String)
    unit_amount = Column(Integer)


class CaseAuthorisation(Base):
    """Case-level authorisation rules"""

    __tablename__ = "zaak_authorisation"
    id = Column(Integer, primary_key=True)
    zaak_id = Column(Integer, ForeignKey("zaak.id"), nullable=False)
    capability = Column(
        Enum("search", "read", "write", "manage"), nullable=False
    )
    entity_id = Column(Text, nullable=False)
    entity_type = Column(Enum("position", "user"), nullable=False)
    scope = Column(Enum("instance"), nullable=False)


class Interface(Base):
    """Contains configuration of integrations with external systems"""

    __tablename__ = "interface"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID)
    name = Column(String)
    active = Column(Boolean)
    multiple = Column(Boolean)
    module = Column(String)
    date_deleted = Column(UTCDateTime)
    interface_config = Column(JSONEncodedDict)


class FileCaseDocument(Base):
    __tablename__ = "file_case_document"

    id = Column(Integer, primary_key=True, autoincrement=True)
    file_id = Column(Integer, nullable=False)
    magic_string = Column(
        String,
        ForeignKey("bibliotheek_kenmerken.magic_string"),
        nullable=False,
    )
    bibliotheek_kenmerken_id = Column(
        String,
        ForeignKey("bibliotheek_kenmerken.id"),
        nullable=False,
    )
    case_id = Column(Integer, ForeignKey("zaak.id"), nullable=False)


class Bedrijf(Base):
    __tablename__ = "bedrijf"

    uuid = Column(GUID)
    id = Column(Integer, primary_key=True)
    object_type = Column(String)
    handelsnaam = Column(Text)
    email = Column(String(128))
    telefoonnummer = Column(String(10))
    werkzamepersonen = Column(Integer)

    dossiernummer = Column(String(8))
    subdossiernummer = Column(String(4))
    hoofdvestiging_dossiernummer = Column(String(8))
    hoofdvestiging_subdossiernummer = Column(String(4))

    vorig_dossiernummer = Column(String(8))

    vorig_subdossiernummer = Column(String(4))

    rechtsvorm = Column(SmallInteger)
    kamernummer = Column(SmallInteger)
    faillisement = Column(SmallInteger)
    surseance = Column(SmallInteger)

    contact_naam = Column(String(64))
    contact_aanspreektitel = Column(String(45))
    contact_voorvoegsel = Column(String(8))
    contact_voorletters = Column(String(19))
    contact_geslachtsnaam = Column(String(95))
    contact_geslachtsaanduiding = Column(String(1))

    vestiging_adres = Column(Text)
    vestiging_straatnaam = Column(Text)
    vestiging_huisnummer = Column(BigInteger)
    vestiging_huisnummertoevoeging = Column(Text)
    vestiging_postcodewoonplaats = Column(Text)
    vestiging_postcode = Column(String(6))
    vestiging_woonplaats = Column(Text)

    correspondentie_straatnaam = Column(String)
    correspondentie_huisnummer = Column(Integer)
    correspondentie_huisnummertoevoeging = Column(String)
    correspondentie_postcodewoonplaats = Column(String)
    correspondentie_postcode = Column(String)
    correspondentie_woonplaats = Column(String)
    correspondentie_adres = Column(String)

    hoofdactiviteitencode = Column(Integer)
    nevenactiviteitencode1 = Column(Integer)
    nevenactiviteitencode2 = Column(Integer)

    authenticated = Column(SmallInteger)
    authenticatedby = Column(Text)
    fulldossiernummer = Column(Text)
    import_datum = Column(UTCDateTime)
    deleted_on = Column(UTCDateTime)
    verblijfsobject_id = Column(String(16))
    system_of_record = Column(String(32))
    system_of_record_id = Column(BigInteger)
    vestigingsnummer = Column(BigInteger)
    vestiging_huisletter = Column(Text)
    correspondentie_huisletter = Column(Text)
    vestiging_adres_buitenland1 = Column(Text)
    vestiging_adres_buitenland2 = Column(Text)
    vestiging_adres_buitenland3 = Column(Text)
    vestiging_landcode = Column(Integer, nullable=False, default=6030)
    correspondentie_adres_buitenland1 = Column(Text)
    correspondentie_adres_buitenland2 = Column(Text)
    correspondentie_adres_buitenland3 = Column(Text)
    correspondentie_landcode = Column(Integer, nullable=False, default=6030)

    rsin = Column(BigInteger)
    oin = Column(BigInteger)

    date_founded = Column(Date)
    date_registration = Column(Date)
    date_ceased = Column(Date)

    main_activity = Column(JSON, nullable=False, server_default="{}")
    secondairy_activities = Column(JSON, nullable=False, server_default="[]")

    search_term = Column(String)
    search_order = Column(String)

    related_custom_object_id = Column(Integer, ForeignKey("custom_object.id"))
    related_custom_object = relationship("CustomObject")
    preferred_contact_channel = Column(String)
    vestiging_bag_id = Column(BigInteger)


class NatuurlijkPersoon(Base):
    __tablename__ = "natuurlijk_persoon"

    uuid = Column(GUID)
    id = Column(Integer, primary_key=True)
    object_type = Column(String)
    search_term = Column(String)
    search_order = Column(String)
    burgerservicenummer = Column(String(9))
    a_nummer = Column(String(10))
    voorletters = Column(String(50))
    voornamen = Column(Text)
    geslachtsnaam = Column(Text)
    voorvoegsel = Column(String(50))
    geslachtsaanduiding = Column(String(3))
    nationaliteitscode1 = Column(SmallInteger)
    nationaliteitscode2 = Column(SmallInteger)
    nationaliteitscode3 = Column(SmallInteger)
    geboorteplaats = Column(Text)
    geboorteland = Column(Text)
    geboortedatum = Column(UTCDateTime)
    aanhef_aanschrijving = Column(String(10))
    voorletters_aanschrijving = Column(String(20))
    voornamen_aanschrijving = Column(String(200))
    naam_aanschrijving = Column(Text)
    voorvoegsel_aanschrijving = Column(String(50))
    burgerlijke_staat = Column(String(1))
    indicatie_geheim = Column(String(1))
    land_waarnaar_vertrokken = Column(SmallInteger)
    import_datum = Column(UTCDateTime)
    adres_id = Column(Integer)

    authenticated = Column(Boolean, nullable=False, default=False)

    authenticatedby = Column(Text)

    deleted_on = Column(UTCDateTime)
    verblijfsobject_id = Column(String(16))
    datum_overlijden = Column(UTCDateTime)
    aanduiding_naamgebruik = Column(String(1))
    onderzoek_persoon = Column(Boolean)
    onderzoek_huwelijk = Column(Boolean)
    onderzoek_overlijden = Column(Boolean)
    onderzoek_verblijfplaats = Column(Boolean)
    partner_a_nummer = Column(String(50))
    partner_burgerservicenummer = Column(String(50))
    partner_voorvoegsel = Column(String(50))
    partner_geslachtsnaam = Column(String(50))
    datum_huwelijk = Column(UTCDateTime)
    datum_huwelijk_ontbinding = Column(UTCDateTime)
    in_gemeente = Column(Boolean)
    landcode = Column(Integer, nullable=False, default=6030)
    naamgebruik = Column(Text)
    active = Column(Boolean, nullable=False, default=True)
    adellijke_titel = Column(Text)

    related_custom_object_id = Column(Integer, ForeignKey("custom_object.id"))
    related_custom_object = relationship("CustomObject")
    preferred_contact_channel = Column(String)

    surname = Column(String)


class Adres(Base):
    __tablename__ = "adres"

    id = Column(Integer, primary_key=True)

    straatnaam = Column(Text)
    huisnummer = Column(BigInteger)
    huisletter = Column(String(1))
    huisnummertoevoeging = Column(Text)
    nadere_aanduiding = Column(String(35))
    postcode = Column(String(6))

    woonplaats = Column(Text)
    gemeentedeel = Column(Text)
    functie_adres = Column(String(1), nullable=False)
    datum_aanvang_bewoning = Column(Date)
    woonplaats_id = Column(String(32))

    gemeente_code = Column(SmallInteger)
    hash = Column(String(32))
    import_datum = Column(UTCDateTime)
    deleted_on = Column(UTCDateTime)
    adres_buitenland1 = Column(Text)
    adres_buitenland2 = Column(Text)
    adres_buitenland3 = Column(Text)
    landcode = Column(Integer, nullable=False, default=6030)
    natuurlijk_persoon_id = Column(
        Integer, ForeignKey(NatuurlijkPersoon.id), nullable=False
    )
    bag_id = Column(Text)


class ContactData(Base):
    """Contact data in the system"""

    __tablename__ = "contact_data"

    id = Column(Integer, primary_key=True)
    gegevens_magazijn_id = Column(Integer)
    betrokkene_type = Column(Integer)
    mobiel = Column(String(255))
    telefoonnummer = Column(String(255))
    email = Column(String(255))
    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    note = Column(String)


class GmNatuurlijkPersoon(Base):
    """Snapshot of citizens in the system"""

    __tablename__ = "gm_natuurlijk_persoon"

    id = Column(Integer, primary_key=True)
    gegevens_magazijn_id = Column(
        Integer, ForeignKey("natuurlijk_persoon.id"), nullable=False
    )
    betrokkene_type = Column(Integer)

    burgerservicenummer = Column(String(9))
    a_nummer = Column(String(10))
    voorletters = Column(String(50))
    voornamen = Column(Text)
    geslachtsnaam = Column(Text)
    voorvoegsel = Column(String(50))
    geslachtsaanduiding = Column(String(3))
    nationaliteitscode1 = Column(SmallInteger)
    nationaliteitscode2 = Column(SmallInteger)
    nationaliteitscode3 = Column(SmallInteger)
    geboorteplaats = Column(Text)
    geboorteland = Column(Text)
    geboortedatum = Column(UTCDateTime)
    aanhef_aanschrijving = Column(String(10))
    voorletters_aanschrijving = Column(String(20))
    voornamen_aanschrijving = Column(String(200))
    naam_aanschrijving = Column(Text)
    voorvoegsel_aanschrijving = Column(String(50))
    burgerlijke_staat = Column(String(1))
    indicatie_geheim = Column(String(1))
    import_datum = Column(UTCDateTime)
    adres_id = Column(Integer, ForeignKey("gm_adres.id"), nullable=False)

    authenticated = Column(SmallInteger)

    authenticatedby = Column(Text)

    verblijfsobject_id = Column(String(16))
    datum_overlijden = Column(UTCDateTime)
    aanduiding_naamgebruik = Column(String(1))
    onderzoek_persoon = Column(Boolean)
    onderzoek_huwelijk = Column(Boolean)
    onderzoek_overlijden = Column(Boolean)
    onderzoek_verblijfplaats = Column(Boolean)
    partner_a_nummer = Column(String(50))
    partner_burgerservicenummer = Column(String(50))
    partner_voorvoegsel = Column(String(50))
    partner_geslachtsnaam = Column(String(50))
    datum_huwelijk = Column(UTCDateTime)
    datum_huwelijk_ontbinding = Column(UTCDateTime)
    landcode = Column(Integer, nullable=False, default=6030)
    naamgebruik = Column(Text)
    adellijke_titel = Column(Text)


class GmAdres(Base):
    """Snapshot of addresses in the system"""

    __tablename__ = "gm_adres"

    id = Column(Integer, primary_key=True)

    straatnaam = Column(Text)
    huisnummer = Column(BigInteger)
    huisletter = Column(String(1))
    huisnummertoevoeging = Column(Text)
    nadere_aanduiding = Column(String(35))
    postcode = Column(String(6))

    woonplaats = Column(Text)
    gemeentedeel = Column(Text)
    functie_adres = Column(String(1), nullable=False)
    datum_aanvang_bewoning = Column(Date)
    woonplaats_id = Column(String(32))

    gemeente_code = Column(SmallInteger)
    hash = Column(String(32))
    import_datum = Column(UTCDateTime)
    deleted_on = Column(UTCDateTime)
    adres_buitenland1 = Column(Text)
    adres_buitenland2 = Column(Text)
    adres_buitenland3 = Column(Text)
    landcode = Column(Integer, nullable=False, default=6030)
    natuurlijk_persoon_id = Column(
        Integer, ForeignKey("gm_natuurlijk_persoon.id"), nullable=False
    )


class GmBedrijf(Base):
    __tablename__ = "gm_bedrijf"

    id = Column(Integer, primary_key=True)
    gegevens_magazijn_id = Column(
        Integer, ForeignKey("bedrijf.id"), nullable=False
    )
    handelsnaam = Column(Text)
    email = Column(String(128))
    telefoonnummer = Column(String(10))
    werkzamepersonen = Column(Integer)

    dossiernummer = Column(String(8))
    subdossiernummer = Column(String(4))
    hoofdvestiging_dossiernummer = Column(String(8))
    hoofdvestiging_subdossiernummer = Column(String(4))

    vorig_dossiernummer = Column(String(8))

    vorig_subdossiernummer = Column(String(4))

    rechtsvorm = Column(SmallInteger)
    kamernummer = Column(SmallInteger)
    faillisement = Column(SmallInteger)
    surseance = Column(SmallInteger)

    contact_naam = Column(String(64))
    contact_aanspreektitel = Column(String(45))
    contact_voorvoegsel = Column(String(8))
    contact_voorletters = Column(String(19))
    contact_geslachtsnaam = Column(String(95))
    contact_geslachtsaanduiding = Column(String(1))

    vestiging_adres = Column(Text)
    vestiging_straatnaam = Column(Text)
    vestiging_huisnummer = Column(BigInteger)
    vestiging_huisnummertoevoeging = Column(Text)
    vestiging_postcodewoonplaats = Column(Text)
    vestiging_postcode = Column(String(6))
    vestiging_woonplaats = Column(Text)

    correspondentie_straatnaam = Column(String)
    correspondentie_huisnummer = Column(Integer)
    correspondentie_huisnummertoevoeging = Column(String)
    correspondentie_postcodewoonplaats = Column(String)
    correspondentie_postcode = Column(String)
    correspondentie_woonplaats = Column(String)
    correspondentie_adres = Column(String)

    hoofdactiviteitencode = Column(Integer)
    nevenactiviteitencode1 = Column(Integer)
    nevenactiviteitencode2 = Column(Integer)

    authenticated = Column(SmallInteger)
    authenticatedby = Column(Text)
    import_datum = Column(UTCDateTime)
    verblijfsobject_id = Column(String(16))
    system_of_record = Column(String(32))
    system_of_record_id = Column(BigInteger)
    vestigingsnummer = Column(BigInteger)
    vestiging_huisletter = Column(Text)
    correspondentie_huisletter = Column(Text)
    vestiging_adres_buitenland1 = Column(Text)
    vestiging_adres_buitenland2 = Column(Text)
    vestiging_adres_buitenland3 = Column(Text)
    vestiging_landcode = Column(Integer, nullable=False, default=6030)
    correspondentie_adres_buitenland1 = Column(Text)
    correspondentie_adres_buitenland2 = Column(Text)
    correspondentie_adres_buitenland3 = Column(Text)
    correspondentie_landcode = Column(Integer, nullable=False, default=6030)


class Config(Base):
    """Contains configuration of the system"""

    __tablename__ = "config"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID)
    definition_id = Column(GUID, nullable=False)
    parameter = Column(String(128))
    value = Column(String)
    advanced = Column(Boolean, nullable=False, default=True)


class ZaaktypeDefinitie(Base):
    """Case type definitions"""

    __tablename__ = "zaaktype_definitie"

    id = Column(Integer, primary_key=True)
    openbaarheid = Column(String(255))
    handelingsinitiator = Column(String(255))
    grondslag = Column(String)
    procesbeschrijving = Column(String(255))
    afhandeltermijn = Column(String(255))
    afhandeltermijn_type = Column(String(255))
    selectielijst = Column(String(255))
    servicenorm = Column(String(255))
    servicenorm_type = Column(String)
    pdc_voorwaarden = Column(String)
    pdc_description = Column(String)
    pdc_meenemen = Column(String)
    pdc_tarief = Column(String)
    omschrijving_upl = Column(String)
    aard = Column(String)
    preset_client = Column(String(255))
    extra_informatie = Column(String)
    extra_informatie_extern = Column(String)


class ZaakKenmerk(Base):
    """Custom attributes of the case."""

    __tablename__ = "zaak_kenmerk"

    id = Column(Integer, primary_key=True)
    zaak_id = Column(Integer, ForeignKey("zaak.id"), nullable=False)
    bibliotheek_kenmerken_id = Column(
        Integer, ForeignKey("bibliotheek_kenmerken.id"), nullable=False
    )
    value = Column(String, nullable=False)
    magic_string = Column(
        String,
        nullable=False,
    )


class ZaaktypeBetrokkenen(Base):
    """Requestors of a castype."""

    __tablename__ = "zaaktype_betrokkenen"

    id = Column(Integer, primary_key=True)
    zaaktype_node_id = Column(Integer, ForeignKey("zaaktype_node.id"))
    betrokkene_type = Column(String)
    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)


class CaseAction(Base):
    """Actions for a case during phase transition."""

    __tablename__ = "case_action"

    id = Column(Integer, primary_key=True, nullable=False)
    case_id = Column(Integer, ForeignKey("zaak.id"), nullable=False)
    casetype_status_id = Column(Integer, ForeignKey("zaaktype_status.id"))
    type = Column(String(64))
    label = Column(String(255))
    automatic = Column(Boolean)
    data = Column(JSONEncodedDict)
    state_tainted = Column(Boolean, default=False)
    data_tainted = Column(Boolean, default=False)


class ZaaktypeChecklistItem(Base):
    """Checklist items, as used in casetype version"""

    __tablename__ = "zaaktype_status_checklist_item"

    id = Column(Integer, primary_key=True, nullable=False)
    casetype_status_id = Column(
        Integer, ForeignKey("zaaktype_status.id"), nullable=False
    )
    label = Column(Text)
    external_reference = Column(Text)


class Checklist(Base):
    """Checklist associated with a case"""

    __tablename__ = "checklist"

    id = Column(Integer, primary_key=True, nullable=False)
    case_id = Column(Integer, ForeignKey("zaak.id"), nullable=False)
    case_milestone = Column(Integer)


class ChecklistItem(Base):
    """Checklist items associated with a case"""

    __tablename__ = "checklist_item"

    id = Column(Integer, primary_key=True, nullable=False)
    uuid = uuid = Column(GUID)
    checklist_id = Column(Integer, ForeignKey("checklist.id"), nullable=False)
    label = Column(Text)
    state = Column(Boolean, default=False, nullable=False)
    sequence = Column(Integer)
    user_defined = Column(Boolean, default=True, nullable=False)
    deprecated_answer = Column(String)
    due_date = Column(Date)
    description = Column(Text)
    assignee_id = Column(Integer, ForeignKey("subject.id"), nullable=True)


class UserEntity(Base):
    """User entitiy related with a subject"""

    __tablename__ = "user_entity"

    id = Column(Integer, primary_key=True, nullable=False)
    uuid = uuid = Column(GUID)
    source_interface_id = Column(Integer, ForeignKey("interface.id"))
    source_identifier = Column(Text, nullable=False)
    subject_id = Column(Integer, ForeignKey("subject.id"))
    date_created = Column(UTCDateTime)
    date_deleted = Column(UTCDateTime)
    properties = Column(JSONEncodedDict, default="{}")
    password = Column(String(255))
    active = Column(Boolean, default=True, nullable=False)


class ObjectRelationship(Base):
    """Relationships between objects"""

    __tablename__ = "object_relationships"

    uuid = Column(GUID, primary_key=True, nullable=False)
    object1_uuid = Column(GUID, nullable=False)
    object2_uuid = Column(GUID, nullable=False)
    type1 = Column(Text, nullable=False)
    type2 = Column(Text, nullable=False)
    object1_type = Column(Text, nullable=False)
    object2_type = Column(Text, nullable=False)
    blocks_deletion = Column(Boolean, default=False, nullable=False)
    title1 = Column(Text)
    title2 = Column(Text)
    owner_object_uuid = Column(GUID, nullable=True)


class CaseRelation(Base):
    """Relationships between cases used for sorting"""

    __tablename__ = "case_relation"

    id = Column(Integer, primary_key=True, nullable=False)
    # These fields are nullable on the database, but that's
    # actually wrong, so we force them to not null Python side
    case_id_a = Column(Integer, ForeignKey("zaak.id"), nullable=False)
    case_id_b = Column(Integer, ForeignKey("zaak.id"), nullable=False)
    order_seq_a = Column(Integer, nullable=False)
    order_seq_b = Column(Integer, nullable=False)
    type_a = Column(Text, nullable=False)
    type_b = Column(Text, nullable=False)
    uuid = Column(GUID)


class CaseAcl(Base):
    """View on several tables, to determine which user has access to which case"""

    __tablename__ = "case_acl"
    __table_args__ = (
        PrimaryKeyConstraint(
            "case_id", "subject_id", "permission", "casetype_id"
        ),
    )
    case_id = Column(Integer, ForeignKey("zaak.id"), nullable=False)
    case_uuid = Column(Integer, ForeignKey("zaak.uuid"), nullable=False)
    permission = Column(
        Enum("manage", "write", "read", "search"), nullable=False
    )
    subject_id = Column(Integer, ForeignKey("subject.id"), nullable=False)

    subject_uuid = Column(Integer, ForeignKey("subject.uuid"), nullable=False)
    casetype_id = Column(Integer, ForeignKey("zaaktype.id"), nullable=False)


class ZaaktypeAuthorisation(Base):
    __tablename__ = "zaaktype_authorisation"

    zaaktype_node_id = Column(
        Integer, ForeignKey("zaaktype_node.id"), nullable=False
    )
    current_zaaktype_node = relationship(
        "ZaaktypeNode", foreign_keys=[zaaktype_node_id]
    )

    id = Column(Integer, primary_key=True, nullable=False)
    recht = Column(Text, nullable=False)
    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)
    deleted = Column(UTCDateTime)
    role_id = Column(Integer, ForeignKey("roles.id"))
    ou_id = Column(Integer, ForeignKey("groups.id"))

    zaaktype_id = Column(Integer, ForeignKey("zaaktype.id"))
    zaaktype = relationship("Zaaktype")

    confidential = Column(Boolean, nullable=False, default=False)


class SubjectPositionMatrix(Base):

    __tablename__ = "subject_position_matrix"

    __table_args__ = (
        PrimaryKeyConstraint("subject_id", "group_id", "role_id", "position"),
    )

    subject_id = Column(Integer)
    group_id = Column(Integer)
    role_id = Column(Integer)
    position = Column(Text)


class FileDerivative(Base):
    """Preview data for a file"""

    __tablename__ = "file_derivative"

    id = Column(GUID, primary_key=True)
    file_id = Column(Integer, ForeignKey("file.id"), nullable=False)
    filestore_id = Column(Integer, ForeignKey("filestore.id"), nullable=False)
    max_width = Column(Integer, nullable=False)
    max_height = Column(Integer, nullable=False)
    date_generated = Column(UTCDateTime, nullable=False)
    type = Column(Text, nullable=False)


# GEO Schemas
class ServiceGeoJSON(Base):
    """Model representing the service_geojson table"""

    __tablename__ = "service_geojson"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False, unique=True)
    geo_json = Column(JSON, nullable=False)


class ServiceGeoJSONRelationship(Base):
    """Model representing sub-table service_geojson_relationship"""

    __tablename__ = "service_geojson_relationship"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False, unique=True)
    related_uuid = Column(
        GUID, ForeignKey("service_geojson.uuid"), nullable=False
    )
    service_geojson_id = Column(
        Integer, ForeignKey("service_geojson.uuid"), nullable=False
    )

    UniqueConstraint(related_uuid, service_geojson_id, name="unique_relation")


class CustomObjectTypeAcl(Base):
    """View on several tables, to determine which user has access to which custom_object"""

    __tablename__ = "custom_object_type_acl"
    __table_args__ = (
        PrimaryKeyConstraint(
            "custom_object_type_id", "group_id", "role_id", "authorization"
        ),
    )
    custom_object_type_id = Column(
        Integer, ForeignKey("custom_object_type.id"), nullable=False
    )
    authorization = Column(Enum("readwrite", "admin", "read"), nullable=False)
    group_id = Column(Integer, ForeignKey("groups.id"), nullable=False)
    role_id = Column(Integer, ForeignKey("roles.id"), nullable=False)


class Transaction(Base):
    """Model representing transaction table"""

    __tablename__ = "transaction"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False, unique=True)
    interface_id = Column(Integer, nullable=False)
    external_transaction_id = Column(String, nullable=False)
    input_data = Column(String)
    input_file = Column(String)
    automated_retry_count = Column(Integer)
    date_created = Column(UTCDateTime, nullable=False)
    date_last_retry = Column(UTCDateTime)
    date_next_retry = Column(UTCDateTime)
    processed = Column(Boolean, default=False)
    date_deleted = Column(UTCDateTime)
    error_count = Column(Integer, default=0)
    direction = Column(String, nullable=False)
    success_count = Column(Integer, default=0)
    total_count = Column(Integer, default=0)
    processor_params = Column(String)
    error_fatal = Column(Boolean)
    preview_data = Column(Text, nullable=False)
    error_message = Column(Text)
    text_vector = Column(postgresql.TSVECTOR, nullable=False)


class TransactionRecord(Base):
    """Model representing transaction_record table"""

    __tablename__ = "transaction_record"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False, unique=True)
    transaction_id = Column(Integer, nullable=False)
    input = Column(String, nullable=False)
    output = Column(String, nullable=False)
    is_error = Column(Boolean, nullable=False)
    date_executed = Column(UTCDateTime, nullable=False)
    date_deleted = Column(UTCDateTime, nullable=False)
    preview_string = Column(String)
    last_error = Column(String, nullable=False)


class ExportQueue(Base):
    """Model representing export_queue table"""

    __tablename__ = "export_queue"
    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False, unique=True)
    subject_id = Column(Integer, ForeignKey("subject.id"), nullable=False)
    subject_uuid = Column(GUID, ForeignKey("subject.uuid"), nullable=False)
    expires = Column(
        UTCDateTime, server_default=sql.text("NOW() + 3 days"), nullable=False
    )
    token = Column(Text, nullable=False)
    filestore_id = Column(Integer, ForeignKey("filestore.id"), nullable=False)
    filestore_uuid = Column(GUID, ForeignKey("filestore.uuid"), nullable=False)
    downloaded = Column(Integer, nullable=False, default=0)


class ZaakBag(Base):
    """Model representing zaak_bag table"""

    __tablename__ = "zaak_bag"
    id = Column(Integer, nullable=False, primary_key=True)
    pid = Column(Integer)
    zaak_id = Column(Integer)
    bag_type = Column(Text)
    bag_id = Column(Text)
    bag_verblijfsobject_id = Column(Text)
    bag_openbareruimte_id = Column(Text)
    bag_nummeraanduiding_id = Column(Text)
    bag_pand_id = Column(Text)
    bag_standplaats_id = Column(Text)
    bag_ligplaats_id = Column(Text)
    bag_coordinates_wsg = Column(Text)


class CaseRelationship(Base):
    "Relations between cases"

    __tablename__ = "view_case_relationship"
    id = Column(Integer, primary_key=True)
    case_id = Column(Integer)
    relation_id = Column(Integer)
    type = Column(Text)
    relation_uuid = Column(GUID)
    order_seq = Column(Integer)


class CountryCode(Base):
    __tablename__ = "country_code"
    id = Column(Integer, nullable=False, primary_key=True)
    dutch_code = Column(Integer)
    uuid = Column(GUID, nullable=False, unique=True)
    label = Column(Text)
    historical = Column(Boolean)


class SavedSearch(Base):
    __tablename__ = "saved_search"
    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True)
    uuid = Column(GUID, nullable=False, unique=True)
    name = Column(Text, nullable=False)
    kind = Column(Text, nullable=False)
    owner_id = Column(Integer, ForeignKey("subject.id"), nullable=False)
    filters = Column(JSON, nullable=False)
    permissions = Column(postgresql.ARRAY(Text), nullable=False)
    columns = Column(JSON, nullable=False)
    sort_column = Column(Text, nullable=False)
    sort_order = Column(Text, nullable=False)


class SavedSearchPermission(Base):
    __tablename__ = "saved_search_permission"
    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True)
    saved_search_id = Column(
        Integer, ForeignKey("saved_search.id"), nullable=False
    )
    group_id = Column(Integer, ForeignKey("groups.id"), nullable=False)
    role_id = Column(Integer, ForeignKey("roles.id"), nullable=False)
    permission = Column(Text, nullable=False)

    __table_args__ = (
        UniqueConstraint(
            "saved_search_id", "group_id", "role_id", "permission"
        ),
    )


class ObjectSubscription(Base):
    __tablename__ = "object_subscription"

    id = Column(Integer, nullable=False, primary_key=True)
    interface_id = Column(Integer)
    external_id = Column(Text)
    local_table = Column(Text)
    local_id = Column(Text)
    date_created = Column(UTCDateTime)
    date_deleted = Column(UTCDateTime)
    object_preview = Column(Text)
    config_interface_id = Column(Integer)


class CaseV2(Base):
    __tablename__ = "view_case_v2"
    id = Column(Integer, primary_key=True)
    uuid = Column(GUID)
    onderwerp = Column(String)
    route_ou = Column(Integer)
    route_role = Column(Integer)
    vernietigingsdatum = Column(UTCDateTime)
    archival_state = Column(Text)
    status = Column(Text)
    contactkanaal = Column(String)
    created = Column(UTCDateTime)
    registratiedatum = Column(UTCDateTime)
    streefafhandeldatum = Column(UTCDateTime)
    afhandeldatum = Column(UTCDateTime)
    stalled_until = Column(UTCDateTime)
    milestone = Column(Integer, nullable=False)
    last_modified = Column(UTCDateTime, nullable=False)
    behandelaar_gm_id = Column(Integer)
    coordinator_gm_id = Column(Integer)
    aanvrager = Column(Integer)
    aanvraag_trigger = Column(Enum("extern", "intern"))
    onderwerp_extern = Column(String)
    resultaat = Column(String)
    resultaat_id = Column(Integer)
    payment_amount = Column(Numeric(100, 2))
    payment_status = Column(String)
    confidentiality = Column(postgresql.JSONB)
    behandelaar = Column(Integer)
    coordinator = Column(Integer)
    urgency = Column(String)
    preset_client = Column(Boolean, nullable=False, default=False)
    prefix = Column(String)
    active_selection_list = Column(Text)
    case_status = Column(postgresql.JSONB)
    case_meta = Column(postgresql.JSONB)
    case_role = Column(postgresql.JSONB)
    type_of_archiving = Column(Text)
    period_of_preservation = Column(Text)
    result_uuid = Column(GUID)
    result = Column(Text)
    result_description = Column(Text)
    result_explanation = Column(Text)
    result_selection_list_number = Column(Integer)
    result_process_type_number = Column(Integer)
    result_process_type_name = Column(Text)
    result_process_type_description = Column(Text)
    result_process_type_explanation = Column(Text)
    result_process_type_generic = Column(Text)
    result_origin = Column(Text)
    result_process_term = Column(Text)
    aggregation_scope = Column(Text)
    number_parent = Column(Integer)
    number_master = Column(Integer)
    number_previous = Column(Integer)
    price = Column(Text)
    result_process_type_object = Column(Text)
    custom_fields = Column(postgresql.JSONB)
    case_actions = Column(postgresql.JSONB)
    case_department = Column(postgresql.JSONB)
    case_subjects = Column(postgresql.JSONB)
    destructable = Column(Boolean)
    aanvrager_gm_id = Column(Integer)
    aanvrager_type = Column(Text)
    zaaktype_id = Column(Integer, nullable=False)
    zaaktype_node_id = Column(Integer, nullable=False)
    deleted = Column(UTCDateTime)
    suspension_rationale = Column(Text)
    premature_completion_rationale = Column(Text)
    days_left = Column(Integer)
    lead_time_real = Column(Integer)
    progress_days = Column(Text)
    html_email_template = Column(Text)
    file_custom_fields = Column(postgresql.JSONB)

    requestor_obj = Column(postgresql.JSONB)
    assignee_obj = Column(postgresql.JSONB)
    coordinator_obj = Column(postgresql.JSONB)

    case_type = Column(postgresql.JSONB)
    case_type_version = Column(postgresql.JSONB)


class BagCache(Base):
    __tablename__ = "bag_cache"
    id = Column(Integer, primary_key=True)
    bag_type = Column(Text)
    bag_id = Column(Text)
    bag_data = Column(postgresql.JSONB)
    date_created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)


class Dashboard(Base):
    __tablename__ = "dashboard"
    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)
    widgets = Column(JSON)
