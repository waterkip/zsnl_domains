# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import re
from collections import namedtuple
from minty.exceptions import Conflict

FileStoreSource = namedtuple(
    "FileStoreSource", ["case_uuid", "filestore_uuid", "subject"]
)


def windows_reserved_names():
    yield "con"
    yield "nul"
    yield "prn"
    yield "aux"

    for i in range(0, 10):
        yield f"lpt{i}"
        yield f"com{i}"


def clean_filename_part(filename: str):
    # Non-printable characters should never be in filenames
    if not filename.isprintable():
        raise Conflict(
            "File name contains invalid characters", "invalid_filename"
        )

    # Replace some known-problematic characters with "_"
    for invalid_char in '<>&":|?*':
        filename = filename.replace(invalid_char, "_")

    # Add an "_" if the file name is a reserved name on Windows

    if filename.startswith("."):
        if filename[1:].lower() in windows_reserved_names():
            filename = filename + "_"
    else:
        if filename.lower() in windows_reserved_names():
            filename = filename + "_"

    # Internet Explorer can be configured to send the full filename, including
    # the local path ("C:\Users\Username\Documents\foo.odt"); strip any included
    # path parts.
    filename = re.split(r"[\\/]", filename)[-1]

    return filename
