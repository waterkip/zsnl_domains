# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from sqlalchemy import sql, types
from sqlalchemy.dialects.postgresql import JSON
from uuid import UUID
from zsnl_domains.case_management.repositories import pip_acl
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import allowed_cases_subquery
from zsnl_domains.shared.util import escape_term_for_like, prepare_search_term

logger = logging.getLogger(__name__)


def get_message_by_uuid(message_uuid: UUID):
    stmt = _message_select_statement().where(
        schema.ThreadMessage.uuid == message_uuid
    )
    return stmt


def get_message_email_source_file_by_uuid(message_uuid: UUID):

    thread_message_external = sql.join(
        schema.ThreadMessage,
        schema.ThreadMessageExternal,
        sql.and_(
            schema.ThreadMessageExternal.id
            == schema.ThreadMessage.thread_message_external_id,
            schema.ThreadMessageExternal.type == "email",
        ),
        isouter=False,
    )

    thread_message_join = thread_message_external.join(
        schema.Thread,
        schema.ThreadMessage.thread_id == schema.Thread.id,
        isouter=True,
    )

    filestore_join = thread_message_join.join(
        schema.Filestore,
        sql.and_(
            schema.ThreadMessageExternal.source_file_id == schema.Filestore.id,
            schema.ThreadMessageExternal.type == "email",
        ),
        isouter=False,
    )

    case_join = filestore_join.join(
        schema.Case, schema.Thread.case_id == schema.Case.id, isouter=True
    )

    email_source_file_query = (
        sql.select(
            [
                schema.ThreadMessage.uuid,
                schema.Case.uuid.label("case_uuid"),
                schema.ThreadMessage.type,
                schema.ThreadMessage.created_by_displayname,
                schema.ThreadMessage.created,
                schema.ThreadMessage.last_modified,
                schema.ThreadMessage.message_date,
                schema.ThreadMessage.created_by_uuid,
                schema.ThreadMessageExternal.attachment_count,
                schema.ThreadMessageExternal.id.label("external_message_id"),
                schema.ThreadMessageExternal.subject.label(
                    "external_message_subject"
                ),
                schema.ThreadMessageExternal.type.label(
                    "external_message_type"
                ),
                schema.ThreadMessageExternal.participants,
                schema.ThreadMessageExternal.content.label(
                    "external_message_content"
                ),
                schema.Filestore.uuid.label(
                    "external_message_source_file_uuid"
                ),
                schema.Thread.case_id,
                attachment_subquery(schema.ThreadMessage.id).label(
                    "attachments"
                ),
            ]
        )
        .select_from(case_join)
        .where(schema.ThreadMessage.uuid == message_uuid)
    )

    return email_source_file_query


def attachment_subquery(message_id):
    preview = sql.alias(schema.Filestore)
    attachment = (
        sql.join(
            schema.Filestore,
            schema.ThreadMessageAttachment,
            schema.Filestore.id == schema.ThreadMessageAttachment.filestore_id,
        )
        .join(
            schema.ThreadMessageAttachmentDerivative,
            sql.and_(
                schema.ThreadMessageAttachmentDerivative.thread_message_attachment_id
                == schema.ThreadMessageAttachment.id,
                schema.ThreadMessageAttachmentDerivative.type == "pdf",
                schema.Filestore.mimetype != "application/pdf",
            ),
            isouter=True,
        )
        .join(
            preview,
            preview.c.id
            == schema.ThreadMessageAttachmentDerivative.filestore_id,
            isouter=True,
        )
    )

    attachment_json = (
        sql.select(
            [
                sql.func.json_build_object(
                    "uuid",
                    schema.ThreadMessageAttachment.uuid,
                    "file_uuid",
                    schema.Filestore.uuid,
                    "filename",
                    schema.ThreadMessageAttachment.filename,
                    "size",
                    schema.Filestore.size,
                    "mimetype",
                    schema.Filestore.mimetype,
                    "md5",
                    schema.Filestore.md5,
                    "date_created",
                    schema.Filestore.date_created,
                    "storage_location",
                    schema.Filestore.storage_location,
                    "preview_uuid",
                    preview.c.uuid,
                )
            ]
        )
        .select_from(attachment)
        .where(schema.ThreadMessageAttachment.thread_message_id == message_id)
        .label("attachment_json_subquery")
    )

    json_array = sql.func.array_to_json(sql.func.array(attachment_json))
    return json_array


def _message_select_statement():

    thread_and_case = sql.outerjoin(
        schema.Thread, schema.Case, schema.Thread.case_id == schema.Case.id
    )
    external_message_join = sql.join(
        schema.ThreadMessage,
        thread_and_case,
        schema.Thread.id == schema.ThreadMessage.thread_id,
    ).outerjoin(
        schema.ThreadMessageExternal,
        schema.ThreadMessage.thread_message_external_id
        == schema.ThreadMessageExternal.id,
    )
    select_stmt = sql.select(
        [
            schema.Thread.uuid.label("thread_uuid"),
            schema.Case.uuid.label("case_uuid"),
            schema.Case.id.label("case_id"),
            schema.ThreadMessage.uuid,
            schema.ThreadMessage.created,
            schema.ThreadMessage.last_modified,
            schema.ThreadMessage.message_date,
            schema.ThreadMessage.type,
            schema.ThreadMessageExternal.id.label("external_message_id"),
            schema.ThreadMessageExternal.content.label(
                "external_message_content"
            ),
            schema.ThreadMessageExternal.subject.label(
                "external_message_subject"
            ),
            schema.ThreadMessageExternal.direction,
            schema.ThreadMessageExternal.source_file_id,
            schema.ThreadMessageExternal.participants,
            schema.ThreadMessageExternal.read_employee,
            schema.ThreadMessageExternal.read_pip,
            schema.ThreadMessageExternal.attachment_count,
            schema.ThreadMessageExternal.type.label("external_message_type"),
            sql.cast(
                schema.ThreadMessageExternal.source_file_id.isnot(None),
                types.Boolean,
            ).label("is_imported"),
            schema.ThreadMessage.created_by_uuid,
            schema.ThreadMessage.created_by_displayname,
            attachment_subquery(schema.ThreadMessage.id).label("attachments"),
        ]
    ).select_from(external_message_join)

    return select_stmt


def document_query(db, user_uuid):
    document_join = (
        sql.join(
            schema.File,
            schema.Filestore,
            schema.File.filestore_id == schema.Filestore.id,
        )
        .join(
            schema.Directory,
            schema.File.directory_id == schema.Directory.id,
            isouter=True,
        )
        .join(
            schema.Case,
            sql.and_(
                schema.File.case_id == schema.Case.id,
                allowed_cases_subquery(db, user_uuid, "read"),
            ),
            isouter=True,
        )
        .join(
            schema.Group,
            schema.Group.id == schema.File.intake_group_id,
            isouter=True,
        )
        .join(
            schema.Role,
            schema.Role.id == schema.File.intake_role_id,
            isouter=True,
        )
    )

    return sql.select(
        [
            schema.File.uuid.label("document_uuid"),
            schema.Filestore.uuid.label("store_uuid"),
            schema.Filestore.mimetype,
            schema.Filestore.size,
            schema.Filestore.storage_location,
            schema.Filestore.md5,
            schema.Filestore.is_archivable,
            schema.Filestore.virus_scan_status,
            schema.Directory.uuid.label("directory_uuid"),
            schema.Case.uuid.label("case_uuid"),
            schema.Case.id.label("case_display_number"),
            schema.File.name.label("filename"),
            schema.File.extension,
            schema.File.accepted,
            schema.Group.uuid.label("intake_group_uuid"),
            schema.Role.uuid.label("intake_role_uuid"),
            sql.func.get_subject_by_legacy_id(schema.File.intake_owner).label(
                "intake_owner_uuid"
            ),
            sql.func.get_subject_by_legacy_id(schema.File.created_by).label(
                "creator_uuid"
            ),
            sql.func.get_subject_display_name_by_uuid(
                sql.func.get_subject_by_legacy_id(schema.File.created_by)
            ).label("creator_displayname"),
            schema.File.date_modified,
            schema.File.id,
            sql.expression.literal("document").label("type"),
        ]
    ).select_from(document_join)


def directory_entry_directory_query(
    db,
    user_uuid,
    directory_uuid=None,
    search_term=None,
    is_pip_user=False,
    no_empty_folders=True,
):
    """Get directory/folder for document tab as directory_entry row.
    Option to filter the results by parent_directory/folder and search_term.

    :param db: SQLAlchemy session to use for retrieval
    :type db: session
    :param user_uuid: UUID of logged in user
    :type user_uuid: UUID
    :param directory_uuid: UUID of the parent_directory/folder, defaults to None
    :type directory_uuid: UUID, optional
    :param search_term: Search term to search for directory, defaults to None
    :type search_term: str, optional
    :param is_pip_user: Boolean to indicate if the user is pip or not, defaults to False
    :type is_pip_user: bool, optional
    :param no_empty_folders: Boolean to filter out empty folders.
    :type no_empty_folders: bool, optional
    :return: Query to get directory as directory_entry row.
    :rtype: sqlalchemy.sql.expression.Select
    """
    parent_directory = sql.alias(schema.Directory)

    # The parent directory is always the last one in the "path" field, so we join on that to retrieve the parent directory if it exists.

    directory_join = sql.join(
        schema.Directory,
        parent_directory,
        schema.Directory.path[sql.func.array_upper(schema.Directory.path, 1)]
        == parent_directory.c.id,
        isouter=True,
    ).join(
        schema.Case,
        sql.and_(schema.Case.id == schema.Directory.case_id),
        isouter=True,
    )

    qry_stmt = sql.select(
        [
            schema.Directory.uuid.label("uuid"),
            schema.Directory.name.label("name"),
            sql.expression.literal("directory").label("type"),
            sql.expression.literal(None).label("description"),
            sql.expression.literal(None).label("extension"),
            sql.expression.literal(None).label("mimetype"),
            sql.expression.literal(None).label("accepted"),
            sql.expression.literal(None).label("last_modified_date_time"),
            sql.expression.literal(None).label("document_number"),
            schema.Case.uuid.label("case_uuid"),
            sql.expression.literal(None).label("rejection_reason"),
            sql.expression.literal(None).label("rejected_by_display_name"),
            sql.expression.literal(None).label("intake_owner"),
            sql.expression.literal(None).label("intake_role_id"),
            sql.expression.literal(None).label("intake_group_id"),
            sql.expression.literal(None).label("intake_group_name"),
            sql.expression.literal(None).label("intake_role_name"),
            sql.expression.literal(None).label("intake_owner_display_name"),
            schema.Directory.case_id.label("case_display_number"),
            parent_directory.c.uuid.label("parent_directory_uuid"),
            parent_directory.c.name.label("parent_directory_display_name"),
            sql.expression.literal(None).label("modified_by_uuid"),
            sql.expression.literal(None).label("modified_by_display_name"),
            sql.expression.literal(None).label("preview_uuid"),
            sql.expression.literal(None).label("thumbnail_uuid"),
            sql.expression.literal(None).label("thumbnail_mimetype"),
        ]
    ).select_from(directory_join)

    if is_pip_user:
        qry_stmt = qry_stmt.where(
            pip_acl.allowed_cases_subquery(user_uuid=user_uuid)
        )
    else:
        qry_stmt = qry_stmt.where(
            allowed_cases_subquery(db, user_uuid, "read")
        )

    if directory_uuid:
        directory_uuid = None if directory_uuid == "\0" else directory_uuid
        qry_stmt = qry_stmt.where(parent_directory.c.uuid == directory_uuid)

    if search_term:
        qry_stmt = qry_stmt.where(
            schema.Directory.name.ilike(
                "%" + escape_term_for_like(search_term) + "%", escape="~"
            )
        )

    if no_empty_folders and is_pip_user:
        recursive_cte_query = (
            sql.select(
                [
                    schema.Directory.id,
                    schema.Directory.path,
                    sql.literal(1).label("depth"),
                ]
            )
            .where(
                sql.and_(
                    schema.Directory.id == schema.File.directory_id,
                    schema.File.publish_pip.is_(True),
                )
            )
            .cte(recursive=True, name="cte")
        )

        directory_alias = sql.alias(schema.Directory)
        recursive_cte = recursive_cte_query.union_all(
            sql.select(
                [
                    directory_alias.c.id,
                    directory_alias.c.path,
                    recursive_cte_query.c.depth + 1,
                ]
            ).where(
                sql.and_(
                    directory_alias.c.id
                    == sql.func.any(recursive_cte_query.c.path),
                    recursive_cte_query.c.depth <= 20,
                )
            )
        )

        directory_ids = sql.select([recursive_cte.c.id])
        qry_stmt = qry_stmt.where(schema.Directory.id.in_(directory_ids))

    return qry_stmt


def directory_entry_document_query(
    db,
    user_uuid,
    directory_uuid=None,
    search_term=None,
    is_pip_user=False,
    is_intake=False,
):
    """Get documents for document tab or document intake as directory_entry row.
    Option to filter the results by parent_directory/folder and search_term.

    :param db: SQLAlchemy Session for retrieval.
    :type db: session
    :param user_uuid: UUID of logged in user.
    :type user_uuid: UUID
    :param directory_uuid: UUID of the parent_directory/folder, defaults to None
    :type directory_uuid: UUID, optional
    :param search_term: Search term, defaults to None
    :type search_term: str, optional
    :param is_pip_user: Boolean to indicate if the logged in user is pip or not., defaults to False
    :type is_pip_user: bool, optional
    :return: Query to get documents for document tab ad directory_entry row.
    :rtype: sqlalchemy.sql.expression.Select
    """
    preview_derivative = sql.alias(schema.FileDerivative)
    preview_store = sql.alias(schema.Filestore)
    preview_uuid_query = (
        sql.select([preview_store.c.uuid])
        .where(
            sql.and_(
                preview_derivative.c.file_id == schema.File.id,
                preview_derivative.c.type == "pdf",
                schema.Filestore.mimetype != "application/pdf",
                preview_store.c.id == preview_derivative.c.filestore_id,
            )
        )
        .limit(1)
    )

    thumbnail_derivative = sql.alias(schema.FileDerivative)
    thumbnail_store = sql.alias(schema.Filestore)
    thumbnail_filter = sql.and_(
        thumbnail_derivative.c.file_id == schema.File.id,
        thumbnail_derivative.c.type == "thumbnail",
        thumbnail_store.c.id == thumbnail_derivative.c.filestore_id,
    )

    thumbnail_uuid_query = (
        sql.select([thumbnail_store.c.uuid]).where(thumbnail_filter).limit(1)
    )
    thumbnail_mimetype_query = (
        sql.select([thumbnail_store.c.mimetype])
        .where(thumbnail_filter)
        .limit(1)
    )

    document_join = (
        sql.join(
            schema.File,
            schema.Filestore,
            schema.File.filestore_id == schema.Filestore.id,
        )
        .join(
            schema.Group,
            schema.File.intake_group_id == schema.Group.id,
            isouter=True,
        )
        .join(
            schema.Role,
            schema.File.intake_role_id == schema.Role.id,
            isouter=True,
        )
        .join(
            schema.Directory,
            schema.File.directory_id == schema.Directory.id,
            isouter=True,
        )
        .join(
            schema.Case,
            sql.and_(schema.File.case_id == schema.Case.id),
            isouter=True,
        )
        .join(
            schema.FileMetaData,
            sql.and_(schema.File.metadata_id == schema.FileMetaData.id),
            isouter=True,
        )
    )

    qry_stmt = (
        sql.select(
            [
                schema.File.uuid.label("uuid"),
                (schema.File.name).label("name"),
                sql.expression.literal("document").label("type"),
                schema.FileMetaData.description.label("description"),
                schema.File.extension.label("extension"),
                schema.Filestore.mimetype.label("mimetype"),
                schema.File.accepted.label("accepted"),
                schema.File.date_modified.label("last_modified_date_time"),
                schema.File.id.label("document_number"),
                schema.Case.uuid.label("case_uuid"),
                schema.File.rejection_reason.label("rejection_reason"),
                schema.File.rejected_by_display_name.label(
                    "rejected_by_display_name"
                ),
                schema.File.intake_owner.label("intake_owner"),
                schema.Role.id.label("intake_role_id"),
                schema.Group.id.label("intake_group_id"),
                schema.Group.name.label("intake_group_name"),
                schema.Role.name.label("intake_role_name"),
                sql.func.get_subject_display_name_by_uuid(
                    sql.func.get_subject_by_legacy_id(schema.File.intake_owner)
                ).label("intake_owner_display_name"),
                schema.Case.id.label("case_display_number"),
                schema.Directory.uuid.label("parent_directory_uuid"),
                schema.Directory.name.label("parent_directory_display_name"),
                sql.func.get_subject_by_legacy_id(
                    schema.File.modified_by
                ).label("modified_by_uuid"),
                sql.func.get_subject_display_name_by_uuid(
                    sql.func.get_subject_by_legacy_id(schema.File.modified_by)
                ).label("modified_by_display_name"),
                preview_uuid_query.label("preview_uuid"),
                thumbnail_uuid_query.label("thumbnail_uuid"),
                thumbnail_mimetype_query.label("thumbnail_mimetype"),
            ]
        )
        .select_from(document_join)
        .where(
            sql.and_(
                schema.File.active_version.is_(True),
                schema.File.date_deleted.is_(None),
                schema.File.destroyed.is_(False),
            )
        )
    )

    if is_pip_user:
        qry_stmt = qry_stmt.where(
            sql.and_(
                pip_acl.allowed_cases_subquery(user_uuid=user_uuid),
                schema.File.publish_pip.is_(True),
            )
        )
    elif is_intake:
        pass
    else:
        qry_stmt = qry_stmt.where(
            allowed_cases_subquery(db, user_uuid, "read")
        )

    if directory_uuid:
        directory_uuid = None if directory_uuid == "\0" else directory_uuid
        qry_stmt = qry_stmt.where(schema.Directory.uuid == directory_uuid)

    if search_term:
        exact_filename_match = (
            schema.File.name + schema.File.extension
        ).ilike("%" + escape_term_for_like(search_term) + "%", escape="~")

        search_index_match = schema.File.search_index.match(
            prepare_search_term(search_term=search_term)
        )

        qry_stmt = qry_stmt.where(
            sql.or_(exact_filename_match, search_index_match)
        )
    return qry_stmt


def parent_directories_query():
    """Get parents for a directory as list of directories.

    :return: Query to get parents for a directory.
    :rtype: sqlalchemy.sql.expression.Select
    """
    parents = sql.alias(schema.Directory)
    parent_of_parent = sql.alias(schema.Directory)

    # The parents of the directory is always in the "path" field, so we join the path to retrieve the parents.
    # The parent of a parent is always the last one in the "path" field of parent, so we join on that to retrieve the parent_of_parent.

    join = sql.join(
        schema.Directory,
        parents,
        parents.c.id == sql.func.any(schema.Directory.path),
        isouter=False,
    ).join(
        parent_of_parent,
        parent_of_parent.c.id
        == parents.c.path[sql.func.array_upper(parents.c.path, 1)],
        isouter=True,
    )

    return sql.select(
        [
            parents.c.id,
            parents.c.case_id,
            parents.c.path,
            parents.c.uuid,
            parents.c.name,
            sql.func.json_build_object("uuid", parent_of_parent.c.uuid).label(
                "parent"
            ),
        ]
    ).select_from(join)


def directory_query():
    """Get details for a directory.

    :return: Query to get directory.
    :rtype: sqlalchemy.sql.expression.Select
    """
    parent_directory = sql.alias(schema.Directory)

    # The parent directory is always the last one in the "path" field, so we join on that to retrieve the parent directory if it exists.

    join = sql.join(
        schema.Directory,
        parent_directory,
        parent_directory.c.id
        == schema.Directory.path[
            sql.func.array_upper(schema.Directory.path, 1)
        ],
        isouter=True,
    )
    return sql.select(
        [
            schema.Directory.id,
            schema.Directory.case_id,
            schema.Directory.path,
            schema.Directory.uuid,
            schema.Directory.name,
            sql.func.json_build_object("uuid", parent_directory.c.uuid).label(
                "parent"
            ),
        ]
    ).select_from(join)


def contact_query(contact_uuid: UUID):
    """Search from 3 tables(Bedrijf, NatuurlijkPersoon, Subject)
     and combine them with a "UNION ALL" where in the normal case one or the other table will return a row.
     Otherwise both tables where the uuid searching for,
     exists in both these tables, in that case, first row is fetched.

    :param contact_uuid: The uuid keyword to retrieve a contact.
    :type contact_uuid: string
    :return: SQLAlchemy query for one Contact row
    """

    natural_person_simple_query = sql.select(
        [
            schema.NatuurlijkPersoon.id,
            schema.NatuurlijkPersoon.uuid,
            sql.func.concat(
                schema.NatuurlijkPersoon.voorletters + " ",
                schema.NatuurlijkPersoon.naamgebruik,
            ).label("name"),
            sql.literal_column("'person'").label("type"),
        ]
    )

    organization_simple_query = sql.select(
        [
            schema.Bedrijf.id,
            schema.Bedrijf.uuid,
            schema.Bedrijf.handelsnaam.label("name"),
            sql.literal_column("'organization'").label("type"),
        ]
    )

    employee_simple_query = sql.select(
        [
            schema.Subject.id,
            schema.Subject.uuid,
            sql.cast(schema.Subject.properties, JSON)[
                "displayname"
            ].astext.label("name"),
            sql.literal_column("'employee'").label("type"),
        ]
    )

    natural_person = natural_person_simple_query.where(
        sql.and_(schema.NatuurlijkPersoon.uuid == contact_uuid)
    )

    organization = organization_simple_query.where(
        sql.and_(schema.Bedrijf.uuid == contact_uuid)
    )

    employee = employee_simple_query.where(
        sql.and_(
            schema.Subject.uuid == contact_uuid,
            schema.Subject.subject_type == "employee",
        )
    )
    return sql.union_all(natural_person, organization, employee)
