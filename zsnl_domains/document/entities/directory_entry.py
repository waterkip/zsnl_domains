# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import EntityBase


class DirectoryEntry(EntityBase):
    """
    DirectoryEntry class

    uuid: UUID: Directory entry uuid (eg: File.uuid)
    type: str: the type (eg: "directory_entry" or "document")
    name: str: The directory entry name (eg: File.name + File.extension)
    entry_type: str: the type (eg: "directory_entry" or "document")
    description: str: The description (eg: FileMetaData.description)
    extension: str: The extension of the document (eg: File.extension)
    mimetype: str: The mimetype: (eg: Filestore.mimetype)
    accpeted: Boolean: is the file accepted by the gebruiker (eg: File.accepted)
    last_modified_date_time: This is the datetime when this *version* of the document has last
                             been modified, thus the *content* of the document has been altered.
                             The modification of the version is retrieved by File.date_created
    directory: The directory uuid or None (eg: File.uuid if entry_type == directory else None)
    document: dict({uuid: UUID, preview_available: bool}) if directory_entry is document else None
              eg: {
                 "uuid": File.uuid,
                 "preview_available": True if mimetype == "application/pdf"
              } if entry_type = document else None

    case: dict({uuid: UUID, display_number: int})
          eg: {
            "uuid": Case.uuid,
            "display_number": Case.id,
          }

    parent: dict({uuid: directory_uuid, display_name: name})
            eg: {
                "uuid": Directory.uuid,
                "display_name": Directory.name,
            }
    modified_by: dict({"uuid", ...  "display_name": ...})
    assignment: dict({
                "employee": {"name": ...},
                "role": {"name": ...},
                "department": {"name": ...})
    """

    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid,
        type="directory_entry",
        name=None,
        entry_type=None,
        description=None,
        extension=None,
        mimetype=None,
        accepted=False,
        document_number=None,
        last_modified_date_time=None,
        directory=None,
        document=None,
        case=None,
        parent=None,
        modified_by=None,
        rejection_reason=None,
        rejected_by_display_name=None,
        assignment=None,
    ):
        self.uuid = uuid
        self.type = type
        self.name = name
        self.entry_type = entry_type
        self.description = description
        self.extension = extension
        self.mimetype = mimetype
        self.accepted = accepted
        self.document_number = document_number
        self.last_modified_date_time = last_modified_date_time
        self.directory = directory
        self.document = document
        self.case = case
        self.parent = parent
        self.modified_by = modified_by
        self.rejection_reason = rejection_reason
        self.rejected_by_display_name = rejected_by_display_name
        self.assignment = assignment
