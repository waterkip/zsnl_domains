# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import http.client
import json
import urllib
import urllib.error
import urllib.request
from minty import Base, exceptions
from typing import Optional


class ConverterWrapper(Base):
    def __init__(self, base_url):
        self.base_url = base_url

    def convert(
        self,
        to_type: str,
        content: bytes,
        options: Optional[dict[str, str]] = None,
    ):
        """Convert a file from one mimetype to other.

        Generally, files can only be converted to "similar" formats:
        images to images, documents to documents, etc. An exception to this
        is PDF: most formats can be converted to PDF, and PDF can be turned
        into PNG.

        :param to_type: The mime type to convert to.
        :type to_type: str
        :param content: The content of the file to convert.
        :type content: bytes
        :param options: A JSON object containing specific options for the conversion, defaults to {}
        :type options: dict, optional
        :rtype: content of converted file
        """

        # content of file should be base64 encoded to pass to converter service.
        base64_encoded_content = base64.b64encode(content)

        values = {
            "to_type": to_type,
            "content": base64_encoded_content.decode("utf-8"),
            "options": options or {},
        }

        # urllib.request supports only bytes to send as POST data.
        data = bytes(json.dumps(values), encoding="utf-8")

        url = self.base_url + "/v1/convert"
        req = urllib.request.Request(
            url, data, {"Content-Type": "application/json"}
        )
        try:
            response = urllib.request.urlopen(req, timeout=60).read()
        except (
            urllib.error.HTTPError,
            urllib.error.URLError,
            http.client.HTTPException,
            OSError,
        ) as e:
            raise exceptions.Conflict(
                "Error during document conversion"
            ) from e

        content = json.loads(response)["content"]
        # decoding the response from converter service.
        base64_decoded_content = base64.b64decode(content)
        return base64_decoded_content


class ConverterInfrastructure(Base):
    """Infrastructure for file conversion"""

    def __call__(self, config):
        """Initialize ConverterWrapper"""
        return ConverterWrapper(base_url=config["converter_base_url"])
