# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from sqlalchemy import sql
from zsnl_domains.database import schema


def custom_fields_query(case_alias=None):
    if case_alias is None:
        case_alias = schema.Case

    return sql.func.array(
        sql.select(
            [
                sql.func.json_build_object(
                    "name",
                    schema.BibliotheekKenmerk.naam,
                    "magic_string",
                    schema.BibliotheekKenmerk.magic_string,
                    "value",
                    schema.ZaakKenmerk.value,
                    "type",
                    schema.BibliotheekKenmerk.value_type,
                )
            ]
        )
        .select_from(
            sql.join(
                schema.ZaakKenmerk,
                schema.BibliotheekKenmerk,
                schema.ZaakKenmerk.bibliotheek_kenmerken_id
                == schema.BibliotheekKenmerk.id,
            )
        )
        .where(schema.ZaakKenmerk.zaak_id == case_alias.id)
        .scalar_subquery()
        .label("custom_fields")
    )


def file_custom_fields_query(case_alias):
    if case_alias is None:
        case_alias = schema.Case

    return sql.func.array(
        sql.select(
            [
                sql.func.json_build_object(
                    "name",
                    schema.BibliotheekKenmerk.naam,
                    "magic_string",
                    schema.BibliotheekKenmerk.magic_string,
                    "type",
                    schema.BibliotheekKenmerk.value_type,
                    "value",
                    sql.func.array(
                        sql.select(
                            sql.func.json_build_object(
                                "md5",
                                schema.Filestore.md5,
                                "size",
                                schema.Filestore.size,
                                "uuid",
                                schema.Filestore.uuid,
                                "filename",
                                schema.File.name + schema.File.extension,
                                "mimetype",
                                schema.Filestore.mimetype,
                                "is_archivable",
                                schema.Filestore.is_archivable,
                                "original_name",
                                schema.Filestore.original_name,
                                "thumbnail_uuid",
                                schema.Filestore.thumbnail_uuid,
                            ),
                        )
                        .select_from(
                            sql.join(
                                schema.FileCaseDocument,
                                schema.File,
                                sql.and_(
                                    schema.FileCaseDocument.file_id
                                    == schema.File.id,
                                    schema.FileCaseDocument.case_id
                                    == case_alias.id,
                                ),
                            ).join(
                                schema.Filestore,
                                schema.File.filestore_id
                                == schema.Filestore.id,
                            )
                        )
                        .where(
                            schema.FileCaseDocument.bibliotheek_kenmerken_id
                            == schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id,
                        )
                        .scalar_subquery()
                    ),
                )
            ]
        )
        .select_from(
            sql.join(
                schema.ZaaktypeKenmerk,
                schema.BibliotheekKenmerk,
                sql.and_(
                    schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id
                    == schema.BibliotheekKenmerk.id,
                    schema.BibliotheekKenmerk.value_type == "file",
                ),
            ).join(
                schema.ZaaktypeNode,
                schema.ZaaktypeNode.id
                == schema.ZaaktypeKenmerk.zaaktype_node_id,
            )
        )
        .where(
            schema.ZaaktypeNode.id == case_alias.zaaktype_node_id,
        )
        .scalar_subquery()
        .label("file_custom_fields")
    )
