# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic import Field
from zsnl_domains.shared.custom_field.types import base as typebase


class CustomFieldTypeText(typebase.CustomFieldValueBase):
    type: str = Field("text", title="A value formatted as text")


class CustomFieldTypeOption(typebase.CustomFieldValueBase):
    type: str = Field("option", title="A value formatted as text")

    @classmethod
    def get_validators(cls, custom_field):
        return {"option_validator": cls._get_option_validator(custom_field)}


class CustomFieldTypeSelect(typebase.CustomFieldValueBase):
    type: str = Field("select", title="A value formatted as text")

    @classmethod
    def get_validators(cls, custom_field):
        return {"option_validator": cls._get_option_validator(custom_field)}


class CustomFieldTypeTextarea(typebase.CustomFieldValueBase):
    type: str = Field(
        "textarea", title="A value formatted as a large portion of text"
    )


class CustomFieldTypeRichText(typebase.CustomFieldValueBase):
    type: str = Field("richtext", title="A value of WYSIWYG formatted text")


class CustomFieldNumeric(typebase.CustomFieldValueBase):
    type: str = Field("numeric", title="A value formatted as number")
