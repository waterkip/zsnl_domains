# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import contextvars

rule_engine = contextvars.ContextVar("rule_engine")
