# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from .. import repositories
from ..entities import saved_search as entity
from pydantic import Field, validate_arguments
from typing import Annotated, List, Optional, Union, cast
from uuid import UUID


def _has_authorization(
    saved_search: entity.SavedSearch, authorization: entity.AuthorizationLevel
):
    if saved_search.entity_meta_authorizations and (
        authorization in saved_search.entity_meta_authorizations
    ):
        return True

    return False


class CreateSavedSearch(minty.cqrs.SplitCommandBase):
    name = "create_saved_search"

    @validate_arguments
    def __call__(
        self,
        uuid: UUID,
        name: str,
        kind: entity.SavedSearchKind,
        filters: Annotated[
            Union[
                entity.CustomObjectSearchFilterDefinition,
                entity.CaseSearchFilterDefinition,
            ],
            Field(discriminator="filter_type"),
        ],
        permissions: List[entity.SavedSearchPermissionDefinition],
        columns: List[entity.SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: entity.SortOrder,
    ):
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        repo.create(
            uuid=uuid,
            name=name,
            kind=kind,
            owner=self.cmd.user_info.user_uuid,
            filters=filters,
            permissions=permissions,
            columns=columns,
            sort_column=sort_column,
            sort_order=sort_order,
        )
        repo.save()


class DeleteSavedSearch(minty.cqrs.SplitCommandBase):
    name = "delete_saved_search"

    @validate_arguments
    def __call__(self, uuid: UUID):
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        saved_search = repo.get(
            uuid=uuid,
            user_info=self.cmd.user_info,
        )

        if not _has_authorization(
            saved_search, entity.AuthorizationLevel.admin
        ):

            raise minty.exceptions.Forbidden(
                "Current user is not authorised to delete this saved search",
                "saved_search/delete/not_authorized",
            )

        saved_search.delete()

        repo.save()


class UpdateSavedSearch(minty.cqrs.SplitCommandBase):
    name = "update_saved_search"

    @validate_arguments
    def __call__(
        self,
        uuid: UUID,
        name: str,
        filters: Union[
            entity.CustomObjectSearchFilterDefinition,
            entity.CaseSearchFilterDefinition,
        ],
        columns: List[entity.SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: entity.SortOrder,
        permissions: Optional[
            List[entity.SavedSearchPermissionDefinition]
        ] = None,
    ):
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        saved_search = repo.get(
            uuid=uuid,
            user_info=self.cmd.user_info,
        )

        if not _has_authorization(
            saved_search, entity.AuthorizationLevel.readwrite
        ):
            raise minty.exceptions.Forbidden(
                "Current user does not have write permission for this saved search",
                "saved_search/update/not_authorized",
            )

        saved_search.update(
            name=name,
            filters=filters,
            columns=columns,
            sort_column=sort_column,
            sort_order=sort_order,
        )

        if permissions is not None:
            if not _has_authorization(
                saved_search, entity.AuthorizationLevel.admin
            ):
                raise minty.exceptions.Forbidden(
                    "Current user is not authorised to update permissions of this saved search",
                    "saved_search/update/permissions/not_authorized",
                )

            saved_search.update_permissions(permissions=permissions)

        repo.save()
