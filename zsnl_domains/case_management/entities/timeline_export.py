# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import TimelineEntry
from minty.entity import Entity, ValueObject
from pydantic import Field
from typing import List, Optional
from uuid import UUID


class OutputUser(ValueObject):
    user_uuid: UUID = Field(..., title="UUID of output user")
    permissions: dict = Field(..., title="Permssions of output user")


class TimelineExport(Entity):
    """Entity represents timeline export"""

    entity_type = "timeline_export"
    entity_id__fields = ["uuid"]

    uuid: Optional[UUID] = Field(
        None, title="UUID of the object which you want to export timeline"
    )
    type: Optional[str] = Field(
        None, title="Type of the the object which you want to export timeline"
    )
    entries: Optional[List[TimelineEntry]] = Field(
        None, title="List of timeline entries you want to import"
    )
    output_user: Optional[OutputUser] = Field(
        None, title="User who is trying to export the timeline"
    )
    upload_result: Optional[dict] = Field(
        None, title="Dictionary containing details of upload result"
    )
    filestore_uuid: Optional[UUID] = Field(None, title="UUID of stored file")

    period_start: Optional[str] = Field(
        None, title="Start time filter for the timeline"
    )
    period_end: Optional[str] = Field(
        None, title="End time filter for the timeline"
    )

    upload_result: Optional[dict] = Field(
        None, title="Dictionary containing details of upload result"
    )
    filestore_uuid: Optional[UUID] = Field(None, title="UUID of stored file")

    @classmethod
    @Entity.event(name="TimelineExportRequested", fire_always=True)
    def request(cls, **kwargs):
        return cls(**kwargs)

    @classmethod
    @Entity.event(name="TimelineExportCreated", fire_always=True)
    def create(cls, **kwargs):
        return cls(**kwargs)
