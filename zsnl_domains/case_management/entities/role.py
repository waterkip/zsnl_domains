# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .department import DepartmentSummary
from minty.entity import Entity
from pydantic import Field
from typing import Optional
from uuid import UUID


class Role(Entity):
    entity_type = "role"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier for this role")
    name: str = Field(..., title="Name of the role")

    entity_relationships = ["parent"]
    description: str = Field("", title="Longer description of the role")

    parent: Optional[DepartmentSummary] = Field(
        None, title="Department this role is part of"
    )
