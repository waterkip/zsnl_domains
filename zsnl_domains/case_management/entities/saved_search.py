# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import enum
import minty.cqrs
from ._shared import AuthorizationLevel
from datetime import date, datetime
from minty import entity
from pydantic import Field, conint, constr
from pydantic.generics import GenericModel
from typing import (
    Annotated,
    Generic,
    List,
    Literal,
    Optional,
    Set,
    Tuple,
    TypeVar,
    Union,
)
from uuid import UUID
from zsnl_domains.case_management.entities.custom_object import (
    ValidArchiveStatus,
    ValidObjectStatus,
)
from zsnl_domains.shared import types
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidContactChannel,
)

ISO8601_PERIOD_REGEX = (
    r"^([?+-])?"
    r"P(?!\b)"
    r"([?0-9]+([,.][0-9]+)?Y)s?"
    r"([?0-9]+([,.][0-9]+)?M)?"
    r"([?0-9]+([,.][0-9]+)?W)?"
    r"([?0-9]+([,.][0-9]+)?D)?"
    r"((T)([?0-9]+([,.][0-9]+)?H)?"
    r"([?0-9]+([,.][0-9]+)?M)?"
    r"([?0-9]+([,.][0-9]+)?S)?)?$"
)

SimpleFilterType = TypeVar("SimpleFilterType")
MinZeroInteger = conint(ge=0)


class SimpleFilterObject(GenericModel, Generic[SimpleFilterType]):
    label: str = Field(
        ..., title="Human-readable preview value of the filter value"
    )
    value: SimpleFilterType = Field(..., title="Value to filter for")

    class Config:
        @staticmethod
        def schema_extra(schema):
            for prop in schema.get("properties", {}).values():
                if (
                    "type" in prop
                    and prop["type"] == "array"
                    and "items" in schema["properties"]["value"]["items"]
                ):
                    updated_array = {
                        "oneOf": schema["properties"]["value"]["items"][
                            "items"
                        ]
                    }
                    schema["properties"]["value"]["items"][
                        "items"
                    ] = updated_array


class FilterDefinitionBase(entity.ValueObject):
    keyword: Optional[str]

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


CustomISODuration = constr(regex=ISO8601_PERIOD_REGEX)


class AbsoluteDate(entity.ValueObject):
    type: Literal["absolute"]
    value: Optional[datetime]
    operator: types.ComparisonFilterOperator


class RelativeDate(entity.ValueObject):
    type: Literal["relative"]
    value: CustomISODuration
    operator: types.ComparisonFilterOperator


class RangeDate(entity.ValueObject):
    type: Literal["range"]
    start_value: datetime
    end_value: datetime
    time_set_by_user: bool


class CustomObjectSearchFilterDefinition(FilterDefinitionBase):
    "Filter definition for saved searches for custom objects"
    filter_type: Literal["custom_object"]

    custom_object_type: SimpleFilterObject[UUID] = Field(
        ..., alias="relationship.custom_object_type"
    )
    status: Optional[SimpleFilterObject[ValidObjectStatus]] = Field(
        alias="attributes.status"
    )
    archive_status: Optional[SimpleFilterObject[ValidArchiveStatus]] = Field(
        alias="attributes.archive_status"
    )
    last_modified: Optional[
        SimpleFilterObject[
            List[
                Annotated[
                    Union[
                        AbsoluteDate,
                        RangeDate,
                        RelativeDate,
                    ],
                    Field(discriminator="type"),
                ],
            ]
        ]
    ] = Field(alias="attributes.last_modified")


class CaseSearchFilterDefinition(FilterDefinitionBase):
    "Filter definition for saved searches for cases"
    filter_type: Literal["case"]
    status: Optional[SimpleFilterObject[ValidCaseStatus]] = Field(
        alias="attributes.status"
    )
    case_type_uuids: Optional[SimpleFilterObject[UUID]] = Field(
        alias="relationship.case_type.id"
    )
    assignee_uuids: Optional[SimpleFilterObject[UUID]] = Field(
        alias="relationship.assignee.id"
    )
    coordinator_uuids: Optional[SimpleFilterObject[UUID]] = Field(
        alias="relationship.coordinator.id"
    )
    requestor_uuids: Optional[SimpleFilterObject[UUID]] = Field(
        alias="relationship.requestor.id"
    )
    filter_registration_date: Optional[
        SimpleFilterObject[List[Tuple[types.ComparisonFilterOperator, date]]]
    ] = Field(alias="attributes.registration_date")
    filter_completion_date: Optional[
        SimpleFilterObject[List[Tuple[types.ComparisonFilterOperator, date]]]
    ] = Field(alias="attributes.completion_date")
    filter_payment_status: Optional[
        SimpleFilterObject[ValidCasePaymentStatus]
    ] = Field(alias="attributes.payment_status")
    filter_channel_of_contact: Optional[
        SimpleFilterObject[ValidContactChannel]
    ] = Field(alias="attributes.channel_of_contact")
    filter_confidentiality: Optional[
        SimpleFilterObject[ValidCaseConfidentiality]
    ] = Field(alias="attributes.confidentiality")
    filter_archival_state: Optional[
        SimpleFilterObject[ValidCaseArchivalState]
    ] = Field(alias="attributes.archival_state")
    filter_retention_period_source_date: Optional[
        SimpleFilterObject[ValidCaseRetentionPeriodSourceDate]
    ] = Field(alias="attributes.retention_period_source_date")
    filter_result: Optional[SimpleFilterObject[ValidCaseResult]] = Field(
        alias="attributes.result"
    )
    filter_case_location: Optional[SimpleFilterObject[str]] = Field(
        alias="attributes.case_location"
    )
    filter_num_unread_messages: Optional[
        SimpleFilterObject[
            List[Tuple[types.ComparisonFilterOperator, MinZeroInteger]]
        ]
    ] = Field(alias="attributes.num_unread_messages")
    filter_num_unaccepted_files: Optional[
        SimpleFilterObject[
            List[Tuple[types.ComparisonFilterOperator, MinZeroInteger]]
        ]
    ] = Field(alias="attributes.num_unaccepted_files")
    filter_num_unaccepted_updates: Optional[
        SimpleFilterObject[
            List[Tuple[types.ComparisonFilterOperator, MinZeroInteger]]
        ]
    ] = Field(alias="attributes.num_unaccepted_updates")
    filter_period_of_preservation_active: Optional[
        SimpleFilterObject[bool]
    ] = Field(alias="attributes.period_of_preservation_active")
    filter_subject: Optional[SimpleFilterObject[str]] = Field(
        alias="attributes.subject"
    )


class SavedSearchPermission(str, enum.Enum):
    """Permissions a user can give to a group/role on a saved search."""

    read = "read"
    write = "write"


class SortOrder(str, enum.Enum):
    asc = "asc"
    desc = "desc"


class SavedSearchKind(str, enum.Enum):
    case = "case"
    custom_object = "custom_object"


class SavedSearchPermissionDefinition(entity.ValueObject):
    group_id: UUID = Field(
        ..., title="Identifier of the group this permission applies to"
    )
    role_id: UUID = Field(
        ..., title="Identifier of the role this permission applies to"
    )
    permission: Set[SavedSearchPermission] = Field(
        ...,
        title="Permissions the specified group + role has for this saved search",
    )


class SavedSearchColumnDefinition(entity.ValueObject):
    source: List[str] = Field(
        ...,
        title="Path to retrieve the field from the case or custom_object entity",
    )
    label: str = Field(..., title="Human-readable name for this column")
    type: str = Field(
        ...,
        title="Type of the field; can be used to determine how to show the 'raw' value",
    )


class SavedSearchOwner(entity.Entity):
    entity_type = "employee"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class SavedSearch(entity.Entity):
    """
    Represents a saved state of the "advanced search" screen.

    Users can create (and save) multiple search queries, for quick access
    while working with the system.
    """

    entity_type = "saved_search"
    entity_id__fields = ["uuid"]
    entity_relationships = ["owner"]
    entity_meta__fields = ["entity_meta_summary", "entity_meta_authorizations"]

    entity_meta_authorizations: Optional[Set[AuthorizationLevel]] = Field(
        None,
        title="The set of rights/authorizations the current user has for the saved search",
    )

    uuid: UUID = Field(..., title="Identifier for this saved search")
    name: str = Field(..., title="Unique name of this search")

    owner: SavedSearchOwner = Field(..., title="Owner of this saved search")

    kind: SavedSearchKind = Field(
        ..., title="The kind of entity this saved search searches for"
    )

    filters: Annotated[
        Union[
            CustomObjectSearchFilterDefinition,
            CaseSearchFilterDefinition,
        ],
        Field(discriminator="filter_type"),
    ]
    permissions: List[SavedSearchPermissionDefinition] = Field(
        ..., title="Group/role/permission"
    )

    columns: List[SavedSearchColumnDefinition] = Field(
        ..., title="Columns visible when using this search"
    )
    sort_column: str = Field(
        ..., title="Column to sort the result on, when using this search"
    )
    sort_order: SortOrder = Field(
        ..., title="Order for sorting results (ascending or descending)"
    )
    date_deleted: Optional[date] = Field(
        None, title="Date the saved search is deleted"
    )

    @classmethod
    @entity.Entity.event(name="SavedSearchCreated", fire_always=True)
    def create(
        cls,
        uuid: UUID,
        name: str,
        kind: SavedSearchKind,
        owner: SavedSearchOwner,
        filters: Union[
            CustomObjectSearchFilterDefinition,
            CaseSearchFilterDefinition,
        ],
        permissions: List[SavedSearchPermissionDefinition],
        columns: List[SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: SortOrder,
        event_service: minty.cqrs.EventService,
    ):
        return cls.parse_obj(
            {
                "entity_id": uuid,
                "uuid": uuid,
                "name": name,
                "kind": kind,
                "owner": owner,
                "filters": filters,
                "permissions": permissions,
                "columns": columns,
                "sort_column": sort_column,
                "sort_order": sort_order,
                "_event_service": event_service,
            }
        )

    @entity.Entity.event(name="SavedSearchDeleted", fire_always=True)
    def delete(self):
        self.date_deleted = date.today()

    @entity.Entity.event(name="SavedSearchUpdated", fire_always=True)
    def update(
        self,
        name: str,
        filters: Union[
            CustomObjectSearchFilterDefinition,
            CaseSearchFilterDefinition,
        ],
        columns: List[SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: SortOrder,
    ):
        self.name = name
        self.filters = filters
        self.columns = columns
        self.sort_column = sort_column
        self.sort_order = sort_order

    @entity.Entity.event(
        name="SavedSearchPermissionsUpdated", fire_always=True
    )
    def update_permissions(
        self, permissions: List[SavedSearchPermissionDefinition]
    ):
        self.permissions = permissions
