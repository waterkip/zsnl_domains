# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity, Field
from typing import Optional
from uuid import UUID


class Requestor(Entity):
    """Requestor Entity"""

    entity_type = "requestor"
    entity_id__fields = ["uuid"]

    id: Optional[str] = Field(None, title="Id of the requestor")
    uuid: Optional[UUID] = Field(
        None, title="Internal identifier of requestor"
    )
    requestor_type: Optional[str] = Field(None, title="Type of the requestor")
    name: Optional[str] = Field(None, title="Name of the requestor")
    surname: Optional[str] = Field(None, title="Surname of the requestor")
    surname_prefix: Optional[str] = Field(
        None, title="Surname prefix of the requestor"
    )
    family_name: Optional[str] = Field(
        None, title="Family name of the requestor"
    )
    full_name: Optional[str] = Field(None, title="Full name of the requestor")
    first_names: Optional[str] = Field(
        None, title="First names for the requestor"
    )
    initials: Optional[str] = Field(None, title="Initials for the requestor")
    used_name: Optional[str] = Field(None, title="Identifier of case type")
    burgerservicenummer: Optional[str] = Field(
        None, title="BSN number for the reqestor"
    )
    noble_title: Optional[str] = Field(None, title="Title of the requestor")
    gender: Optional[str] = Field(None, title="Gender of the requestor")
    date_of_birth: Optional[datetime] = Field(
        None, title="Requestor's date of birth"
    )
    date_of_divorce: Optional[datetime] = Field(
        None, title="Date of divorce for requestor"
    )
    place_of_birth: Optional[str] = Field(
        None, title="Requestor's place of birth"
    )
    country_of_birth: Optional[str] = Field(
        None, title="Requestor's country of birth"
    )
    salutation: Optional[str] = Field(
        None, title="Salutation used for requestor"
    )
    salutation1: Optional[str] = Field(
        None, title="Another Salutation used for requestor"
    )
    salutation2: Optional[str] = Field(
        None, title="Another Salutation used for requestor"
    )
    unique_name: Optional[str] = Field(
        None, title="Unique name for the requestor"
    )
    subject_type: Optional[str] = Field(None, title="type of the subject")
    status: Optional[str] = Field(None, title="Status of requestor")
    place_of_residence: Optional[str] = Field(
        None, title="place of residence for requestor"
    )
    country_of_residence: Optional[str] = Field(
        None, title="country of residence for requestor"
    )
    street: Optional[str] = Field(None, title="Address street name")
    residence_street: Optional[str] = Field(
        None, title="Residence address street name"
    )
    zipcode: Optional[str] = Field(None, title="Zipcode value")
    residence_zipcode: Optional[str] = Field(
        None, title="Residence address zipcode"
    )
    house_number: Optional[str] = Field(
        None, title="House number for the requestor"
    )
    residence_house_number: Optional[str] = Field(
        None, title="Residence house number for requestor"
    )
    mobile_number: Optional[str] = Field(
        None, title="Contact mobile number of requestor"
    )
    phone_number: Optional[str] = Field(
        None, title="contact phone number of requestor"
    )
    email: Optional[str] = Field(None, title="Email address of requestor")
    department: Optional[str] = Field(
        None, title="Name of the department for requestor"
    )
    is_secret: Optional[bool] = Field(None, title="If its secret value")
    a_number: Optional[str] = Field(
        None, title="Another identifier in the dutch systems, called a_nummer"
    )
    foreign_residence_address_line1: Optional[str] = Field(
        None, title="foreign residence address first line"
    )
    foreign_residence_address_line2: Optional[str] = Field(
        None, title="foreign residence address second line"
    )
    foreign_residence_address_line3: Optional[str] = Field(
        None, title="foreign residence address third line"
    )
    has_correspondence_address: Optional[bool] = Field(
        None, title="Indicate if requestor has correspondence address"
    )
    correspondence_house_number: Optional[str] = Field(
        None, title="correspondence house number"
    )
    correspondence_zipcode: Optional[str] = Field(
        None, title="correspondence zipcode value"
    )
    correspondence_place_of_residence: Optional[str] = Field(
        None, title="correspondence place of residence"
    )
    correspondence_street: Optional[str] = Field(
        None, title="correspondence address street value"
    )
    password: Optional[str] = Field(
        None, title="Password details for requestor"
    )
    investigation: Optional[str] = Field(
        None, title="investigation value for reqestor"
    )
    date_of_marriage: Optional[datetime] = Field(
        None, title="Marriage date for requestor"
    )
    coc: Optional[str] = Field(
        None, title="Chamber of Commerce number of the organization"
    )
    type_of_business_entity: Optional[str] = Field(
        None, title="Type of the business"
    )
    establishment_number: Optional[str] = Field(
        None, title="Establishment number of requestor organization"
    )
    trade_name: Optional[str] = Field(
        None, title="Trade name for requestor organization"
    )
    login: Optional[str] = Field(None, title="login details for requestor")
    title: Optional[str] = Field(None, title="Title for the requestor")
