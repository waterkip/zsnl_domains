# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty
from ..entities import Case
from ..entities.case_type_version import RuleAction
from ..services import CaseTemplateService
from abc import ABC, abstractmethod


class BaseAction(ABC, minty.Base):

    name = None

    def __init__(self, case: Case):
        self.case = case

    @abstractmethod
    def run(self, action: RuleAction):
        pass


class ChangeConfidentiality(BaseAction):
    """Class to change confidentiality of case"""

    name = "change_confidentiality"

    def run(self, action: RuleAction):
        self.case.confidentiality = action.settings["value"]


class SetCustomFieldValue(BaseAction):
    """Class to change custom_field value of a case"""

    name = "set_value"

    def _update_price(self, action: RuleAction):
        try:
            self.case.set_payment_amount(
                payment_amount=float(action.settings["value"])
            )
        except ValueError:
            self.logger.info(
                f"The supplied value {action.settings['value']} bedrag web for case {self.case.uuid} is not a decimal"
            )
        except KeyError:
            self.logger.info(
                f"Ruleaction 'vul waarde in' for 'Bedrag web' does not contain a price for case with uuid {self.case.uuid}"
            )

    def _update_result(self, action: RuleAction):
        try:
            sequence_num = int(action.settings["value"])
            if self.case.case_type_version:
                case_type_result = (
                    self.case.case_type_version.case_type_results[sequence_num]
                    if sequence_num
                    < len(self.case.case_type_version.case_type_results)
                    else None
                )
                if case_type_result:
                    self.case.set_case_result(
                        case_type_result=case_type_result
                    )
                else:
                    self.logger.info(
                        f"No case type result found on index {sequence_num}"
                    )
        except KeyError:
            self.logger.info(
                f"No result value specified for ruleaction set_value (result) for case {self.case.uuid}"
            )
        except ValueError:
            self.logger.info(
                f"Result value '{action.settings['value']}' specified for ruleaction set_value is invalid for case {self.case.uuid}"
            )

    def run(self, action: RuleAction):
        if "kenmerk" not in action.settings:
            self.logger.info(
                f"Invalid set_value rule action for case {self.case.uuid}"
            )
            return
        if action.settings["kenmerk"] == "price":
            self._update_price(action=action)
        elif action.settings["kenmerk"] == "case_result":
            self._update_result(action=action)
        else:
            self.case.update_custom_field(
                magic_string=action.settings["kenmerk"]["field_magic_string"],
                new_value=[action.settings["value"]],
            )


class ChangeRegistrationDate(BaseAction):
    """Class to change registration date of case"""

    name = "wijzig_registratiedatum"

    def run(self, action: RuleAction):
        recalculate_target_completiondate = (
            True
            if action.settings.get("recalculate", "off") == "on"
            else False
        )
        try:
            registration_date = self.case.custom_fields[
                action.settings["kenmerk"]["field_magic_string"]
            ]["value"][0]

            def empty_username_provider():
                # No (easy) way to retrieve the username here.
                # (no access to cmd.get_repository and user_uuid)
                # It would be better to invest time to make the
                # CaseTemplateService work in a decorator like the rule_engine
                # does
                return ""

            self.case.set_registration_date(
                registration_date=registration_date,
                recalculate_target_completiondate=recalculate_target_completiondate,
                template_service=CaseTemplateService(
                    user_name_retriever=empty_username_provider
                ),
            )
        except (IndexError, KeyError):
            self.logger.info(
                f"Invalid registrationdate value in ruleaction for case {self.case.uuid}"
            )
