# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from ...shared.services.template import TemplateService
from ..entities import CustomObject

logger = logging.getLogger(__name__)


class CustomObjectTemplateService(TemplateService):
    def render_value(self, custom_object: CustomObject, template: str):
        template = getattr(custom_object.custom_object_type, template)

        if template is None:
            template = ""

        template_variables = self._prepare_entity(custom_object)
        return self.render(template, template_variables)
