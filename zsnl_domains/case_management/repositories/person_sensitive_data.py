# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from ... import DatabaseRepositoryBase
from ..entities import PersonSensitiveData
from minty.exceptions import NotFound
from minty.repository import Repository
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema

person_info_query = (
    sql.select(
        [
            schema.NatuurlijkPersoon.uuid,
            schema.NatuurlijkPersoon.burgerservicenummer.label(
                "personal_number"
            ),
        ]
    )
    .where(schema.NatuurlijkPersoon.deleted_on.is_(None))
    .select_from(
        sql.join(
            schema.NatuurlijkPersoon,
            schema.Subject,
            schema.Subject.uuid == schema.NatuurlijkPersoon.uuid,
            isouter=True,
        )
    )
)


class PersonSensitiveDataRepository(Repository, DatabaseRepositoryBase):
    def find_person_sensitive_data_by_uuid(
        self, uuid: UUID
    ) -> PersonSensitiveData:
        """Find sensitive information for a person by uuid."""
        qry_stmt = person_info_query.where(
            schema.NatuurlijkPersoon.uuid == uuid
        )

        query_result = self.session.execute(qry_stmt).fetchone()
        if not query_result:
            raise NotFound(
                f"Person with uuid '{uuid}' not found.", "person/not_found"
            )

        return self._sqla_to_entity(query_result=query_result)

    def _sqla_to_entity(self, query_result) -> PersonSensitiveData:

        return PersonSensitiveData(
            entity_id=query_result.uuid,
            uuid=query_result.uuid,
            personal_number=query_result.personal_number,
        )
