# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .entities import Employee, Organization, Person
from .entities.person import PersonLimited
from .repositories import (
    EmployeeRepository,
    OrganizationRepository,
    PersonRepository,
)
from typing import Callable, Union, cast
from uuid import UUID


def get_subject(
    get_repository: Callable, subject_type: str, subject_uuid: UUID
) -> Union[Person, Organization, Employee]:
    if subject_type == "person":
        subject_repo = cast(PersonRepository, get_repository("person"))
        subject = subject_repo.find_person_by_uuid(subject_uuid)
    elif subject_type == "organization":
        subject_repo = cast(
            OrganizationRepository, get_repository("organization")
        )
        subject = subject_repo.find_organization_by_uuid(subject_uuid)
    elif subject_type == "employee":
        subject_repo = cast(EmployeeRepository, get_repository("employee"))
        subject = subject_repo.find_employee_by_uuid(subject_uuid)
    else:  # pragma: no cover
        raise KeyError(subject_type)

    return subject


def get_subject_limited(
    get_repository: Callable, subject_type: str, subject_uuid: UUID
) -> Union[PersonLimited, Organization, Employee]:
    if subject_type == "person":
        subject_repo = cast(PersonRepository, get_repository("person"))
        subject = subject_repo.find_person_by_uuid_limited(subject_uuid)
    elif subject_type == "organization":
        subject_repo = cast(
            OrganizationRepository, get_repository("organization")
        )
        subject = subject_repo.find_organization_by_uuid_limited(subject_uuid)
    elif subject_type == "employee":
        subject_repo = cast(EmployeeRepository, get_repository("employee"))
        subject = subject_repo.find_employee_by_uuid_limited(subject_uuid)
    else:  # pragma: no cover
        raise KeyError(subject_type)

    return subject
