# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .. import entities
from ..repositories import CountryRepository
from typing import List, cast


class GetCountriesList(minty.cqrs.SplitQueryBase):
    name = "get_countries_list"

    def __call__(
        self,
    ) -> List[entities.Country]:
        repo = cast(CountryRepository, self.get_repository("country"))

        countries_list = repo.get_countries_list()
        return countries_list
