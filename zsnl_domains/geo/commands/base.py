# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import (
    GeoFeatureRelationshipRepository,
    GeoFeatureRepository,
)
from minty.exceptions import NotFound
from minty.validation import validate_with
from pkgutil import get_data
from typing import List, cast
from uuid import UUID, uuid4


class CreateGeoFeature(minty.cqrs.SplitCommandBase):
    name = "create_geo_feature"

    @validate_with(get_data(__name__, "validation/create_geo_feature.json"))
    def __call__(
        self,
        uuid: str,
        geojson: dict,
    ):
        """
        Creates a new GeoFeature entry for an entity with the specified UUID
        """

        repo = cast(GeoFeatureRepository, self.get_repository("geo_feature"))
        repo.create_geo_feature(uuid=UUID(uuid), geo_json=geojson)
        repo.save()


class DeleteGeoFeature(minty.cqrs.SplitCommandBase):
    name = "delete_geo_feature"

    @validate_with(get_data(__name__, "validation/delete_geo_feature.json"))
    def __call__(self, object_uuid: str):
        """
        Delete geo features.
        """

        repo = cast(GeoFeatureRepository, self.get_repository("geo_feature"))
        try:
            geo_feature = repo.get_geo_feature(uuid=UUID(object_uuid))
        except NotFound:
            self.logger.debug(
                f"Geo not found with {object_uuid=}. Skipping delete."
            )
            return

        geo_feature.delete()

        repo.save()


class SetGeoFeatureRelationships(minty.cqrs.SplitCommandBase):
    name = "set_geo_feature_relationships"

    @validate_with(
        get_data(__name__, "validation/set_geo_feature_relationship.json")
    )
    def __call__(self, origin_uuid: str, related_uuids: List[str]):
        "Set the relationships for a geo feature"

        repo = cast(
            GeoFeatureRelationshipRepository,
            self.get_repository("geo_feature_relationship"),
        )
        current_relationships = repo.get_relationships(
            origin_uuid=UUID(origin_uuid)
        )

        for relationship in current_relationships:
            related_uuid = str(relationship.related_uuid)
            if related_uuid in related_uuids:
                # Relationship already exists: remove from "to add" list
                related_uuids.remove(related_uuid)
            else:
                # Relationship no longer exists: delete
                relationship.delete()

        for related_uuid in related_uuids:
            repo.create_geo_feature_relationship(
                relationship_uuid=uuid4(),
                origin_uuid=UUID(origin_uuid),
                related_uuid=UUID(related_uuid),
            )

        repo.save()


class UpdateGeoFeatureRelationships(minty.cqrs.SplitCommandBase):
    name = "update_geo_feature_relationships"

    @validate_with(
        get_data(__name__, "validation/update_geo_feature_relationships.json")
    )
    def __call__(self, origin_uuid: str, added: List[str], removed: List[str]):
        """
        Update the list of relationships for a geo feature by adding and
        removing relations as specified.
        """

        repo = cast(
            GeoFeatureRelationshipRepository,
            self.get_repository("geo_feature_relationship"),
        )
        current_relationships = {
            str(relationship.related_uuid): relationship
            for relationship in repo.get_relationships(
                origin_uuid=UUID(origin_uuid)
            )
        }

        for relationship in current_relationships:
            if relationship in removed:
                # Relationship found in "current relationships", and it's on
                # the remove list.
                current_relationships[relationship].delete()

        for relation_to_add in added:
            if relation_to_add in current_relationships:
                # Already there, no need to add again.
                continue

            # Create the newly added relations
            repo.create_geo_feature_relationship(
                relationship_uuid=uuid4(),
                origin_uuid=UUID(origin_uuid),
                related_uuid=UUID(relation_to_add),
            )

        repo.save()
